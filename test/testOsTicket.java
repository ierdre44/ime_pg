import models.AbsTicket;
import models.TraitementExcel.PlanningRoulement;
import models.osTicket.Ticket;
import models.osTicket.osticketConnector;
import org.junit.BeforeClass;
import org.junit.Test;
import play.test.FakeApplication;
import play.test.Helpers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Created by richard on 30/04/15.
 */
public class testOsTicket {

    private static FakeApplication app;


    @BeforeClass
    public static void startApp() {
        Map<String, String> settings = new HashMap<String, String>();
        settings.put("db.default.url","jdbc:h2:tcp://localhost/bdH2/baseDonnee.db,MODE=PostgreSQL");
        settings.put("db.default.jndiName", "DefaultDS");
        app = Helpers.fakeApplication(settings);


        Helpers.start(app);


    }


    /**
     * On test le parsing fichier roulement planning
     *
     * Modificaiton
     * 1 - Ajout de la date de fin pour les horaires de roulement
     * 2 - Attention avec nom prenom ????
     * 3 - Une fiche de roulement et une seul par salarié sur le site portail
     * --> rangement par odre alphabétiquet et par services
     * 4 - Consolidation en classeur /service totaux etc ...
     *
     *
     *
     */
    @Test
    public void testgetAllTicketOpen(){


        ArrayList<AbsTicket> tickets= osticketConnector.getTicketOpen();
        Iterator I= tickets.iterator();


        while (I.hasNext()){
            AbsTicket ticket = (AbsTicket) I.next();
            System.out.println(ticket);
        }






    }
}
