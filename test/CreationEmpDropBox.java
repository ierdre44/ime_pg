import com.fasterxml.jackson.databind.JsonNode;
import models.Employe;
import models.Requete;
import models.osTicket.Ticket;
import org.junit.BeforeClass;
import org.junit.Test;
import play.test.FakeApplication;
import play.test.Helpers;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by richard on 27/05/15.
 */
public class CreationEmpDropBox {
    private static FakeApplication app;


    @BeforeClass
    public static void startApp() {
        Map<String, String> settings = new HashMap<String, String>();
        settings.put("db.default.url","jdbc:h2:tcp://localhost/bdH2/baseDonnee.db,MODE=PostgreSQL");
        settings.put("db.default.jndiName", "DefaultDS");
        app = Helpers.fakeApplication(settings);


        Helpers.start(app);


    }

    @Test
    public void testCreateEmp(){
        // Ecrire un fichier (pas d'accés en lecture ...)
        // pour la lecture utilisation de SAX
        //SXSSFWorkbook book = new SXSSFWorkbook();
        //book.setCompressTempFiles(true);

        // SXSSFSheet sheet = (SXSSFSheet) book.createSheet();
        //sheet.setRandomAccessWindowSize(100);// keep 100 rows in memory, exceeding rows will be flushed to disk

        // File file = new File("./test/test-A/fic-roulement-A-educ.xlsx");

        Ticket t = new Ticket();
        t.sujet="hubo_transfert";
        t.idUser= Employe.findByMail("richard.urunuela@gmail.com").getId();
        t.nombreFichier=1;
        String []files = {"/tmp/test-drop.xlsx"};
        t.Fichiers= new long[1];
        t.Fichiers[0]=-1;
        t.setFile(files);
        t.virtual= true;
        t.isDropBox=true;
        t.email= "richard.urunuela@gmail.com";


        Requete req = new Requete(t);
        JsonNode res = req.execute(t);



        System.out.println(res);



    }
}
