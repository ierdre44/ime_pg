import controllers.DropBoxController;
import models.Dropbox.DropInterface;
import models.Employe;
import models.Entreprise;
import org.junit.BeforeClass;
import org.junit.Test;
import play.Play;
import play.mvc.Result;
import play.test.FakeApplication;
import play.test.Helpers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static play.test.Helpers.contentAsString;

/**
 * Created by richard on 15/07/15.
 */
public class testMAJIME {


    private static FakeApplication app;


    @BeforeClass
    public static void startApp() {
        Map<String, String> settings = new HashMap<String, String>();
        settings.put("db.default.url", "jdbc:h2:tcp://localhost/bdH2/baseDonnee.db,MODE=PostgreSQL");
        settings.put("db.default.jndiName", "DefaultDS");
        app = Helpers.fakeApplication(settings);


        Helpers.start(app);


    }
    @Test
    public void F_Create_Emp_by_admin(){
        System.err.println("-----------------------------------------------------------------\n" +
                "F create  employe avec fiche\n" +
                "-----------------------------------------------------------------");
        // copie le fichier dans le bon répertoires
         long entId=-1;
        for (Entreprise ent :Entreprise.find.all()){
            if (ent.getNom().compareToIgnoreCase("IMEPG")==0) {
                entId=ent.getId();
            }
        }
        if (entId<=0){
            System.err.println(" ERREUR IME ");

        }

        String entrePriseTest = Play.application().configuration().getString("hubo.dropbox.HomePathEntreprise").replace("<entreprise_nom>", "HUBO");


        //On lance analyse
        // lancement du programme de recherche entreprise
        Result res = new DropBoxController().ListTicketDropbox();
        System.err.println("-----------------------------------------------------------------\n" +
                contentAsString(res) +
                "\n-----------------------------------------------------------------\n");

        res = new DropBoxController().ListTicketDropboxEntreprise(entId);
        System.err.println("-----------------------------------------------------------------\n" +
                contentAsString(res) +
                "\n-----------------------------------------------------------------\n");
        res = new DropBoxController().doCreationDropboxEntreprise(entId);
        System.err.println("-----------------------------------------------------------------\n" +
                contentAsString(res) +
                "\n-----------------------------------------------------------------\n");
        System.err.println("-----------------------------------------------------------------\n" +
                "Affiche employe Liste\n" +
                "-----------------------------------------------------------------");




    }


}