import models.CompteDropbox;
import models.Dropbox.DropInterface;
import models.TraitementExcel.ExcelModele;
import models.TraitementExcel.TrameBase;
import org.junit.BeforeClass;
import org.junit.Test;
import play.test.FakeApplication;
import play.test.Helpers;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by richard on 05/06/15.
 */
public class testDropTrameBase {
    private static FakeApplication app;

    @BeforeClass
    public static void startApp() {
        Map<String, String> settings = new HashMap<String, String>();
        settings.put("db.default.url","jdbc:h2:/Users/richard/Documents/perso/planningserver/imePG-devel/bd/baseDonnee.db");
        settings.put("db.default.jndiName", "DefaultDS");
        app = Helpers.fakeApplication(settings);


        Helpers.start(app);


    }
    @Test
    public void testParseFile() {
        System.out.println("\n\n\n TEST GET Trame");
        CompteDropbox compte = new CompteDropbox();
        compte.setKey("ack2rnu8d0o6p6o");
        compte.setSsecret("2ajwsgkn5s88b9t");
        compte.setAccessToken("./test/auth-drop-hubo");
        compte.setDropUsername("support@hubo-soft.com");
        compte.setDropPass("secret1602");
        DropInterface drop = new DropInterface();
        drop.initDrop(compte);
        //System.out.println(drop.getlisteFichier("/Apps/Entreprises/test").size());
        drop.getFichier("/Apps/Entreprises/tests/PlanningAnnuel-test-A.xlsx"
                ,"/tmp/test-trame.xlsx");

        TrameBase trame = new TrameBase();


        ((ExcelModele) trame).initFromTemplate("/tmp/test-trame.xlsx");

        ((ExcelModele) trame).saveToFileFromTemplate("/tmp/res.xlsx");

        System.out.println("--> " + trame.getAdresseMail());


    }
}
