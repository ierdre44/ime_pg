import models.CompteDropbox;
import models.Dropbox.DropInterface;
import models.Employe;
import models.Entreprise;
import org.junit.BeforeClass;
import org.junit.Test;
import play.test.FakeApplication;
import play.test.Helpers;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by richard on 01/05/15.
 */
public class testInitBaseDemo {
    private static FakeApplication app;


    @BeforeClass
    public static void startApp() {
        Map<String, String> settings = new HashMap<String, String>();
        settings.put("db.default.url","jdbc:h2:tcp://localhost/bdH2/baseDonnee.db,MODE=PostgreSQL");
        settings.put("db.default.jndiName", "DefaultDS");
        app = Helpers.fakeApplication(settings);


        Helpers.start(app);


    }

    @Test
    public void ajoutEmploye(){
       /* Employe emp = new Employe();
        emp.setAdresseMail("richard.urunuela@gmail.com");
        // voir emma.delatour44@gmail.com
        emp.setPassword("secret1602");
        emp.setTraitementRequete("ImePGrequete");
        emp.setIsManager(true);
        emp.setIssPortalUser(true);*/
        Employe emp = new Employe();
        emp.setAdresseMail("richard.urunuela@gmail.com");
        // voir emma.delatour44@gmail.com
        emp.setPassword("secret1602");
        emp.setTraitementRequete("ImePGrequete");
        emp.setIsManager(true);
        emp.setIssPortalUser(true);


        CompteDropbox compte = new CompteDropbox();
        compte.setKey("ack2rnu8d0o6p6o");
        compte.setSsecret("2ajwsgkn5s88b9t");
        compte.setAccessToken("./test/auth-drop-hubo");
        compte.setDropUsername("support@hubo-soft.com");
        compte.setDropPass("secret1602");
        Entreprise ent = new Entreprise();
        ent.setNom("IMEPG");



        ent.employes.add(emp);
        emp.setEntreprise(ent);
        ent.setCompteDropbox(compte);
        compte.save();
        compte.entreprises.add(ent);




        ent.save();
        emp.save();



    }

}