import com.avaje.ebean.Ebean;
import models.CompteDropbox;
import models.Dropbox.DropInterface;
import models.Entreprise;
import org.junit.BeforeClass;
import org.junit.Test;
import play.test.FakeApplication;
import play.test.Helpers;
import scala.tools.cmd.gen.AnyVals;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by richard on 02/06/15.
 */
public class testDropBox {
    private static FakeApplication app;


    @BeforeClass
    public static void startApp() {
        Map<String, String> settings = new HashMap<String, String>();
        settings.put("db.default.url","jdbc:h2:/Users/richard/Documents/perso/planningserver/imePG-devel/bd/baseDonnee.db");
        settings.put("db.default.jndiName", "DefaultDS");
        app = Helpers.fakeApplication(settings);


        Helpers.start(app);


    }

    @Test
    public void testCreateDropbox(){
        System.out.println("\n\n\n TEST Init");

        CompteDropbox compte = new CompteDropbox();
        compte.setKey("ack2rnu8d0o6p6o");
        compte.setSsecret("2ajwsgkn5s88b9t");
        compte.setAccessToken("./test/auth-drop-hubo");
        compte.setDropUsername("support@hubo-soft.com");
        compte.setDropPass("secret1602");
        DropInterface drop = new DropInterface();

        String res=(drop.initDrop(compte)).toString();
        System.out.println(res);


/*        compte = Ebean.find(CompteDropbox.class,1);
        Entreprise ent = new Entreprise();
        ent.setNom  ("IMEPG");
        ent.compteDropbox=compte;
        ent.save();*/

    }
    @Test
    public void testListDropbox(){
        System.out.println("\n\n\n TEST LISTE FICHIER");
        CompteDropbox compte = new CompteDropbox();
        compte.setKey("ack2rnu8d0o6p6o");
        compte.setSsecret("2ajwsgkn5s88b9t");
        compte.setAccessToken("./test/auth-drop-hubo");
        compte.setDropUsername("support@hubo-soft.com");
        compte.setDropPass("secret1602");
        DropInterface drop = new DropInterface();
        drop.initDrop(compte);
        ArrayList res= drop.getlisteFichier("/Apps/Entreprises/IMEPG/modele");

        System.out.println(res.get(0));

    }
    @Test
    public void testGetFile(){
        System.out.println("\n\n\n TEST GET FICHIER");
        CompteDropbox compte = new CompteDropbox();
        compte.setKey("ack2rnu8d0o6p6o");
        compte.setSsecret("2ajwsgkn5s88b9t");
        compte.setAccessToken("./test/auth-drop-hubo");
        compte.setDropUsername("support@hubo-soft.com");
        compte.setDropPass("secret1602");
        DropInterface drop = new DropInterface();
        drop.initDrop(compte);

        drop.getFichier("/Apps/Entreprises/IMEPG/modele"+"/trameBase-model.xlsx"
                        ,"/tmp/test-drop.xlsx");



    }
    @Test
    public void testUploadFile(){
        System.out.println("\n\n\n TEST Put FICHIER");
        CompteDropbox compte = new CompteDropbox();
        compte.setKey("ack2rnu8d0o6p6o");
        compte.setSsecret("2ajwsgkn5s88b9t");
        compte.setAccessToken("./test/auth-drop-hubo");
        compte.setDropUsername("support@hubo-soft.com");
        compte.setDropPass("secret1602");
        DropInterface drop = new DropInterface();
        drop.initDrop(compte);

        drop.putFichier("/tmp/test-drop.xlsx",
                "/Apps/Entreprises/IMEPG/modele"+"/test-drop.xlsx"
                );
        ArrayList res= drop.getlisteFichier("/Apps/Entreprises/IMEPG/modele");

        System.out.println(res.get(0));
        System.out.println(res.get(1));


    }
    @Test
    public void testdeleteFile(){
        System.out.println("\n\n\n TEST Delete FICHIER");
        CompteDropbox compte = new CompteDropbox();
        compte.setKey("ack2rnu8d0o6p6o");
        compte.setSsecret("2ajwsgkn5s88b9t");
        compte.setAccessToken("./test/auth-drop-hubo");
        compte.setDropUsername("support@hubo-soft.com");
        compte.setDropPass("secret1602");
        DropInterface drop = new DropInterface();
        drop.initDrop(compte);

        drop.putFichier("/tmp/test-drop.xlsx",
                "/Apps/Entreprises/IMEPG/modele" + "/test-drop.xlsx"
        );
        ArrayList res= drop.getlisteFichier("/Apps/Entreprises/IMEPG/modele");
        System.out.println(res.size());

        drop.deleteFichier( "/Apps/Entreprises/IMEPG/modele"+"/test-drop.xlsx");
        res= drop.getlisteFichier("/Apps/Entreprises/IMEPG/modele");
        System.out.println(res.size());

    }
    @Test
    public void testMoveFile(){
        System.out.println("\n\n\n TEST Archive FICHIER");
        CompteDropbox compte = new CompteDropbox();
        compte.setKey("ack2rnu8d0o6p6o");
        compte.setSsecret("2ajwsgkn5s88b9t");
        compte.setAccessToken("./test/auth-drop-hubo");
        compte.setDropUsername("support@hubo-soft.com");
        compte.setDropPass("secret1602");
        DropInterface drop = new DropInterface();
        drop.initDrop(compte);
        drop.putFichier("/tmp/test-drop.xlsx",
                "/Apps/Entreprises/IMEPG/modele"+"/test-drop.xlsx"
        );
        ArrayList res= drop.getlisteFichier("/Apps/Entreprises/IMEPG/modele");
        System.out.println(res.size());

        drop.moveFichier("/Apps/Entreprises/IMEPG/modele"+"/test-drop.xlsx",
                "/Apps/Entreprises/IMEPG/modele/archive"+"/test-drop.xlsx");
        res= drop.getlisteFichier("/Apps/Entreprises/IMEPG/modele");
        System.out.println(res.size());

    }


}
