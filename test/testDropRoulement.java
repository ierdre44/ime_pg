import models.CompteDropbox;
import models.Dropbox.DropInterface;
import models.TraitementExcel.PlanningRoulement;
import org.junit.BeforeClass;
import org.junit.Test;
import play.test.FakeApplication;
import play.test.Helpers;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by richard on 04/06/15.
 */
public class testDropRoulement {
    private static FakeApplication app;


    @BeforeClass
    public static void startApp() {
        Map<String, String> settings = new HashMap<String, String>();
        settings.put("db.default.url","jdbc:h2:/Users/richard/Documents/perso/planningserver/imePG-devel/bd/baseDonnee.db");
        settings.put("db.default.jndiName", "DefaultDS");
        app = Helpers.fakeApplication(settings);


        Helpers.start(app);


    }
    @Test
    public void testRoulementDrop(){

        System.out.println("\n\n\n TEST GET Roulement");
        CompteDropbox compte = new CompteDropbox();
        compte.setKey("ack2rnu8d0o6p6o");
        compte.setSsecret("2ajwsgkn5s88b9t");
        compte.setAccessToken("./test/auth-drop-hubo");
        compte.setDropUsername("support@hubo-soft.com");
        compte.setDropPass("secret1602");
        DropInterface drop = new DropInterface();
        drop.initDrop(compte);
        //System.out.println(drop.getlisteFichier("/Apps/Entreprises/test").size());
        drop.getFichier("/Apps/Entreprises/tests/hubo_roulement-A-educ.xlsm"
                ,"/tmp/test-drop.xlsm");


         PlanningRoulement plan= new PlanningRoulement();
        plan.validation("/tmp/test-drop.xlsm");
        System.out.println(" RESULTAT " + plan.getResultat().toString());
    }
}
