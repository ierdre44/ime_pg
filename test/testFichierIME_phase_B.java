import Util.MyUtilLib;
import com.mysql.jdbc.jdbc2.optional.MysqlConnectionPoolDataSource;
import controllers.DropBoxController;
import models.CompteDropbox;
import models.Dropbox.DropInterface;
import models.Employe;
import models.Entreprise;
import models.TraitementExcel.ExcelModele;
import models.TraitementExcel.PlanningRoulement;
import models.TraitementExcel.TrameBase;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellReference;
import org.junit.*;
import org.junit.runners.MethodSorters;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import play.Play;
import play.api.test.TestBrowser;
import play.libs.F;
import play.test.*;

import java.io.*;
import java.nio.file.*;
import java.util.*;

import static controllers.routes.ref.Assets;
import static controllers.routes.ref.DropBoxController;
import static org.fest.assertions.Assertions.assertThat;
import static play.mvc.Results.ok;
import static play.test.Helpers.running;
import static play.test.Helpers.testServer;
import static controllers.routes.ref.Application;
import static org.fest.assertions.Assertions.assertThat;
import static play.mvc.Http.Status.OK;
import static play.mvc.Http.Status.UNAUTHORIZED;
import static play.test.Helpers.*;

import java.util.HashMap;
import java.util.Map;
import org.junit.BeforeClass;
import org.junit.Test;
import play.mvc.Result;
import play.test.FakeRequest;

/**
 * Created by richard on 06/07/15.
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class testFichierIME_phase_B {

    private static FakeApplication app;

    private static long entId;

    private static long empId;
    private static long empIdManager;


    @BeforeClass
    public static void startApp() {

        Map<String, String> settings = new HashMap<String, String>();
        settings.put("db.default.url", "jdbc:h2:tcp://localhost/bdH2/test1_IME");
        settings.put("db.default.jndiName", "DefaultDS");
        app = Helpers.fakeApplication(settings);


        Helpers.start(app);








    }


    @Test
    public void A_testEnv(){
        System.err.println("-----------------------------------------------------------------\n" +
                "A test  env \n" +
                "-----------------------------------------------------------------");

        List<Entreprise> ents = Entreprise.find.all();

     /*   ents.get(0).delete();
            ents = Entreprise.find.all();*/
        System.out.println(ents.size());
        assertThat(ents.size()).isEqualTo(1);

        List<Employe> emps = Employe.find.all();
        System.out.println(emps.size());
        assertThat(emps.size()).isEqualTo(2);
        System.out.println(emps.get(0).getJson());
        System.out.println(emps.get(0).getId());
        System.out.println(emps.get(1).getJson());
        System.out.println(emps.get(1).getId());
        empId=Employe.findByMail("richard.urunuela@icloud.com").getId();
        empIdManager=Employe.findByMail("richard.urunela@gmail.com").getId();




    }

    /**
     * Test des fichiers semaines
     *
     */
    @Test
    public void B_testFile_A(){
        System.err.println("-----------------------------------------------------------------\n" +
                "B test  env \n" +
                "-----------------------------------------------------------------");
        String Path="/var/hubo/users/"+empId+"/";
        for (int i = 0 ; i< 53; i++) {
            String S1 = Path + "planning-semaine-"+i+".xlsm";
            try {
                String d1 = MyUtilLib.getStringFromExcel(S1, 0, 1, 1);
                String d2 = MyUtilLib.getStringFromExcel(S1, 0, 2, 1);
                Date D1 = MyUtilLib.formatDateFromString(d1);
                //System.out.println(d2);
                Date D2 = MyUtilLib.formatDateFromString(d2);
                Calendar cal1 = Calendar.getInstance();
                cal1.setTime(D1);
                Calendar cal2 = Calendar.getInstance();
                cal2.setTime(D2);
                long nb_jour = ((D2.getTime() - D1.getTime()) / 1000 / 60 / 60 / 24) + 1;
                //System.out.println(nb_jour + " \n" + D1 + " \n" + D2);
                System.out.println("-");
                if (nb_jour < 7) {
                    boolean resA = (cal1.get(Calendar.DAY_OF_WEEK) == Calendar.MONDAY);//Lundi
                    boolean resB = (cal2.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY);//Dimanche

                    assertThat(resA || resB).isTrue();
                } else {
                    boolean resA = (cal1.get(Calendar.DAY_OF_WEEK) == Calendar.MONDAY);//Lundi
                    boolean resB = (cal2.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY);//Dimanche
                    assertThat(resA && resB).isTrue();
                }


            } catch (IOException e) {
                e.printStackTrace();
            } catch (InvalidFormatException e) {
                e.printStackTrace();
            }
        }


    }
    /**
     * Test des fichiers semaines
     *
     */
   /* @Test
    public void C_POSE_CONGE_A(){
        System.err.println("-----------------------------------------------------------------\n" +
                "C Pose Conge   \n" +
                "-----------------------------------------------------------------");
        String Path="/var/hubo/users/"+empId+"/";
        String tmpFile="/tmp/wb-1.xls";

        // Create file avec conge pose = > Modification du fichier trame-Annuel
        InputStream inp = null;
        try {
            inp = new FileInputStream(Path+"trame-Annuel.xlsx");
            Workbook wb = WorkbookFactory.create(inp);
            Sheet sheet = wb.getSheetAt(0);
            for(String r: new String[]{"T","U","V","W","X"}) {
                CellReference ref = new CellReference(r+"6");
                Cell cell = sheet.getRow(ref.getRow()).getCell(ref.getCol());
                ///System.out.println(cell.getStringCellValue());
                cell.setCellValue("CP");
            }
            for(String r: new String[]{"I","J","K","L","M"}) {
                CellReference ref = new CellReference(r+"16");
                Cell cell = sheet.getRow(ref.getRow()).getCell(ref.getCol());
               // System.out.println(cell.getStringCellValue());
                cell.setCellValue("CT");
            }
            // Write the output to a file
            FileOutputStream fileOut = new FileOutputStream(tmpFile);
            wb.write(fileOut);
            fileOut.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (InvalidFormatException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        //InputStream inp = new FileInputStream("workbook.xlsx");




        // copie le fichier dans le bon répertoires
        Employe emp = Employe.find.byId(empIdManager);
        Entreprise ent = emp.getEntreprise();
        String entrePriseTest = Play.application().configuration().getString("hubo.dropbox.HomePathEntreprise").replace("<entreprise_nom>", "HUBO");
        // recupérer fichier de test et copie

        DropInterface drop = new DropInterface();
        drop.initDrop(ent.getCompteDropbox());

        String ToTransfert = entrePriseTest + "/administrateurs/" + emp.getAdresseMail() + "/Hubo-Planning/Hubo-Transfert";
        drop.putFichier(tmpFile, ToTransfert + "/PlanningAnnuel-test-phase2.xlsx");
        Result res=ok();
        res = new DropBoxController().ListTicketDropboxEntreprise(emp.getEntreprise().getId());
        System.out.println("-----------------------------------------------------------------\n"
                +contentAsString(res));
        res = new DropBoxController().doCreationDropboxEntreprise(emp.getEntreprise().getId());
        System.err.println("-----------------------------------------------------------------\n" +
                contentAsString(res) +
                "\n-----------------------------------------------------------------\n");


        // Check si jour sont bien posé
        String semaine1=drop.getTempFichier(entrePriseTest+"/2015/salaries/Alain-Pierre-162/Hubo-Planning"+"/planning-semaine-3.xlsm");
        String semaine2=drop.getTempFichier(entrePriseTest+"/2015/salaries/Alain-Pierre-162/Hubo-Planning"+"/planning-semaine-24.xlsm");
        //System.out.println(semaine1+"\n"+semaine2);
        int minColonne = PlanningRoulement.refDebutZonePlanning.getCol();
        int minRow = PlanningRoulement.refDebutZonePlanning.getRow();
        int maxColonne = PlanningRoulement.refFinZonePlanning.getCol();
        int maxRow = PlanningRoulement.refFinZonePlanning.getRow();
        assertThat(minColonne).isEqualTo(4);
        assertThat(minRow).isEqualTo(6);
        assertThat(maxColonne).isEqualTo(67);
        assertThat(maxRow).isEqualTo(10);
        for(String fileName:new String[]{semaine1,semaine2}){

            try {
                inp = new FileInputStream(fileName);
                Workbook wb = WorkbookFactory.create(inp);
                Sheet sheet = wb.getSheetAt(0);
                for (int ligne=minRow;ligne< maxRow ; ligne++){
                    for (int colonne =minColonne;colonne < maxColonne ; colonne ++ ){
                        Cell cell = sheet.getRow(ligne).getCell(colonne);
                        //System.out.println(" "+ MyUtilLib.isStyleNoTEPourPlanning(cell) +" " + colonne + "  "+ligne);
                        //Verifie que les cellules sont grisé
                        assertThat(MyUtilLib.isStyleNoTEPourPlanning(cell)).isTrue();

                    }
                }




                // Write the output to a file

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (InvalidFormatException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }


        }






    }
    *//**
     * Modification planning Horaire avec couleur
     * semaine 25
     * du	15/06/2015
     * au	21/06/2015
     * get color en D7 et copie zone :
     * M8 - > BH8
     * M9 - > BH9
     * M10 - > AW10
     *//*
    @Test
    public void D_MOD_HEURE(){
        System.err.println("-----------------------------------------------------------------\n" +
                "C Pose Conge   \n" +
                "-----------------------------------------------------------------");
        String Path="/var/hubo/users/"+empId+"/";
        String tmpFile="/tmp/wb-2.xls";

        // Create file avec conge pose = > Modification du fichier trame-Annuel
        InputStream inp = null;
        try {
            inp = new FileInputStream(Path+"trame-Annuel.xlsx");
            Workbook wb = WorkbookFactory.create(inp);
            Sheet sheet = wb.getSheetAt(0);
            for(String r: new String[]{"T","U","V","W","X"}) {
                CellReference ref = new CellReference(r+"6");
                Cell cell = sheet.getRow(ref.getRow()).getCell(ref.getCol());
                ///System.out.println(cell.getStringCellValue());
                cell.setCellValue("CP");
            }
            for(String r: new String[]{"I","J","K","L","M"}) {
                CellReference ref = new CellReference(r+"16");
                Cell cell = sheet.getRow(ref.getRow()).getCell(ref.getCol());
                // System.out.println(cell.getStringCellValue());
                cell.setCellValue("CT");
            }
            // Write the output to a file
            FileOutputStream fileOut = new FileOutputStream(tmpFile);
            wb.write(fileOut);
            fileOut.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (InvalidFormatException e) {
            e.printStackTrace();
        } catch (IOExceptiosecret1602
        n e) {
            e.printStackTrace();
        }


    }*/

    @Test
    public void E_testMergeFile() {

        //Test si trame de base OK
        TrameBase trame = new TrameBase();
        //  ((ExcelModele) trame).setPathModele("trame-Annuel.xlsx");
        // ((ExcelModele) trame).initFromTemplate();
        trame.initFromFile("/var/hubo/users/" + empId + "/trame-Annuel.xlsx");


        PlanningRoulement plan= new PlanningRoulement();
        plan.validation("/var/hubo/users/"+empId+"/planning-Roulement.xlsm");


        trame.mergePlanning(plan);




        ((ExcelModele) trame).saveToFileFromTemplate("/tmp/res2.xlsx");

        //System.out.println(trame.getResultat().toString());


    }

}
