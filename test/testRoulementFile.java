import controllers.DropBoxController;
import models.Dropbox.DropInterface;
import models.Employe;
import models.Entreprise;
import models.TraitementExcel.PlanningRoulement;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.junit.BeforeClass;
import org.junit.Test;
import play.Play;
import play.mvc.Result;
import play.test.FakeApplication;
import play.test.Helpers;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static play.test.Helpers.contentAsString;

/**
 * Created by richard on 20/04/15.
 */
public class testRoulementFile {

    private static FakeApplication app;


    @BeforeClass
     public static void startApp() {
        Map<String, String> settings = new HashMap<String, String>();
        settings.put("db.default.url","jdbc:h2:tcp://localhost/bdH2/test1_IME");
        settings.put("db.default.jndiName", "DefaultDS");
        app = Helpers.fakeApplication(settings);


        Helpers.start(app);


    }


    /**
     * On test le parsing fichier roulement planning
     *
     * Modificaiton
     * 1 - Ajout de la date de fin pour les horaires de roulement
     * 2 - Attention avec nom prenom ????
     * 3 - Une fiche de roulement et une seul par salarié sur le site portail
     * --> rangement par odre alphabétiquet et par services
     * 4 - Consolidation en classeur /service totaux etc ...
     *
     *
     *
     */
   /* @Test
    public void testParseFile(){
        // Ecrire un fichier (pas d'accés en lecture ...)
        // pour la lecture utilisation de SAX
        //SXSSFWorkbook book = new SXSSFWorkbook();
        //book.setCompressTempFiles(true);

       // SXSSFSheet sheet = (SXSSFSheet) book.createSheet();
        //sheet.setRandomAccessWindowSize(100);// keep 100 rows in memory, exceeding rows will be flushed to disk



        PlanningRoulement plan= new PlanningRoulement();
        plan.validation("/var/hubo/users/301/planning-Roulement.xlsm");
        System.out.println(plan.getResultat().toString());





    }*/
    /***
     * Creation d'un employe par admin user
     */
    @Test
    public void F_Create_Emp_by_admin(){
        System.err.println("-----------------------------------------------------------------\n" +
                "F create  employe avec fiche\n" +
                "-----------------------------------------------------------------");
        // copie le fichier dans le bon répertoires
        Employe emp = Employe.find.byId(161l);
        Entreprise ent= emp.getEntreprise();
        String entrePriseTest = Play.application().configuration().getString("hubo.dropbox.HomePathEntreprise").replace("<entreprise_nom>", "HUBO");
        // recupérer fichier de test et copie

        DropInterface drop = new DropInterface();
        drop.initDrop(ent.getCompteDropbox());
        //String testFileCreateEmp=
          //      "/Apps/Entreprises/tests/test-educateur-A.xlsm";
        String testFileCreateEmp=
                "/Apps/Entreprises/tests/hubo_roulement - ARCOBELLI.xlsm";
        System.err.println(testFileCreateEmp);
        String ToTransfert = entrePriseTest + "/administrateurs/"+emp.getAdresseMail()+"/Hubo-Planning/Hubo-Transfert";
        String tempFile=drop.getTempFichier(testFileCreateEmp);
        System.err.println(tempFile);

        //tempFile="/tmp/planning-Roulement.xlsm";
        drop.putFichier(tempFile, ToTransfert + "/test-educA.xlsm");

        //On lance analyse
        // lancement du programme de recherche entreprise
        Result res = new DropBoxController().ListTicketDropbox();
        System.err.println("-----------------------------------------------------------------\n" +
                contentAsString(res) +
                "\n-----------------------------------------------------------------\n");

        res = new DropBoxController().ListTicketDropboxEntreprise(33l);
        System.err.println("-----------------------------------------------------------------\n" +
                contentAsString(res) +
                "\n-----------------------------------------------------------------\n");
        res = new DropBoxController().doCreationDropboxEntreprise(33l);
        System.err.println("-----------------------------------------------------------------\n" +
                contentAsString(res) +
                "\n-----------------------------------------------------------------\n");
        System.err.println("-----------------------------------------------------------------\n" +
                "Affiche employe Liste\n" +
                "-----------------------------------------------------------------");
        List<Employe> emps = Employe.find.all();
        System.err.println(emps.size());
        for (Employe em:emps){
            System.err.println(em.getAdresseMail());
        }







    }

    /***
     * Pose Conge par admin user
     */
    /*//region Description
    @Test
    public void H_POSE_CONGE_by_admin() {
        System.err.println("-----------------------------------------------------------------\n" +
                "H Pose conge avec fiche\n" +
                "-----------------------------------------------------------------");

        List<Employe> emps = Employe.find.all();
        for (Employe em : emps) {
            System.out.println(em.getAdresseMail());
        }
        // copie le fichier dans le bon répertoires
        Employe emp = Employe.find.byId(161l);
        Entreprise ent = emp.getEntreprise();
        String entrePriseTest = Play.application().configuration().getString("hubo.dropbox.HomePathEntreprise").replace("<entreprise_nom>", "HUBO");
        // recupérer fichier de test et copie

        DropInterface drop = new DropInterface();
        drop.initDrop(ent.getCompteDropbox());
        String testFileCreateEmp =
                "/Apps/Entreprises/tests/PlanningAnnuel-test-A.xlsx";
        System.err.println(testFileCreateEmp);
        String ToTransfert = entrePriseTest + "/administrateurs/" + emp.getAdresseMail() + "/Hubo-Planning/Hubo-Transfert";
        String tempFile = drop.getTempFichier(testFileCreateEmp);
        System.err.println(tempFile);
        tempFile="/tmp/trame-Annuel-test.xlsx";
        drop.putFichier(tempFile, ToTransfert + "/PlanningAnnuel-test-A.xlsx");

        Result res = new DropBoxController().ListTicketDropboxEntreprise(33l);
        System.err.println("-----------------------------------------------------------------\n" +
                contentAsString(res) +
                "\n-----------------------------------------------------------------\n");
        res = new DropBoxController().doCreationDropboxEntreprise(33l);
        System.err.println("-----------------------------------------------------------------\n" +
                contentAsString(res) +
                "\n-----------------------------------------------------------------\n");

        System.err.println("-----------------------------------------------------------------\n" +
                "Verification fichier" + entrePriseTest + "/2015/salaries" +
                "\n-----------------------------------------------------------------\n");
        ArrayList<String> listesS = drop.getlisteDirectory(entrePriseTest + "/2015/salaries");

        String f1 = drop.getTempFichier(entrePriseTest + "/2015/salaries/" + listesS.get(0) + "/planning-semaine-1.xlsm");
        System.err.println(f1);


    }*/
    //endregion

  


}
