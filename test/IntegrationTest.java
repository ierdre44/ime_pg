import org.junit.*;

import play.mvc.*;
import play.test.*;
import play.libs.F.*;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import static play.test.Helpers.*;
import static org.fest.assertions.Assertions.*;

import static org.fluentlenium.core.filter.FilterConstructor.*;

public class IntegrationTest {

    /**
     * add your integration test here
     * in this example we just check if the welcome page is being shown
     */
    private static FakeApplication app;


    //@BeforeClass
    //public static void startApp() {
    @Test
    public void test() {
        File bdRef = new File("./bdH2/test1_IME.h2.db");
        bdRef.delete();
        Map<String, String> settings = new HashMap<String, String>();
        settings.put("db.default.url", "jdbc:h2:tcp://localhost/bdH2/test1_IME");
        settings.put("db.default.jndiName", "default");
        app = Helpers.fakeApplication(settings);


        Helpers.start(app);
    /*}
    @Test
    public void test() {*/
        running(testServer(9999, app), HTMLUNIT, new Callback<TestBrowser>() {
            public void invoke(TestBrowser browser) {
                browser.goTo("http://localhost:9999");
                System.out.println(browser.pageSource());
                //assertThat(browser.pageSource()).contains("Your new application is ready.");
            }
        });
    }

}
