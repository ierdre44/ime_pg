import controllers.DropBoxController;
import org.junit.BeforeClass;
import org.junit.Test;
import play.mvc.Result;
import play.test.FakeApplication;
import play.test.Helpers;

import java.util.HashMap;
import java.util.Map;

import static org.fest.assertions.Assertions.assertThat;
import static play.test.Helpers.*;

/**
 * Created by richard on 09/06/15.
 */
public class testRouteDropbox {
    private static FakeApplication app;


    @BeforeClass
    public static void startApp() {
        Map<String, String> settings = new HashMap<String, String>();
        settings.put("db.default.url","jdbc:h2:tcp://localhost/bdH2/baseDonnee.db,MODE=PostgreSQL");
        settings.put("db.default.jndiName", "DefaultDS");
        app = Helpers.fakeApplication(settings);


        Helpers.start(app);


    }

  /*  @Test
    public void listeTicketRouteDropbox() {

        Result result = route(fakeRequest(GET, "/management/TicketDropbox"));
        assertThat(result).isNotNull();
        System.out.println(contentAsString(result));

    }
    @Test
    public void listeTicketRouteDropboxByEntreprise() {

        Result result = route(fakeRequest(GET, "/management/TicketDropbox/1"));
        assertThat(result).isNotNull();
        System.out.println(contentAsString(result));

    }*/
    @Test
    public void DoCreateRouteDropboxByEntreprise() {
        System.err.println( "-----------------------------------------------------------------\n"+
                           " Mise a jour avec les tickets de creation de l'entreprise numero 1 \n"+
                           "-----------------------------------------------------------------");
        //Result result = route(fakeRequest(GET, "/management/CreateTicketDropbox/1"));
        Result result = new DropBoxController().doCreationDropboxEntreprise(1);
        assertThat(result).isNotNull();
        System.out.println(contentAsString(result));
        System.err.println(
                "-----------------------------------------------------------------");

    }


}
