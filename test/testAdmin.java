import com.fasterxml.jackson.databind.JsonNode;
import models.Employe;
import models.Requete;
import models.osTicket.Ticket;
import org.junit.BeforeClass;
import org.junit.Test;
import play.test.FakeApplication;
import play.test.Helpers;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by richard on 15/07/15.
 */
public class testAdmin {
    private static FakeApplication app;


    @BeforeClass
    public static void startApp() {
        Map<String, String> settings = new HashMap<String, String>();
        settings.put("db.default.url","jdbc:h2:tcp://localhost/bdH2/test1_IME");
        settings.put("db.default.jndiName", "DefaultDS");
        app = Helpers.fakeApplication(settings);


        Helpers.start(app);


    }
    @Test
    public void testCreate(){
       /* String testFileCreateEmp=
                "/Apps/Entreprises/tests/testAdmin.xlsx";
        System.err.println(testFileCreateEmp);
        String ToTransfert = entrePriseTest + "/administrateurs/"+emp.getAdresseMail()+"/Hubo-Planning/Hubo-Transfert";
        String tempFile=drop.getTempFichier(testFileCreateEmp);
        System.err.println(tempFile);*/
        Ticket t = new Ticket();
        t.sujet="hubo_transfert";
        t.idUser= Employe.findByMail("richard.urunuela@gmail.com").getId();
        t.nombreFichier=1;
        String []files = {"/tmp/test-admin.xlsx"};
        t.Fichiers= new long[1];
        t.Fichiers[0]=-1;
        t.setFile(files);
        t.virtual= true;
        t.isDropBox=true;
        t.email= "richard.urunuela@gmail.com";


        Requete req = new Requete(t);
        JsonNode res = req.execute(t);



        System.out.println(res);


    }
}
