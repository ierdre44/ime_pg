import Util.MyUtilLib;
import controllers.DropBoxController;
import models.CompteDropbox;
import models.Dropbox.DropInterface;
import models.Employe;
import models.Entreprise;
import org.junit.*;
import org.junit.runners.MethodSorters;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import play.Play;
import play.api.test.TestBrowser;
import play.libs.F;
import play.test.*;

import java.io.File;
import java.io.IOException;
import java.nio.file.*;
import java.util.*;

import static controllers.routes.ref.DropBoxController;
import static org.fest.assertions.Assertions.assertThat;
import static play.test.Helpers.running;
import static play.test.Helpers.testServer;
import static controllers.routes.ref.Application;
import static org.fest.assertions.Assertions.assertThat;
import static play.mvc.Http.Status.OK;
import static play.mvc.Http.Status.UNAUTHORIZED;
import static play.test.Helpers.*;

import java.util.HashMap;
import java.util.Map;
import org.junit.BeforeClass;
import org.junit.Test;
import play.mvc.Result;
import play.test.FakeRequest;

/**
 * Created by richard on 18/06/15.
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class CreateTestEnvIME_phase_A {

    private static FakeApplication app;

    private static long entId;

    private static long empId;


    @BeforeClass
    public static void startApp() {
        Path from = Paths.get("./bdH2/test1_IME_REF.h2.db");
        Path to = Paths.get("./bdH2/test1_IME.h2.db");
        File bdRef = new File("./bdH2/test1_IME.h2.db");
        bdRef.delete();

        CopyOption[] options = new CopyOption[]{
                StandardCopyOption.REPLACE_EXISTING,
                StandardCopyOption.COPY_ATTRIBUTES
        };
        try {
            Files.copy(from, to, options);
        } catch (IOException e) {
            e.printStackTrace();
        }

        Map<String, String> settings = new HashMap<String, String>();
        settings.put("db.default.url", "jdbc:h2:tcp://localhost/bdH2/test1_IME");
        settings.put("db.default.jndiName", "DefaultDS");
        app = Helpers.fakeApplication(settings);


        Helpers.start(app);


  /*  }
        @Test
        public void testBase(){*/



        System.err.println("-----------------------------------------------------------------\n" +
                "test creation env \n" +
                "-----------------------------------------------------------------");

        List<Entreprise> ents = Entreprise.find.all();

      if (ents.size()>0)  ents.get(0).delete();
        if (Employe.find.all().size()>0)   Employe.find.all().get(0).delete();
        if (Employe.find.all().size()>0)   Employe.find.all().get(0).delete();
            ents = Entreprise.find.all();
        System.out.println(ents.size());
        assertThat(ents.size()).isEqualTo(0);

            List<Employe> emps = Employe.find.all();
            System.out.println(emps.size());
            assertThat(emps.size()).isEqualTo(0);

    }





    @Test
    public void A_createEnt(){
        System.err.println("-----------------------------------------------------------------\n" +
                "A test creation entreprise avec compte Dropbox \n" +
                "-----------------------------------------------------------------");
        CompteDropbox compte = new CompteDropbox();
        compte.setKey("ack2rnu8d0o6p6o");
        compte.setSsecret("2ajwsgkn5s88b9t");
        compte.setAccessToken("./test/auth-drop-hubo");
        compte.setDropUsername("support@hubo-soft.com");
        compte.setDropPass("secret1602");
        compte.save();

      /*
       Problème on ne peut pas partager de fichier interne à l'application ...
       Donc on passe sur partage fichier excel mais on ne peut pas
       supprimer de répertoires

       CompteDropbox compte = new CompteDropbox();
        compte.setKey("wzbm0zo8npgnrw5");
        compte.setSsecret("hgaub531kwm82ff");
        compte.setAccessToken("./test/auth-drop-hubo-absences");
        compte.setDropUsername("support@hubo-soft.com");
        compte.setDropPass("secret1602");
        compte.save();*/






        Entreprise ent = new Entreprise();
        ent.setNom  ("HUBO");
        ent.compteDropbox=compte;
        ent.save();
        this.entId= ent.getId();

        List<Entreprise> ents = Entreprise.find.all();
        System.out.println(ents.size());
        assertThat(ents.size()).isEqualTo(1);


    }
    @Test
    public void B_createEmp() {
        System.err.println("-----------------------------------------------------------------\n" +
                "B test creation employe \n" +
                "-----------------------------------------------------------------");
        Employe emp = new Employe();
        emp.setAdresseMail("richard.urunuela@gmail.com");
        emp.setManager(true);
        Entreprise ent = Entreprise.find.byId(entId);
        emp.setTraitementRequete("ImePGrequete");

        emp.setEntreprise(ent);

        emp.save();
        System.out.println(emp+"");
        List<Employe> emps = Employe.find.all();
        System.out.println(emps.size());
        assertThat(emps.size()).isEqualTo(1);
        empId = emp.getId();


    }
    @Test
    public void D_dropBoxEnv(){
        System.err.println("-----------------------------------------------------------------\n" +
                "D init Env Dropbox \n" +
                "-----------------------------------------------------------------");
        Entreprise ent = Entreprise.find.byId(entId);
        System.out.println(ent.toString());
        DropInterface drop = new DropInterface();
        drop.initDrop(ent.getCompteDropbox());
        assertThat(drop.getDropClient()).isNotNull();
        //Liste directory dans le reperotire entreprise
        String entrePriseTest = Play.application().configuration().getString("hubo.dropbox.HomePathEntreprise").replace("<entreprise_nom>","HUBO");


        //System.out.println(listeFichier.get(0));
        assertThat(drop.fileExist(entrePriseTest)).isTrue();

        ArrayList<String> listeFichier = drop.getlisteFichier(entrePriseTest);
        System.out.println(listeFichier.size());
        //drop.createDirectory()

        //Creation arborescence pour HUBO_ABSENCE_PROGRAMME_IME
        //
        drop.createDirectory(entrePriseTest + "/administrateurs");
        drop.createDirectory(entrePriseTest + "/2015");
        drop.createDirectory(entrePriseTest + "/modeles");
        drop.putFichier("/var/hubo/modele/PlanningAnnuel.xlsx", entrePriseTest+"/modeles/trameBase-model.xlsx");

        // drop.removeDirectory(entrePriseTest+"/administrateurs");





    }

    /**
     * Creation admin user
     */
    @Test
    public void E_Create_admin_dir(){
        Entreprise ent = Entreprise.find.byId(entId);
        System.err.println("-----------------------------------------------------------------\n" +
                "E create Admin employe\n" +
                "-----------------------------------------------------------------");
        List<Employe> emps = Employe.find.all();
        for(Employe emp:emps){
            if (emp.entreprise.getId()==ent.getId()){
                System.err.println("Employe find " + emp);
                if (emp.getIsManager()){
                    String entrePriseTest = Play.application().configuration().getString("hubo.dropbox.HomePathEntreprise").replace("<entreprise_nom>","HUBO");
                    DropInterface drop = new DropInterface();
                    drop.initDrop(ent.getCompteDropbox());
                    drop.createDirectory(entrePriseTest + "/administrateurs/"+emp.getAdresseMail());
                    drop.createDirectory(entrePriseTest + "/administrateurs/"+emp.getAdresseMail()+"/Hubo-Planning/Hubo-Transfert");



                }
            }
        }


    }
    /***
     * Creation d'un employe par admin user
     */
    @Test
    public void F_Create_Emp_by_admin(){
        System.err.println("-----------------------------------------------------------------\n" +
                "F create  employe avec fiche\n" +
                "-----------------------------------------------------------------");
        // copie le fichier dans le bon répertoires
        Employe emp = Employe.find.byId(empId);
        Entreprise ent= emp.getEntreprise();
        String entrePriseTest = Play.application().configuration().getString("hubo.dropbox.HomePathEntreprise").replace("<entreprise_nom>", "HUBO");
        // recupérer fichier de test et copie

        DropInterface drop = new DropInterface();
        drop.initDrop(ent.getCompteDropbox());
       String testFileCreateEmp=
                "/Apps/Entreprises/tests/test-educateur-A.xlsm";

       // String testFileCreateEmp=
         //       "/Apps/Entreprises/tests/hubo_roulement - ARCOBELLI.xlsm";
        System.err.println(testFileCreateEmp);
        String ToTransfert = entrePriseTest + "/administrateurs/"+emp.getAdresseMail()+"/Hubo-Planning/Hubo-Transfert";
        String tempFile=drop.getTempFichier(testFileCreateEmp);
        System.err.println(tempFile);
        drop.putFichier(tempFile, ToTransfert + "/test-educA.xlsm");

        //On lance analyse
        // lancement du programme de recherche entreprise
        Result res = new DropBoxController().ListTicketDropbox();
        System.err.println("-----------------------------------------------------------------\n" +
                contentAsString(res) +
                "\n-----------------------------------------------------------------\n");

        res = new DropBoxController().ListTicketDropboxEntreprise(entId);
        System.err.println("-----------------------------------------------------------------\n" +
                contentAsString(res) +
                "\n-----------------------------------------------------------------\n");
        res = new DropBoxController().doCreationDropboxEntreprise(entId);
        System.err.println("-----------------------------------------------------------------\n" +
                contentAsString(res) +
                "\n-----------------------------------------------------------------\n");
        System.err.println("-----------------------------------------------------------------\n" +
                "Affiche employe Liste\n" +
                "-----------------------------------------------------------------");
        List<Employe> emps = Employe.find.all();
        System.err.println(emps.size());
        for (Employe em:emps){
            System.err.println(em.getAdresseMail());
        }




    }
    /***
     * Creation d'un employe par admin user
     */
    @Test
    public void G_RE_Create_Emp_by_admin() {
        System.err.println("-----------------------------------------------------------------\n" +
                "-->G create  employe avec fiche test avec user existe deja\n" +
                "-----------------------------------------------------------------");
        F_Create_Emp_by_admin();
    }
    /***
     * Pose Conge par admin user
     */
    @Test
    public void H_POSE_CONGE_by_admin() {
        System.err.println("-----------------------------------------------------------------\n" +
                "H Pose conge avec fiche\n" +
                "-----------------------------------------------------------------");

        List<Employe> emps = Employe.find.all();
        for (Employe em : emps) {
            System.out.println(em.getAdresseMail());
        }
        // copie le fichier dans le bon répertoires
        Employe emp = Employe.find.byId(empId);
        Entreprise ent = emp.getEntreprise();
        String entrePriseTest = Play.application().configuration().getString("hubo.dropbox.HomePathEntreprise").replace("<entreprise_nom>", "HUBO");
        // recupérer fichier de test et copie

        DropInterface drop = new DropInterface();
        drop.initDrop(ent.getCompteDropbox());
        String testFileCreateEmp =
                "/Apps/Entreprises/tests/PlanningAnnuel-test-A.xlsx";
        System.err.println(testFileCreateEmp);
        String ToTransfert = entrePriseTest + "/administrateurs/" + emp.getAdresseMail() + "/Hubo-Planning/Hubo-Transfert";
        String tempFile = drop.getTempFichier(testFileCreateEmp);
        System.err.println(tempFile);
        drop.putFichier(tempFile, ToTransfert + "/PlanningAnnuel-test-A.xlsx");

        Result res = new DropBoxController().ListTicketDropboxEntreprise(entId);
        System.err.println("-----------------------------------------------------------------\n" +
                contentAsString(res) +
                "\n-----------------------------------------------------------------\n");
        res = new DropBoxController().doCreationDropboxEntreprise(entId);
        System.err.println("-----------------------------------------------------------------\n" +
                contentAsString(res) +
                "\n-----------------------------------------------------------------\n");

        System.err.println("-----------------------------------------------------------------\n" +
                "Verification fichier" + entrePriseTest + "/2015/salaries" +
                "\n-----------------------------------------------------------------\n");
        ArrayList<String> listesS = drop.getlisteDirectory(entrePriseTest + "/2015/salaries");

        String f1 = drop.getTempFichier(entrePriseTest + "/2015/salaries/" + listesS.get(0) + "/planning-semaine-1.xlsm");
        System.err.println(f1);


    }
    /***
     * Creation d'un employe par admin user
     */
    @Test
    public void I_TEST_FICHIER_SEMAINE(){


    }







    @AfterClass
    public static void EndApp() {
        System.err.println("-----------------------------------------------------------------\n" +
                "END ------------------->>>>\n" +
                "-----------------------------------------------------------------");

        DropInterface drop = new DropInterface();
        Employe emp = Employe.find.byId(empId);
        Entreprise ent= emp.getEntreprise();
        drop.initDrop(ent.getCompteDropbox());
        String entrePriseTest = Play.application().configuration().getString("hubo.dropbox.HomePathEntreprise").replace("<entreprise_nom>", "HUBO");

        String ToTransfert = entrePriseTest + "/administrateurs/"+emp.getAdresseMail()+"/Hubo-Planning/Hubo-Transfert";

        if (drop.fileExist(ToTransfert + "/PlanningAnnuel-test-A.xlsx")){
            drop.deleteFichier(ToTransfert + "/PlanningAnnuel-test-A.xlsx");
        }


    }




}
