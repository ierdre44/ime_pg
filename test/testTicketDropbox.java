import com.fasterxml.jackson.databind.JsonNode;
import controllers.DropBoxController;
import models.AbsTicket;
import models.CompteDropbox;
import models.Dropbox.DropInterface;
import models.Entreprise;
import models.Requete;
import models.osTicket.Ticket;
import org.junit.BeforeClass;
import org.junit.Test;
import play.mvc.Result;
import play.test.FakeApplication;
import play.test.Helpers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static play.test.Helpers.contentAsString;

/**
 * Created by richard on 03/06/15.
 */
public class testTicketDropbox {
    private static FakeApplication app;


    @BeforeClass
    public static void startApp() {
        Map<String, String> settings = new HashMap<String, String>();
        settings.put("db.default.url","jdbc:h2:tcp://localhost/bdH2/baseDonnee.db,MODE=PostgreSQL");
        //settings.put("db.default.url","jdbc:h2:mem:play;MODE=PostgreSQL;");

        settings.put("db.default.jndiName", "DefaultDS");
        app = Helpers.fakeApplication(settings);


        Helpers.start(app);


    }
    @Test
    public void getTickets(){

        System.out.println("\n**********************************\n TEST Tickets \n" +
                "**********************************");


       /*  CompteDropbox compte = CompteDropbox.find.all().get(0);
        System.out.println(compte+" -->"+ CompteDropbox.find.all().size());

        ArrayList <AbsTicket>liste = compte.getTicketOpen();
        for ( AbsTicket ticket : liste){
           // System.out.println(ticket.getJson());
            //System.out.println(ticket.files[0]);


            Requete req = new Requete(ticket);
            JsonNode res = req.execute(ticket);

*/
        List<Entreprise> ents= Entreprise.find.all();
        for (Entreprise ent:ents){
            if (ent.getNom().compareToIgnoreCase("IMEPG")!=0 ) continue;
                Result res = new DropBoxController().doCreationDropboxEntreprise(ent.getId());

            System.out.println(contentAsString(res));
        }





    }
}
