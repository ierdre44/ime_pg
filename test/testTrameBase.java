import models.TraitementExcel.ExcelModele;
import models.TraitementExcel.PlanningRoulement;
import models.TraitementExcel.TrameBase;
import org.junit.BeforeClass;
import org.junit.Test;
import play.test.FakeApplication;
import play.test.Helpers;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by richard on 28/04/15.
 */
public class testTrameBase {

    private static FakeApplication app;



    @BeforeClass
    public static void startApp() {
        Map<String, String> settings = new HashMap<String, String>();
        settings.put("db.default.url", "jdbc:h2:tcp://localhost/bdH2/test1_IME");
        settings.put("db.default.jndiName", "DefaultDS");
        app = Helpers.fakeApplication(settings);


        Helpers.start(app);


    }

    /**
     * Ici on teste la résupération de la trame de base construite à
     * partir d'un modèle
     * trame.initFromTemplate();
     * et alimentation à partir des infos de bases
     */
   /* @Test
    public void testParseFile() {
        TrameBase trame = new TrameBase();
        ((ExcelModele) trame).setPathModele("./test/test-A/");
        ((ExcelModele) trame).initFromTemplate();

        //((ExcelModele) trame).saveToFileFromTemplate("/tmp/res.xlsx");

        //System.out.println(trame.getResultat().toString());


    }*/
/*
Ici on test le merge de deux fichiers
la trame de base est enrichi d'information de planing

 */
    @Test
    public void testMergeFile() {
               TrameBase trame = new TrameBase();
        //  ((ExcelModele) trame).setPathModele("trame-Annuel.xlsx");
        // ((ExcelModele) trame).initFromTemplate();
        trame.initFromFile("/var/hubo/users/162/trame-Annuel.xlsx");


        PlanningRoulement plan= new PlanningRoulement();
        plan.validation("/var/hubo/users/162/planning-Roulement.xlsm");


        trame.mergePlanning(plan);




        ((ExcelModele) trame).saveToFileFromTemplate("/tmp/res2.xlsx");

        //System.out.println(trame.getResultat().toString());


    }



}
