import models.TraitementExcel.PlanningCesiFormateur;
import models.TraitementExcel.PlanningRoulement;
import org.junit.BeforeClass;
import org.junit.Test;
import play.test.FakeApplication;
import play.test.Helpers;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by richard on 27/04/15.
 */
public class testPCesi {
    private static FakeApplication app;

    @BeforeClass
    public static void startApp() {
        Map<String, String> settings = new HashMap<String, String>();
        settings.put("db.default.url","postgres://play:play@localhost:5432/IMEPG");
        settings.put("db.default.jndiName", "DefaultDS");
        app = Helpers.fakeApplication(settings);


        Helpers.start(app);


    }
    @Test
    public void testParseFile(){

        PlanningCesiFormateur plan= new PlanningCesiFormateur();
        //plan.validation("/Users/richard/Dropbox/suivie\\ administratif/2015-2016/prevision/PLANNING_INTERVENANTS_RIL_2015-3.xlsx");
        plan.validation("/tmp/test.xlsx");
        System.out.println(plan.getResultat().toString());

    }

}
