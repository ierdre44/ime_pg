
import org.junit.BeforeClass;
import org.junit.Test;
import play.mvc.Result;
import play.test.FakeApplication;
import play.test.Helpers;

import java.util.HashMap;
import java.util.Map;

import static org.fest.assertions.Assertions.assertThat;
import static play.mvc.Results.ok;
import static play.test.Helpers.*;

/**
 * Created by richard on 12/05/15.
 */
public class testRoute {
    private static FakeApplication app;


    @BeforeClass
    public static void startApp() {
        Map<String, String> settings = new HashMap<String, String>();
        settings.put("db.default.url","postgres://play:play@localhost:5432/imepg");
        settings.put("db.default.jndiName", "DefaultDS");
        app = Helpers.fakeApplication(settings);


        Helpers.start(app);


    }
    @Test
    public void rootRoute() {
        Result result = routeAndCall(fakeRequest(GET, "/"));
        assertThat(result).isNotNull();

    }
    @Test
    public void listeTicketRoute() {

        Result result = route(fakeRequest(GET, "/management/opentickets"));
        assertThat(result).isNotNull();
       System.out.println(contentAsString(result));

    }
    @Test
    public void majTicketRoute() {

        Result result = route(fakeRequest(GET, "/management/majtickets"));
        assertThat(result).isNotNull();
        System.out.println(contentAsString(result));

    }


}
