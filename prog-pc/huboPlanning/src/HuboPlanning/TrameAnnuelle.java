package HuboPlanning;

//import jdk.nashorn.internal.ir.ObjectNode;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellReference;

import java.io.*;
import java.util.HashMap;
import java.util.Iterator;

/**
 * Created by richard on 17/08/2015.
 */
class VDay{
    // double valHaut=-1;
    double valBas=0.0;
    String lblHaut="";
    //String lblBas="??";
    boolean isOpen=false;

    @Override
    public String toString(){
        String res="";

        res = res + lblHaut + " "+valBas;

        return res;

    }


    public boolean isAbsence() {
        if (this.lblHaut.toUpperCase().compareTo("TE")==0) return false;
        else return  true;
    }
}
class cellCase {
    int row;
    int col;

    public cellCase(int row, int col) {
        this.row   =row ;
        this.col=col;
    }
}
public class TrameAnnuelle {
    protected static final String TRAME_ANNUELLE ="/Hubo-Planning/"+ "trame-Annuel.xlsx";
    private static final String ISFERIE = "F";

    private  Workbook workbook;
    private long annee;
    private String adresseMail="NO mail";
    private String nom="NO fname";
    private String prenom="NO name";

    private int PREMIER_MOIS=5;
    private int PREMIER_JOUR=1;
    private cellCase CDepartAnnee =new cellCase(0,14);
    private cellCase  CdesCP=new cellCase(30,5);

    CellReference refCellAnnee = new CellReference("O1");


    private cellCase  TypeSamediDimanche=new cellCase(29,0);
    private cellCase  TypeNotExist=new cellCase(30,5);
    private cellCase  TypeFerie=new cellCase(29,5);
    //Identification
    private cellCase  CadresseMail=new cellCase(0,6);
    private cellCase  Cnom=new cellCase(1,6);
    private cellCase  Cprenom=new cellCase(1,14);
    protected HashMap<String,VDay> listeJour;



    public static boolean syncTrameToSemaine(String Path) {
        boolean res=true;
        TrameAnnuelle trame = new TrameAnnuelle(Path);
        // Parcours des semaines pour générer absence dans les semaines
        for (int nbSemaine=0; nbSemaine< 54 ; nbSemaine++){
            PlanningSemaine plSemaine = new  PlanningSemaine(Path,nbSemaine);
            if (!plSemaine.exist) continue;
            //MyUtilLib.affLogInfo(plSemaine.toString());
            plSemaine.syncAbsence(trame);
            plSemaine.syncHoraire(trame);
            plSemaine.save(Path, nbSemaine);
            trame.save(Path);



        }

        return res;
    }


    private void save(String path) {
        majCell();
        try {
            FileOutputStream fileOut = new FileOutputStream(path+TRAME_ANNUELLE);

            workbook.write(fileOut);
            fileOut.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void majCell() {
        Sheet feuille = workbook.getSheetAt(0);
        Iterator<Row> rowIt = feuille.rowIterator();
        FormulaEvaluator evaluator = workbook.getCreationHelper().createFormulaEvaluator();
        // evaluator.evaluateAll();
        /**/
        //Mise à jour celllule de travail
        for (int m=0;m<24; m =m+2) {
            int mois = 0;
            int p = 0;
            if ((m % 2) == 0) {
                mois = (m / 2) + 1;
            } else {
                mois = ((m + 1) / 2) + 1;
            }

            for (int j = 0; j < 31; j++) {


                Row row = feuille.getRow(PREMIER_MOIS + m);
                Row row2 = feuille.getRow(PREMIER_MOIS + m + 1);

                Cell Hcell = row.getCell(j + PREMIER_JOUR);
                Cell Bcell = row2.getCell(j + PREMIER_JOUR);
                VDay dayCrt= this.listeJour.get((j + 1) + "-" + mois+"-"+(int)this.annee);


                Hcell.setCellValue(dayCrt.lblHaut);
                Bcell.setCellValue(0);
                if (dayCrt.lblHaut.length()>0) {

                    Bcell.setCellValue(dayCrt.valBas );
                }

            }
        }
        evaluator.evaluateAll();
       /* while (rowIt.hasNext()){
            Row row = rowIt.next();
            Iterator<Cell> cellIt = row.cellIterator();
            while (cellIt.hasNext()){
                Cell cell=cellIt.next();
                //evaluator.evaluateInCell(cell);

             /*  if (cell.getCellType()==Cell.CELL_TYPE_FORMULA) {
                    cell.setCellValue(evaluator.evaluateFormulaCell(cell));
                   // System.out.println(cell.getRichStringCellValue());
                }*/
        //}

//        }




    }

    public TrameAnnuelle(String Path){



        InputStream inp= null;
        try {
            inp = new FileInputStream(Path+TRAME_ANNUELLE);
            workbook = WorkbookFactory.create(inp);
            inp.close();


            Sheet feuille = workbook.getSheetAt(0);
            this.annee = MyUtilLib.getLongFrom(feuille.getRow(CDepartAnnee.row),CDepartAnnee.col);
            listeJour=new HashMap<String,VDay>();

            for (int i=1; i <=12 ; i++) {
                for (int j=1; j <=31 ; j++) {
                    listeJour.put(j+"-"+i+"-"+(int)this.annee,new VDay());


                }
            }

            Cell cell = feuille.getRow(CdesCP.row).getCell(CdesCP.col);

            Cell c = feuille.getRow(CadresseMail.row).getCell(CadresseMail.col);
            if (c!=null) this.adresseMail= c.getStringCellValue();
            c = feuille.getRow(Cprenom.row).getCell(Cprenom.col);
            if (c!=null) this.prenom= c.getStringCellValue();
            c = feuille.getRow(Cnom.row).getCell(Cnom.col);
            if (c!=null) this.nom= c.getStringCellValue();


            for (int m=0;m<24; m =m+2) {
                int mois=0;
                int p=0;

                mois =((m+1)/2)+1;

                for (int j = 0; j < 31; j++) {

                    Row row = feuille.getRow(PREMIER_MOIS + m);
                    Row row2 = feuille.getRow(PREMIER_MOIS + m+1);

                    Cell Hcell = row.getCell(j+PREMIER_JOUR);
                    Cell Bcell = row2.getCell(j+PREMIER_JOUR);
                    VDay dayCrt= this.listeJour.get((j+1)+"-"+mois+"-"+(int)this.annee);

                    if( (Hcell!= null) && (Hcell.getCellType()==Cell.CELL_TYPE_STRING)){
                        dayCrt.lblHaut= Hcell.getStringCellValue();

                    }

                    if( (Bcell!= null) && (Bcell.getCellType()==Cell.CELL_TYPE_NUMERIC)){
                        dayCrt.valBas=Bcell.getNumericCellValue();

                    }

                    if (dayCrt.lblHaut.compareToIgnoreCase(ISFERIE)==0){
                    }
                    //JOUR VIDE
                    if (dayCrt.lblHaut.length()==0){
                    }

                }
            }






        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (InvalidFormatException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }



    }
}
