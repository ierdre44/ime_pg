package HuboPlanning;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellReference;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;

import java.io.*;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by richard on 17/08/2015.
 */
class IVDay{
    double durePresence=0;
    int jour;
    private boolean absence=false;
    private String motifAbsence="";

    public IVDay(double duree, int day) {
        this.durePresence= duree;
        this.jour= day;
        this.absence=false;
    }


    public void setAbsence(boolean absence) {
        this.absence = absence;
    }

    public boolean getAbsence() {
        return absence;
    }

    public void setMotifAbsence(String motifAbsence) {
        this.motifAbsence = motifAbsence;
    }

    public String getMotifAbsence() {
        return motifAbsence;
    }
}
class CodeCell {
    String backgroundColor="";
    String foregroudColor="";
    Short fillPattern=0;

    CodeCell(String backgroundColor, String foregroudColor, Short fillPattern) {
        this.backgroundColor = backgroundColor;
        this.foregroudColor = foregroudColor;
        this.fillPattern = fillPattern;
    }


}

class IVEvent {
    Date hDebut;
    Date hFin;
    int jour;
    String libelle;
    private double duree;
    //  CodeCell style;

    public IVEvent (double heureDebut,
                    double heureFin,
                    int jour,
                    String libelle
    ){
        this.jour= jour;

        this.libelle= libelle;
        try {
            DateFormat dfm = new SimpleDateFormat("HH:mm");
            double hour=(int) heureDebut;
            double minute =  (heureDebut - hour);
            minute = (int) (minute*60);

            hDebut = dfm.parse((int)hour+":"+(int)minute);

            hour=(int) heureFin;

            minute =  (heureFin - hour);
            minute = (int) (minute*60);

            hFin = dfm.parse((int)hour+":"+(int)minute);
        } catch (ParseException e) {
            e.printStackTrace();
        }


    }



    public double getdureeInMinute() {

        long diff = this.hFin.getTime() - this.hDebut.getTime();
        long diffMinutes = diff / (60 * 1000) % 60;



        return ((double)diffMinutes);
    }
}
public class PlanningSemaine {
    private static final int FIRST_ROW_CODE_COULEUR = 6;
    private static final int LAST_ROW_CODE_COULEUR = 10;
    private static final int COL_CODE_COULEUR = 3;

    protected cellCase CdateDebutApplication=new cellCase(1,1);
    protected cellCase CdateFinApplication=new cellCase(2,1);
    protected cellCase  CdateDebut=new cellCase(1,1);
    protected cellCase  CdateFin=new cellCase(2,1);


    //TOtALE
    CellReference refCellTolale = new CellReference("BV5");
    CellReference refCellCompensationAbsence = new CellReference("BR5");


    private cellCase  CID=new cellCase(0,1);
    protected cellCase  CdateSemaine=new cellCase(3,1);
    private HashMap<String, CodeCell> listeLibelle;
    private ArrayList<IVEvent> listVEvent;

    private ArrayList<IVDay> listVDAY;
    private int posColonneHeure = -1;
    private int valHeureDepart = -1;
    private int colFinHeure = -1;
    private String Prenom;
    private String Nom;
    private double idLu;
    protected Date DateDebut;
    protected Date DateFin;
    private String AdresseMail = "";
    ///**************
    private String fonction;
    private String service;
    private double quotite;
    private double baseHoraire=39;
    public long semaine;
    public boolean exist=true;
    //private String absence;

    public String toString() {
        String res = "";
        res = res + "\n----------------------------------------\n";
        res = res + Prenom + "|" + Nom + "|" + semaine + "|" + "\n"
                + "|" + DateDebut + "|" + DateFin + "\n";
        res = res + "----------------------------------------\n";
        for (IVDay vDay : listVDAY) {
            res = res + vDay.jour + " | " + vDay.durePresence + " " + vDay.getAbsence() + "\n";
        }

        res = res + "----------------------------------------\n";

        return res;

    }



    public String getPrenom() {
        return Prenom;
    }

    public void setPrenom(String prenom) {
        Prenom = prenom;
    }

    public String getNom() {
        return Nom;
    }

    public void setNom(String nom) {
        Nom = nom;
    }

    public void setFonction(String fonction) {
        this.fonction = fonction;
    }

    public void setService(String service) {
        this.service = service;
    }

    public void setQuotite(double quotite) {
        this.quotite = quotite;
    }

    public double getBaseHoraire() {
        return baseHoraire;
    }

    public void setBaseHoraire(double baseHoraire) {
        this.baseHoraire = baseHoraire;
    }


    private Workbook workbook;

    public PlanningSemaine(String Path, int nbSemaine) {
        InputStream inp = null;
        listeLibelle = new HashMap<String, CodeCell>();
        listVEvent = new ArrayList<IVEvent>();
        listVDAY = new ArrayList<IVDay>();

        try {
            File pl = new File(Path + "/Hubo-Planning/planning-semaine-" + nbSemaine + ".xlsm");
            if (!pl.exists()) {
                this.exist=false;
                return ;

            }
            inp = new FileInputStream(Path + "/Hubo-Planning/planning-semaine-" + nbSemaine + ".xlsm");
            workbook = WorkbookFactory.create(inp);
            inp.close();
            Sheet feuille = workbook.getSheet("roulement");
            this.semaine = MyUtilLib.getLongFrom(feuille.getRow(CdateSemaine.row),CdateSemaine.col);
            this.DateDebut=MyUtilLib.formatDateFromString(feuille.getRow(CdateDebut.row).getCell(CdateDebut.col).getStringCellValue());
            this.DateFin=MyUtilLib.formatDateFromString(feuille.getRow(CdateFin.row).getCell(CdateFin.col).getStringCellValue());




            for (Row row : feuille) {
                for (Cell cell : row) {
                    if (cell.getCellType() == Cell.CELL_TYPE_STRING) {

                        String valCourante = cell.getStringCellValue();
                        //GEt cell font  color pour les codes
                        if ((row.getRowNum() >= FIRST_ROW_CODE_COULEUR) &&
                                (row.getRowNum() <= LAST_ROW_CODE_COULEUR)) {
                            if (cell.getColumnIndex() == COL_CODE_COULEUR) {

                                XSSFCellStyle style = (XSSFCellStyle) cell.getCellStyle();

                                CodeCell codecrt = new CodeCell(this.getFillBackgroundColorColor(style),
                                        this.getFillForegroundColorColor(style),
                                        style.getFillPattern());
                                this.listeLibelle.put(cell.getStringCellValue(), codecrt);


                            }

                        }
                        if (cell.getStringCellValue().contains("Mail")) {
                            Cell celltmp=row.getCell(cell.getColumnIndex() + 1);
                            this.AdresseMail=celltmp.getStringCellValue();
                        }
                        if (cell.getStringCellValue().contains("Nom")) {
                            Cell celltmp=row.getCell(cell.getColumnIndex() + 1);
                            this.Nom=celltmp.getStringCellValue();
                        }
                        if (cell.getStringCellValue().contains("Prénom")) {
                            Cell celltmp=row.getCell(cell.getColumnIndex() + 1);
                            this.Prenom=celltmp.getStringCellValue();
                        }
                        if (cell.getStringCellValue().contains("Emploi")) {
                            Cell celltmp=row.getCell(cell.getColumnIndex() + 1);
                            this.fonction=celltmp.getStringCellValue();
                        }
                        if (cell.getStringCellValue().contains("ETP")) {
                            this.quotite = MyUtilLib.getLongFrom(row, cell.getColumnIndex() + 1);
                        }
                        //identification
                        if (cell.getStringCellValue().endsWith("#ID#")) {
                            Cell idcell = row.getCell(cell.getColumnIndex() + 1);
                            this.idLu = MyUtilLib.getLongFrom(row, cell.getColumnIndex() + 1);
                            // JE considere cette case avec le style vide utilisé comme référence
                            XSSFCellStyle style = (XSSFCellStyle) cell.getCellStyle();
                            CodeCell codecrt = new CodeCell(this.getFillBackgroundColorColor(style),
                                    this.getFillForegroundColorColor(style),
                                    style.getFillPattern());
                            this.listeLibelle.put("NULL", codecrt);

                        }
                        if (cell.getStringCellValue().contains("Base  planning")) {
                            this.baseHoraire=MyUtilLib.getLongFrom(row, cell.getColumnIndex()+1);
                        }
                        if (cell.getStringCellValue().toUpperCase().contains("#DEBUT#")) {
                            Cell celltmp=row.getCell(cell.getColumnIndex() + 1);

                            this.posColonneHeure = cell.getColumnIndex()+1
                            ;
                            this.valHeureDepart = Integer.parseInt((String) (celltmp.getStringCellValue().split("h")[0]));

                        }
                        if (cell.getStringCellValue().toUpperCase().contains("#FIN#")) {
                            this.colFinHeure = cell.getColumnIndex()
                            ;
                        }


                    }


                }
            }


            // IDENTIFICATION CELLULE HORAIRE

            double duree = 0;
            int pos=0;
           // for (Row row : feuille) {
            for (int r=FIRST_ROW_CODE_COULEUR ; r<=FIRST_ROW_CODE_COULEUR + 4;r++) {
                Row row = feuille.getRow(r) ;
                int day = 0;
                String libelleRef = "NULL";
                int offset = -1;
                duree = 0;
                for ( int col=COL_CODE_COULEUR+1;col<= this.colFinHeure; col++ ){
                    Cell cell = row.getCell(col);
             //   for (Cell cell : row) {
                    //Style
                    if (cell != null) {

                        // Cadre des cases
                        if ((row.getRowNum() >= FIRST_ROW_CODE_COULEUR) &&
                                (row.getRowNum() <= FIRST_ROW_CODE_COULEUR + 4) &&
                                (cell.getColumnIndex() <= this.colFinHeure) &&
                                (cell.getColumnIndex() > COL_CODE_COULEUR)) {
                            XSSFCellStyle style2=null;
                             style2 =  (XSSFCellStyle)cell.getCellStyle();
                            String libelleCodeCrt = this.getStyleName(style2);
                            // Libelle n'est pas un libelle vide
                            Cell cNAme = row.getCell(2);
                            double heure = (this.valHeureDepart + (double) (cell.getColumnIndex() - (FIRST_ROW_CODE_COULEUR)) / 4) + 0.5;

                            if (libelleCodeCrt.compareTo("NULL") == 0) {

                                if (libelleRef.compareTo("NULL") != 0) {
                                    double heureDebut = (this.valHeureDepart + (double) (offset - (FIRST_ROW_CODE_COULEUR)) / 4) + 0.5;

                                    IVEvent vevent = new IVEvent(heureDebut, heure, MyUtilLib.getNumDayFromString(cNAme.getStringCellValue()), libelleRef);

                                    day = vevent.jour;
                                    listVEvent.add(vevent);

                                }
                                libelleRef = "NULL";
                                offset = -1;

                            } else {
                                duree += 15;
                                // On continue avec le même intervalle
                                if (libelleRef.compareTo(libelleCodeCrt) == 0) {
                                }
                                // debut
                                if (libelleRef.compareTo("NULL") == 0) {
                                    offset = cell.getColumnIndex();
                                    libelleRef = libelleCodeCrt;
                                }
                                // debut et fin
                                if (libelleCodeCrt.compareTo(libelleRef) != 0) {
                                    double heureDebut = (this.valHeureDepart + (double) (offset - (FIRST_ROW_CODE_COULEUR)) / 4) + 0.5;

                                    IVEvent vevent = new IVEvent(heureDebut, heure, MyUtilLib.getNumDayFromString(cNAme.getStringCellValue()), libelleRef);
                                    day = vevent.jour;
                                    listVEvent.add(vevent);




                                }

                            }
                        }

                    }

                }

                //if (duree > 0) {
                    IVDay vd = new IVDay(duree / 60, day);
                    this.listVDAY.add(vd);
                    day = 0;
                    duree = 0;
                //}
               // System.out.println(this.listVDAY.size());



            }
            int a = 0;

        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        } catch (InvalidFormatException ex) {
            ex.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
//TEst si le style de la case (le fond ) est différend d'un blanc
    //Si oui c'est une case marqué pour le travail
    private String getStyleName(XSSFCellStyle style) {
        String res;
        if (style.getFillForegroundXSSFColor()==null) return "NULL";
        if (style.getFillForegroundXSSFColor().getARGBHex()==null) return "NULL";
        if (style.getFillForegroundXSSFColor().getARGBHex().compareTo("FFFFFFFF")==0  ) return "NULL";
        if (style.getFillForegroundXSSFColor().getARGBHex().compareTo("FFFF8080")==0  ) return "NULL";
        return "WORK";





    }

    private String getFillForegroundColorColor(XSSFCellStyle style) {
        if (style.getFillForegroundColorColor() == null)
            return null;
        else return style.getFillForegroundColorColor().getARGBHex();
    }

    private String getFillBackgroundColorColor(XSSFCellStyle style) {
        if (style.getFillBackgroundColorColor() == null)
            return null;
        else return style.getFillBackgroundColorColor().getARGBHex();
    }

    public void syncAbsence(TrameAnnuelle trame) {
        MyUtilLib.affLogInfo("-----Sync absence---------" + this.semaine);
        GregorianCalendar cal = new GregorianCalendar(TimeZone.getTimeZone("Europe/Paris"));
        cal.setTime(this.DateDebut);
        for (int dec = 0; dec < 5; dec++) {
            int jourDeLaSemaine = cal.get(GregorianCalendar.DAY_OF_WEEK);
            dec = jourDeLaSemaine-2;
            //System.out.println(jourDeLaSemaine);
            Cell totale = this.workbook.getSheetAt(0).getRow(refCellTolale.getRow()+dec+2).getCell(refCellTolale.getCol());
            Cell compensationAbsence= this.workbook.getSheetAt(0).getRow(refCellCompensationAbsence.getRow()+dec+2).getCell(refCellCompensationAbsence.getCol());
            int day = cal.get(GregorianCalendar.DAY_OF_MONTH);
            int month = cal.get(GregorianCalendar.MONTH) + 1;
            int year = cal.get(GregorianCalendar.YEAR);

            VDay dayCrt = trame.listeJour.get(day + "-" + month + "-"+year);
            if (dayCrt == null) continue;


            totale.setCellValue(this.listVDAY.get(dec).durePresence);
            this.resetAbsenceAffiChage(dec);

            if (dayCrt.isAbsence()) {
                this.listVDAY.get(dec).setAbsence(true);
                this.listVDAY.get(dec).setMotifAbsence(dayCrt.lblHaut);
                // A préciser dans le cas de changement de format


                // Modifie le format d'affichage
                if (dayCrt.valBas>0 && (dayCrt.lblHaut.compareToIgnoreCase("R")==0)){
                    compensationAbsence.setCellValue(0);

                    //compensationAbsence.setCellValue(dayCrt.valBas);
                }

                    else {
                    compensationAbsence.setCellValue(0);
                }

                this.setAbsenceAffiChage(dec);

                //


            }
          //  MyUtilLib.affLogInfo(jourDeLaSemaine + " " + day + " " + month + "  " + trame.listeJour.get(day + "-" + month + "-" + year) +
            //        " " + this.listVDAY.get(dec).jour + " " + this.listVDAY.get(dec).getAbsence());

            cal.set(GregorianCalendar.DAY_OF_MONTH, cal.get(GregorianCalendar.DAY_OF_MONTH) + 1);
        }


    }

    private void resetAbsenceAffiChage(int dec) {
        Sheet feuille = workbook.getSheetAt(0);

        Row row = feuille.getRow(dec + 6);
        for (int col = this.posColonneHeure + 1; col < this.colFinHeure; col++) {
            Cell cell = row.getCell(col);
            cell.setCellValue("");
            CellStyle style = cell.getCellStyle();
          //  style.setBorderRight(XSSFCellStyle.BORDER_MEDIUM_DASH_DOT_DOT);
           // style.setBorderLeft(XSSFCellStyle.BORDER_MEDIUM_DASH_DOT_DOT);
        }
    }

    private void setAbsenceAffiChage(int dec) {
        Sheet feuille = workbook.getSheetAt(0);
        Row row = feuille.getRow(dec + 6);
        for (int col = this.posColonneHeure + 1; col < this.colFinHeure; col++) {
            Cell cell = row.getCell(col);
            cell.setCellValue("");
            CellStyle style = cell.getCellStyle();
            //style.setWrapText(true);

            style.setAlignment(XSSFCellStyle.ALIGN_CENTER);
            Font font2 = workbook.createFont();
            font2.setFontHeightInPoints((short) 9);
            font2.setFontName("Arial");

            font2.setItalic(true);
            font2.setColor((short) 7);
            style.setFont(font2);

            if (col%2 == 0) cell.setCellValue(listVDAY.get(dec).getMotifAbsence());
             //style.setBorderBottom(XSSFCellStyle.BORDER_MEDIUM);
             //style.setBorderTop(XSSFCellStyle.BORDER_MEDIUM);
           //  style.setBorderRight(XSSFCellStyle.BORDER_NONE);
            // style.setBorderLeft(XSSFCellStyle.BORDER_NONE);

        }

    }


    public void save(String path, int nbSemaine) {

        try {
            //FormulaEvaluator evaluator = workbook.getCreationHelper().createFormulaEvaluator();
            //evaluator.evaluateAll();
            FileOutputStream fileOut = new FileOutputStream(path + "/Hubo-Planning/planning-semaine-" + nbSemaine + ".xlsm");

            workbook.write(fileOut);
            fileOut.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void syncHoraire(TrameAnnuelle trame) {
        MyUtilLib.affLogInfo("-------Sync horaire-----" + this.semaine);
        GregorianCalendar cal = new GregorianCalendar(TimeZone.getTimeZone("Europe/Paris"));
        cal.setTime(this.DateDebut);
        for (int dec = 0; dec < 5; dec++) {
            int day = cal.get(GregorianCalendar.DAY_OF_MONTH);
            int month = cal.get(GregorianCalendar.MONTH) + 1;
            int year = cal.get(GregorianCalendar.YEAR);
            int jourDeLaSemaine = cal.get(GregorianCalendar.DAY_OF_WEEK);
            VDay dayCrt = trame.listeJour.get(day + "-" + month + "-"+year);
            IVDay dayPl = this.listVDAY.get(dec);
            if (dayCrt!=null) { //date d'une autre année
                if (dayPl.getAbsence()) {

                }
                //TE
                else {
                    if (dayCrt.lblHaut.compareToIgnoreCase("TE") == 0) {
                        dayCrt.valBas = dayPl.durePresence;
                    }
                }
            }




            cal.set(GregorianCalendar.DAY_OF_MONTH, cal.get(GregorianCalendar.DAY_OF_MONTH) + 1);



        }

    }
}
