package HuboPlanning;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;

//import java.awt.*;
import java.util.ArrayList;

class monTraitement implements Runnable{
    //   public final static String PATH_TO_HUBO_HOME = "/Users/richard/Dropbox/employes/";
    //  public final static String PATH_TO_HUBO_HOME = "/tmp/test/";

    // public static final String PATH_TO_EMP="/Users/richard/Dropbox/richard.urunuela@gmail.com/";
    public final static String PATH_TO_HUBO_HOME = "../employes/";

    public static final String PATH_TO_EMP="./";

    private final ProgressIndicator progress;

    public monTraitement(ProgressIndicator p1) {
        // p1.setProgress(10);
        this.progress = p1;
    }

    @Override
    public void run() {
        //Procèdure
        /***
         * Répertoires home
         */
        /***
         * Check si il existe bien les différents fichiers
         */

        // get Liste employe gere par l'adminitrateur
        ArrayList<String>  listeEmploye=MyUtilLib.ListeEmployeGererParAdmin(PATH_TO_EMP);

        // Pour chaque employe on effectue traitement

        for(String path:listeEmploye){
            String empCrtHome=MyUtilLib.checkRepertoireOK(PATH_TO_HUBO_HOME,path);
            //
            boolean repertoireOK= empCrtHome.length()>1;
            //
            if (repertoireOK) {
                MyUtilLib.affLogInfo("Empl Home OK pour :"+path);
            }
            else {
                MyUtilLib.affLogError("Empl Home NOK pour :"+path);
                continue;

            }
            boolean trameAnnuelleOk=MyUtilLib.checkTrameAnnuel(PATH_TO_HUBO_HOME+empCrtHome+TrameAnnuelle.TRAME_ANNUELLE);
            if (trameAnnuelleOk) {
                MyUtilLib.affLogInfo("Empl Trame OK pour :"+path);
            }
            else {
                MyUtilLib.affLogError("Empl Trame NOK pour :" + path);
                continue;
            }

            /*****
             * Puch CP de trame vers Semaine
             */
            boolean SyncAbsenceOk=TrameAnnuelle.syncTrameToSemaine(PATH_TO_HUBO_HOME + empCrtHome);



        }
        if (progress!=null) {
            Platform.runLater(new Runnable() {

                @Override
                public void run() {

                    progress.setProgress(1);
                }
            });

        }
        else {
            System.exit(0);
        }


    }
}
public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("sample.fxml"));
        primaryStage.setTitle("Hubo Sync Planning");
        ProgressIndicator p1 = new ProgressIndicator();
        p1.setProgress(-1);
        Scene scene = new Scene(root, 150, 100);

        // scene.getStylesheets().add("./style.css");
        final VBox hb=new VBox();
        hb.setAlignment(Pos.CENTER);
        Label label1 = new Label();
        label1.setText(" HUBO ");
        label1.setTextAlignment(TextAlignment.CENTER);

        label1.setAlignment(Pos.CENTER);
        /*FlowPane flow = new FlowPane();
        flow.setPadding(new Insets(0, 1, 0, 1));
        flow.setVgap(4);
        flow.setHgap(4);
        flow.setPrefWrapLength(250); // preferred width allows for two columns
       // flow.setStyle("-fx-background-color: DAE6F3;");
        //hb.getChildren().add(p1);

        flow.getChildren().add(p1);
        flow.getChildren().add(label1);
        hb.getChildren().add(flow);*/
        hb.getChildren().addAll(label1,p1);


        scene.setRoot(hb);
        primaryStage.setScene(scene);
        primaryStage.show();
        primaryStage.setResizable(false);
        Thread T=new Thread(new monTraitement(p1));
        T.start();







    }


    public static void main(String[] args) {
        if (args.length>0) {
            if (args[0].compareToIgnoreCase("-noUI") == 0) {
                Thread T = new Thread(new monTraitement(null));
                T.start();
            }
        }
        else {
            launch(args);
        }
    }
}
