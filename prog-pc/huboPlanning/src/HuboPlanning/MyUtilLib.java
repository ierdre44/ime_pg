package HuboPlanning;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.ss.usermodel.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by richard on 17/08/2015.
 */
public class MyUtilLib {

    // Couleur Format
    public static final String ANSI_RESET = "\u001B[0m";
    public static final String ANSI_BLACK = "\u001B[30m";
    public static final String ANSI_RED = "\u001B[31m";
    public static final String ANSI_GREEN = "\u001B[32m";
    public static final String ANSI_YELLOW = "\u001B[33m";
    public static final String ANSI_BLUE = "\u001B[34m";
    public static final String ANSI_PURPLE = "\u001B[35m";
    public static final String ANSI_CYAN = "\u001B[36m";
    public static final String ANSI_WHITE = "\u001B[37m";
    ///
    private static final int POS_ADRESSE_MAIL_LISTE_EMPLOYE = 0 ;
    private static final int POS_NOM_LISTE_EMPLOYE = 0;
    private static final int POS_PRENOM_LISTE_EMPLOYE = 1;

    public static void  affLogInfo(String info){
        System.out.println(ANSI_BLUE+"[INFO] "+info);
    }

    public static void affLogError(String info) {
        System.out.println(ANSI_RED+"[ERROR] "+info);

    }

    public static Long getLongFrom(Row row, int i) {
        long res ;
        if (row.getCell(i)==null) return 0l;
        try {
            res = (long)row.getCell(i).getNumericCellValue();
        }
        catch (java.lang.IllegalStateException E){
            if (row.getCell(i).getStringCellValue().length()==0) return 0l;
            res = Long.parseLong(row.getCell(i).getStringCellValue());
        }
        return res;
    }

    public static Date formatDateFromString(String date) {
        java.util.Calendar cal = java.util.Calendar.getInstance();
        Date parsedDate = null;

        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        try {
            parsedDate = formatter.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return parsedDate;

    }
    public static ArrayList<String> ListeEmployeGererParAdmin(String pathToEmp) {

        ArrayList<String> res = new ArrayList<String>();
        try {

            OPCPackage pkg = null;
            String nom="";
            String prenom="";
            InputStream inp=new FileInputStream(pathToEmp+"/liste_employes.xlsx");
            Workbook workbook = WorkbookFactory.create(inp);
            inp.close();
            Sheet feuille = workbook.getSheetAt(0);
            boolean hasLine=true;
            int line = 0;
            while (hasLine) {
                if (feuille.getRow(line) == null) {
                    hasLine = false;
                    continue;
                }
                Cell cellNom = feuille.getRow(line).getCell(POS_NOM_LISTE_EMPLOYE);
                if (cellNom!= null) {
                    nom = cellNom.getStringCellValue();

                }
                Cell cellPrenom= feuille.getRow(line).getCell(POS_PRENOM_LISTE_EMPLOYE);
                if (cellNom!= null) {
                    prenom = cellPrenom.getStringCellValue();
                }
                affLogInfo(nom+"-"+prenom);
                res.add(nom+"-"+prenom);
                line++;
            }



        } catch (IOException e) {
            e.printStackTrace();
        } catch (InvalidFormatException e) {
            e.printStackTrace();
        }
            // init onglet de dico agenda -> entreprise


        return res;
    }

    public static String  checkRepertoireOK(String pathToHuboHome,String empStringId) {
        // Cherche repertoires du salarié
        String res="";
        File folder = new File(pathToHuboHome);
        File[] listOfFiles = folder.listFiles();
        for (File file :listOfFiles) {
            if (file.isDirectory()){
                if (file.getName().toUpperCase().contains(empStringId.toUpperCase())){
                    res= file.getName();
                }

            }
        }
        return res;
        // Check si on a le fichier de trame annuelle



    }

    public static boolean checkTrameAnnuel(String empCrtHomeTrame) {
        boolean res = true;
        File trame = new File(empCrtHomeTrame);
        if (trame.exists()) return true;
        else return  false;

    }
    public static int getNumDayFromString(String stringCellValue) {
        if (stringCellValue.contains("lundi")) return 2;
        if (stringCellValue.contains("Lundi") ) return 2;
        if (stringCellValue.contains("mardi")) return 3;
        if (stringCellValue.contains("Mardi") ) return 3;
        if (stringCellValue.contains("Mercredi") ) return 4;
        if (stringCellValue.contains("mercredi") ) return 4;
        if (stringCellValue.contains("Jeudi") ) return 5;
        if (stringCellValue.contains("jeudi") ) return 5;
        if (stringCellValue.contains("Vendredi") ) return 6;
        if (stringCellValue.contains("vendredi") ) return 6;
        if (stringCellValue.contains("Samedi")) return 7;
        if (stringCellValue.contains("samedi")) return 7;
        if (stringCellValue.contains("dimanche") ) return 1;
        if (stringCellValue.contains("Dimanche") ) return 1;

        return -1;
    }
}
