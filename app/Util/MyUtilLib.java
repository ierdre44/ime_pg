package Util;

import Util.MonMail;
import models.AbsTicket;
import models.osTicket.Ticket;
import models.osTicket.osticketConnector;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFClientAnchor;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFClientAnchor;
import org.apache.poi.xssf.usermodel.XSSFRichTextString;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by richard on 22/09/14.
 */
public class MyUtilLib {

    public static final long TYPE_FICHIER_COURS_DEMO = 8001l;

    public static final long TYPE_FICHIER_ENTREPRISE_ID = 4100l;
    public static final long TYPE_FICHIER_SALARIE_ID = 5100l;

    public static final long TYPE_FICHIER_PLANNING_ID = 6100l;
    public static final long TYPE_FICHIER_PLANNING_CONTRAT_ID = 7100l;
    public static final long TYPE_FICHIER_PLANNING_SALARIE_ID = 9100l;
    public static final long TYPE_FICHIER_PLANNING_DATA_ID = 3100l;


    //public static String hostAdresse="localhost";
    //public static String hostAdresse="www.";
    public static MOIS getMois(int i){

        return  MOIS.values()[i];
    }

    public static Long getLongFrom(Row row, int i) {
        long res ;
        try {
            res = (long)row.getCell(i).getNumericCellValue();
        }
        catch (java.lang.IllegalStateException E){
            res = Long.parseLong(row.getCell(i).getStringCellValue());
        }
        return res;
    }

    public static long getLongInString(String title) {
        String motif = "(.*?)*#\\{(.*?)\\}#.*?";
        Pattern pattern = Pattern.compile(motif);
        Matcher matcher = pattern.matcher(title.trim());
        long id= -1;
        if (matcher.find()){ //While
            id = Long.parseLong(matcher.group(2));
        }

        return id;
    }

    // Différnce en jours entre deux dates
    public static double getDaysBetweenDates(Date theEarlierDate, Date theLaterDate) {
        double result = Double.POSITIVE_INFINITY;
        if (theEarlierDate != null && theLaterDate != null) {
            final long MILLISECONDS_PER_DAY = 1000 * 60 * 60 * 24;
            Calendar aCal = Calendar.getInstance();
            aCal.setTime(theEarlierDate);
            long aFromOffset = aCal.get(Calendar.DST_OFFSET);
            aCal.setTime(theLaterDate);
            long aToOffset = aCal.get(Calendar.DST_OFFSET);
            long aDayDiffInMili = (theLaterDate.getTime() + aToOffset) - (theEarlierDate.getTime() + aFromOffset);
            result = ((double) aDayDiffInMili / MILLISECONDS_PER_DAY);
        }
        return result;
    }



    //retourne l'id d'un onglet excel
    public static long getOngletId(Sheet sheet) {
        long res = -1;
        Iterator<Row> rowIterator = sheet.iterator();
        Row row = null;


        while(rowIterator.hasNext()) {
            row = rowIterator.next();
            Iterator<org.apache.poi.ss.usermodel.Cell> cellIterator = row.cellIterator();

            while (cellIterator.hasNext()) {
                org.apache.poi.ss.usermodel.Cell cell = cellIterator.next();
                if (cell.getCellType() == Cell.CELL_TYPE_STRING) {
                    if (cell.getStringCellValue().compareToIgnoreCase("#ID#") == 0) {
                        cell = cellIterator.next();
                        if (cell != null) {
                            if (cell.getCellType() == Cell.CELL_TYPE_STRING) {
                                res = Long.parseLong(cell.getStringCellValue());

                            }
                            if (cell.getCellType() == Cell.CELL_TYPE_NUMERIC) {
                                res = (long) cell.getNumericCellValue();
                            }
                            return res;
                        }


                    }
                }
            }
        }


        return res;
    }
// Check si une cell est valide contient une chaine et bien une chaine de char

    public static boolean checkIfCellChaineValide(Cell cell) {
        boolean res=true;

        if (cell != null ){
            if (cell.getCellType() == Cell.CELL_TYPE_STRING) {


            }
            else {
                res = false;
            }

        }
        else {
            res = false;
        }

        return res;
    }

        public static Comment createComment(String s, Sheet sheet) {
            Drawing patr = sheet.createDrawingPatriarch();
            Comment comment1 = null;
            try {
                comment1 = patr.createCellComment(new HSSFClientAnchor(0, 0, 0, 0, (short) 4, 2, (short) 6, 5));

                comment1.setString(new HSSFRichTextString(s));
            }
            catch (ClassCastException E){
                System.out.println(" PB Format");
                comment1 = patr.createCellComment(new XSSFClientAnchor(0, 0, 0, 0, (short) 4, 2, (short) 6, 5));
                comment1.setString(new XSSFRichTextString(s));

            }
            return comment1;
        }

    public static CellStyle createStyleWarning(Workbook workbook) {
        CellStyle style = workbook.createCellStyle();
        style.setFillForegroundColor(IndexedColors.LIGHT_GREEN.getIndex());
        style.setFillPattern(CellStyle.SOLID_FOREGROUND);
        return style;

    }

    public static CellStyle createStyleBlueRTTP(Workbook workbook) {
        CellStyle style = workbook.createCellStyle();
        style.setFillForegroundColor(IndexedColors.BLUE_GREY.getIndex());
        style.setFillPattern(CellStyle.BORDER_DASHED);
        style.setBorderBottom(HSSFCellStyle.BORDER_MEDIUM);
        style.setBorderTop(HSSFCellStyle.BORDER_MEDIUM);
        style.setBorderRight(HSSFCellStyle.BORDER_MEDIUM);
        style.setBorderLeft(HSSFCellStyle.BORDER_MEDIUM);
        return style;

    }
    public static CellStyle createStyleNoTEPourPlanning(Workbook workbook) {
        CellStyle style = workbook.createCellStyle();
        style.setFillForegroundColor(IndexedColors.CORAL.getIndex());
        style.setFillPattern(CellStyle.BIG_SPOTS);
        return style;


    }
    /**
     * CONGE DANS PLANNIG hebdo
     * @param cell
     * @return
     */

    public static boolean isStyleNoTEPourPlanning( Cell cell){
        boolean res=false;
        CellStyle style = cell.getCellStyle();
       // System.err.println((style.getFillForegroundColor()==IndexedColors.LIGHT_ORANGE.getIndex()) + " \n" +
         //               (style.getFillPattern()==CellStyle.BORDER_DOTTED)+"\n-----------\n");

        res =  (style.getFillForegroundColor()==IndexedColors.CORAL.getIndex()) &&
                (style.getFillPattern()==CellStyle.BIG_SPOTS);


        return res;
    }


    public static CellStyle createStyleOrangeCPP(Workbook workbook) {
        CellStyle style = workbook.createCellStyle();
        style.setFillForegroundColor(IndexedColors.LIGHT_ORANGE.getIndex());
        style.setFillPattern(CellStyle.BORDER_DOTTED);
        style.setBorderBottom(HSSFCellStyle.BORDER_MEDIUM);
        style.setBorderTop(HSSFCellStyle.BORDER_MEDIUM);
        style.setBorderRight(HSSFCellStyle.BORDER_MEDIUM);
        style.setBorderLeft(HSSFCellStyle.BORDER_MEDIUM);
        return style;

    }


    public static Workbook createWorkBook(String fileName) throws IOException {
        Workbook workbook=null; //<-Interface, accepts both HSSF and XSSF.
       /* File file = new File(fileName);
        if (FilenameUtils.getExtension(fileName).equalsIgnoreCase("xls")) {
            workbook = new HSSFWorkbook(new FileInputStream(file));
        } else if (FilenameUtils.getExtension(fileName).equalsIgnoreCase("xlsx")) {
            workbook = new XSSFWorkbook(new FileInputStream(file));
        } else {
            throw new IllegalArgumentException("Received file does not have a standard excel extension.");
        }

        return workbook;
        */
        try {
            return   WorkbookFactory.create(new File(fileName));
        } catch (InvalidFormatException e) {
            e.printStackTrace();
        }

        return workbook;
    }

    public static double getDoubleFrom(Row row, int i) {
        double res ;
        try {
            res = (double)row.getCell(i).getNumericCellValue();
        }
        catch (java.lang.IllegalStateException E){
            res = Double.parseDouble(row.getCell(i).getStringCellValue());
        }
        return res;
    }

    public static Date formatDateFromString(String date) {
        java.util.Calendar cal = java.util.Calendar.getInstance();
        Date parsedDate = null;

        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        try {
            parsedDate = formatter.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return parsedDate;

    }

    public static int getNumDayFromString(String stringCellValue) {
        if (stringCellValue.contains("lundi")) return 2;
        if (stringCellValue.contains("Lundi") ) return 2;
        if (stringCellValue.contains("mardi")) return 3;
        if (stringCellValue.contains("Mardi") ) return 3;
        if (stringCellValue.contains("Mercredi") ) return 4;
        if (stringCellValue.contains("mercredi") ) return 4;
        if (stringCellValue.contains("Jeudi") ) return 5;
        if (stringCellValue.contains("jeudi") ) return 5;
        if (stringCellValue.contains("Vendredi") ) return 6;
        if (stringCellValue.contains("vendredi") ) return 6;
        if (stringCellValue.contains("Samedi")) return 7;
        if (stringCellValue.contains("samedi")) return 7;
        if (stringCellValue.contains("dimanche") ) return 1;
        if (stringCellValue.contains("Dimanche") ) return 1;

        return -1;
    }

    public static String formatTime(Date hDebut) {
        SimpleDateFormat dt = new SimpleDateFormat("HH:mm");

        return (dt.format(hDebut));
    }
//TO DO
    // Changer de structure de données de façon à
    // gérer plusieurs sheet dans un fichier
    // Pour l'instant un numero de sheet un fichier
    // Attention même si un document contient plusieurs sheet il n'a qu'un numéro ....
    // Traitant pour un document non pas la feuille seule .....

    public static  void getSheetConnus(AbsTicket ticket) {
        ticket.listesheet= new double[ticket.nombreFichier*2];
        // Creation fichier temporaire
        if (!ticket.virtual) {
            String files[] = osticketConnector.generateFichier(ticket.listesheet, ticket.idBase, ticket.threadnum);
            ticket.setFile(files);
            for (int i = 0; i < ticket.nombreFichier; i++) {
                //BUG ???
                ticket.listesheet[(i * 2) + 1] = checExcelId(files[i]);
            }
        }
        else {
            String files[] = ticket.generateFile();
            System.out.println("ticket Virtuel ");
            for (int i=0;i< ticket.files.length; i++) {
                ticket.listesheet[(i * 2) + 1] = checExcelId(ticket.files[i]);

            }
        }




    }




    //Mois

    public enum MOIS{
        JANVIER(0),
        FEVRIER(1),
        MARS(2),
        AVRIL(3),
        MAI(4),JUIN(5) , JUILLET(6), AOUT(7) , SEPTEMBRE(8), OCTOBRE (9),NOVEMBRE(10), DECEMBRE(11);


        int num;

        MOIS(int i) {
            this.num=i;
        }
        public int getNum(){
            return this.num;
        }

    };
    //Met les heures des dates à 00 :( car pb lors des comparaisons des heures


    public static Date clearHour(Date dateReference) {
        Calendar dateCourante = Calendar.getInstance();
        dateCourante.setTime(dateReference);
        return (clearHour(dateCourante)).getTime();
    }
    public static Calendar clearHour(Calendar dateReference) {
        // dateReference.clear(Calendar.HOUR_OF_DAY);
        //dateReference.clear(Calendar.HOUR);
        dateReference.set(Calendar.HOUR,0);
        dateReference.set(Calendar.HOUR_OF_DAY,0);
        dateReference.clear(Calendar.MINUTE);
        dateReference.clear(Calendar.SECOND);
        dateReference.clear(Calendar.MILLISECOND);
        return dateReference;
    }

    public static String formatDate(Date d) {

        SimpleDateFormat dt = new SimpleDateFormat("dd/MM/yyyy");

        return (dt.format(d));
    }
//GET DATE FROM JJ MM AA
    public static Date formatDate(String jj,String mm,String aa) {


        Calendar dateCourante = clearHour(Calendar.getInstance());
        dateCourante.set(Calendar.DAY_OF_MONTH,new Integer(jj));
        dateCourante.set(Calendar.MONTH, new Integer(mm) - 1);
        Integer annee =new Integer(aa);
        if (annee < 2000 ) annee=annee+2000;
        dateCourante.set(Calendar.YEAR, annee);
        return (new Date(dateCourante.getTimeInMillis()));
    }

    public static String formatDateDaviCal(Date d){
        //20150110T075554Z
        SimpleDateFormat dt = new SimpleDateFormat("yyyyMMdd'T'HHmmss'Z'");
        return (dt.format(d));
    }
    public static String formatDateDaviCal(Date debut, Date heureDeb) {
        SimpleDateFormat dt = new SimpleDateFormat("yyyyMMdd");
        SimpleDateFormat dt2 = new SimpleDateFormat("HHmmss");

        return (dt.format(debut)+'T'+dt2.format(heureDeb)+'Z');
    }




    public static String  formatDate(Calendar cal){
      //  return ( (cal.get(Calendar.DAY_OF_MONTH)) + "/" +
        //        (cal.get(Calendar.MONTH)+1) + "/" +
         //       (cal.get(Calendar.YEAR)) );
    return ((cal.get(Calendar.YEAR))+"-"+(cal.get(Calendar.MONTH)+1)+"-"+(cal.get(Calendar.DAY_OF_MONTH)));
    }

    public static String formatDateToDavicalDataBase(Date dateCreate) {
        SimpleDateFormat dt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return (dt.format(dateCreate)+".000000+01");
    }

    public static boolean isFeriee(Calendar p_date)
    {
        // On constitue la liste des jours fériés
        final List< Calendar > joursFeries = new ArrayList< Calendar >();
        // On recherche les jours fériés de l'année de la date en paramètre
        final Calendar jourFerie = (Calendar) p_date.clone();
        jourFerie.set(jourFerie.get(Calendar.YEAR), Calendar.JANUARY, 1);
        joursFeries.add((Calendar) jourFerie.clone());
        jourFerie.set(jourFerie.get(Calendar.YEAR), Calendar.MAY, 1);
        joursFeries.add((Calendar) jourFerie.clone());
        jourFerie.set(jourFerie.get(Calendar.YEAR), Calendar.MAY, 8);
        joursFeries.add((Calendar) jourFerie.clone());
        jourFerie.set(jourFerie.get(Calendar.YEAR), Calendar.JULY, 14);
        joursFeries.add((Calendar) jourFerie.clone());
        jourFerie.set(jourFerie.get(Calendar.YEAR), Calendar.AUGUST, 15);
        joursFeries.add((Calendar) jourFerie.clone());
        jourFerie.set(jourFerie.get(Calendar.YEAR), Calendar.NOVEMBER, 1);
        joursFeries.add((Calendar) jourFerie.clone());
        jourFerie.set(jourFerie.get(Calendar.YEAR), Calendar.NOVEMBER, 11);
        joursFeries.add((Calendar) jourFerie.clone());
        jourFerie.set(jourFerie.get(Calendar.YEAR), Calendar.DECEMBER, 25);
        joursFeries.add((Calendar) jourFerie.clone());

        // Calcul du jour de pâques (algorithme de Oudin (1940))
        // Calcul du nombre d'or - 1
        final int intGoldNumber = p_date.get(Calendar.YEAR) % 19;
        // Année divisé par cent
        final int intAnneeDiv100 = (int) (p_date.get(Calendar.YEAR)
                / 100);
        // intEpacte est = 23 - Epacte (modulo 30)
        final int intEpacte = (intAnneeDiv100 - intAnneeDiv100 / 4
                - (8 * intAnneeDiv100 + 13) / 25
                + (19 * intGoldNumber) + 15) % 30;
        // Le nombre de jours à partir du 21 mars
        // pour atteindre la pleine lune Pascale
        final int intDaysEquinoxeToMoonFull = intEpacte - (intEpacte / 28)
                * (1 - (intEpacte / 28) * (29 / (intEpacte + 1))
                * ((21 - intGoldNumber) / 11));
        // Jour de la semaine pour la pleine lune Pascale (0=dimanche)
        final int intWeekDayMoonFull = (p_date.get(Calendar.YEAR)
                + p_date.get(Calendar.YEAR) / 4
                + intDaysEquinoxeToMoonFull + 2 - intAnneeDiv100
                + intAnneeDiv100 / 4) % 7;
        // Nombre de jours du 21 mars jusqu'au dimanche de ou
        // avant la pleine lune Pascale (un nombre entre -6 et 28)
        final int intDaysEquinoxeBeforeFullMoon =
                intDaysEquinoxeToMoonFull - intWeekDayMoonFull;
        // mois de pâques
        final int intMonthPaques = 3
                + (intDaysEquinoxeBeforeFullMoon + 40) / 44;
        // jour de pâques
        final int intDayPaques = intDaysEquinoxeBeforeFullMoon + 28
                - 31 * (intMonthPaques / 4);
        // lundi de pâques
        jourFerie.set(
                p_date.get(Calendar.YEAR), intMonthPaques - 1, intDayPaques + 1);
        final Calendar lundiDePaque = (Calendar) jourFerie.clone();
        joursFeries.add(lundiDePaque);
        // Ascension
        final Calendar ascension = (Calendar) lundiDePaque.clone();
        ascension.add(Calendar.DATE, 38);
        joursFeries.add(ascension);
        //Pentecote
        final Calendar lundiPentecote = (Calendar) lundiDePaque.clone();
        lundiPentecote.add(Calendar.DATE, 49);
        joursFeries.add(lundiPentecote);
      /*  if (joursFeries.contains(p_date)
                || p_date.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY
                || p_date.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
            return false;
        }*/

        if (joursFeries.contains(p_date)) return true;
        return false;
    }
    // Convertir ouvrable ouvre

    public static int convertirOuvrableOuvre(int c){
        int res = ((int)(c/6)*5) + (int)(c%6);
        return res;

    }


    public static String getRandom(int numchars) {
        Random r = new Random();
        StringBuffer sb = new StringBuffer();
        while (sb.length() < numchars) {
            sb.append(Integer.toHexString(r.nextInt()));
        }

        return sb.toString().substring(0, numchars);
    }

    //READ write parameter for test
    //write to file
    public static void  saveParamChanges(Properties props) {
        try {
            /*Properties props = new Properties();
            props.setProperty("ServerAddress", serverAddr);
            props.setProperty("ServerPort", ""+serverPort);
            props.setProperty("ThreadCount", ""+threadCnt);*/
            File f = new File("/tmp/hubo.properties");
            OutputStream out = new FileOutputStream(f,true);
            props.store(out, "This is an optional header comment string");


        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    //read from file
    public static Properties  loadParams() {
        Properties props = new Properties();
        InputStream is = null;

        // First try loading from the current directory
        try {
            File f = new File("/tmp/hubo.properties");
            is = new FileInputStream( f );
        }
        catch ( Exception e ) { is = null; }

        try {
            if ( is == null ) {
                // Try loading from classpath
                MyUtilLib ctx = new MyUtilLib();
                is = ctx.getClass().getResourceAsStream("server.properties");
            }

            // Try loading properties from the file (if found)
            props.load(is);
        }
        catch ( Exception e ) {
                System.err.println(e.toString());
            }
        return props;


    }
    public static void removeParamsFile(){
        (new File ("/tmp/hubo.properties")).delete();


    }

    public static void sendMessage(String mail,String title , String mess, String modele){
        HashMap<String, String> listeFichier = new HashMap<String, String>();


        HashMap<String, String> listeValue = new HashMap<String, String>();
        listeValue.put("Requete", mess);
        MonMail.sendMailTemplateWithFile(mail, title,
                listeValue,
                modele,
                listeFichier);
    }


    /***
     * Manipulate Excel File
     */
    public static Date getDateFromExcel(String fileName,int feuille, int ligne, int colonne) throws IOException, InvalidFormatException {
        Date res = new Date();
        FileInputStream file = null;
     //   try {
            file = new FileInputStream(new File(fileName));

            org.apache.poi.ss.usermodel.Workbook workbook = WorkbookFactory.create(file);


            org.apache.poi.ss.usermodel.Sheet sheet = workbook.getSheetAt(feuille);
            Row row = sheet.getRow(ligne);
            Cell cell = row.getCell(colonne);
            res = cell.getDateCellValue();


       /* }
         catch (Exception e) {
            System.out.println(" ERROR --> \n\n\n");
             e.printStackTrace();
        }*/
        return res;


    }
    public static String getStringFromExcel(String fileName,int feuille, int ligne, int colonne) throws IOException, InvalidFormatException {
        String res = "";
        FileInputStream file = null;
        //   try {
        file = new FileInputStream(new File(fileName));

        org.apache.poi.ss.usermodel.Workbook workbook = WorkbookFactory.create(file);


        org.apache.poi.ss.usermodel.Sheet sheet = workbook.getSheetAt(feuille);
        Row row = sheet.getRow(ligne);
        Cell cell = row.getCell(colonne);
        res = cell.getStringCellValue();


       /* }
         catch (Exception e) {
            System.out.println(" ERROR --> \n\n\n");
             e.printStackTrace();
        }*/
        return res;


    }
    public static double checExcelId(String fileName) {

        double res = -1;
        FileInputStream file = null;
        try {
            file = new FileInputStream(new File(fileName));

            org.apache.poi.ss.usermodel.Workbook workbook = WorkbookFactory.create(file);


            //org.apache.poi.ss.usermodel.Sheet sheet = workbook.getSheet("absence");
            int max =  workbook.getNumberOfSheets();
            for (int i = 0; i <  max ; i++) {
                org.apache.poi.ss.usermodel.Sheet sheet = workbook.getSheetAt(i);

                Iterator<Row> rowIterator = sheet.iterator();
                Row row = null;


                while(rowIterator.hasNext()) {
                    row = rowIterator.next();
                    Iterator<org.apache.poi.ss.usermodel.Cell> cellIterator = row.cellIterator();

                    while (cellIterator.hasNext()) {
                        org.apache.poi.ss.usermodel.Cell cell = cellIterator.next();
                        if (cell.getCellType() == Cell.CELL_TYPE_STRING) {
                            if (cell.getStringCellValue().compareToIgnoreCase("#ID#") == 0) {
                                cell = cellIterator.next();
                                if (cell != null) {
                                    if (cell.getCellType() == Cell.CELL_TYPE_STRING) {
                                        // res = Long.parseLong(cell.getStringCellValue());
                                        res = Double.parseDouble(cell.getStringCellValue());
                                    }
                                    if (cell.getCellType() == Cell.CELL_TYPE_NUMERIC) {
                                        res = (double) cell.getNumericCellValue();
                                    }

                                    return res;
                                }


                            }
                        }
                    }
                }


            }


        } catch (Exception e) {
            System.out.println(" ERROR --> \n\n\n");
            e.printStackTrace();
        }



        return res;
    }

}
