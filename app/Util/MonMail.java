package Util;



import java.lang.Integer;import java.lang.String;
import org.apache.commons.mail.*;
import org.stringtemplate.v4.ST;
import org.stringtemplate.v4.STGroup;
import org.stringtemplate.v4.STGroupDir;
import scala.collection.mutable.HashTable;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Properties;
import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

/**
 * Created by richard on 13/06/14.
 */
public class MonMail {
 /*   String smtpHost = Play.application().configuration().getString("smtp.host");
    Integer smtpPort = Play.application().configuration().getInt( "smtp.port" );
    String smtpUser = Play.application().configuration().getString( "smtp.user" );
    String smtpPassword = Play.application().configuration().getString( "smtp.password"
*/

    
    
    /*String smtpHost = "";
    static Integer smtpPort=25;
    static String smtpUser="";
    static String smtpPassword="" ;
    static String AdresseMailFrom= "richard.urunuela@irccyn.ec-nantes.fr";
    static String HostName= "smtp.irccyn.ec-nantes.fr";
    static boolean SSL=false;
*/

   /* static Integer smtpPort=587;
    static String smtpUser="richard.urunuela@icloud.com";
    static String smtpPassword="secret1602" ;
    static String AdresseMailFrom= "richard.urunuela@icloud.com";
    static String HostName= "smtp.mail.me.com";
    static boolean SSL=true;
*/
    static Integer smtpPort=587;
    static String smtpUser="ierdremail@gmail.com";
    static String smtpPassword="12se34cre56t" ;
    static String AdresseMailFrom= "ierdremail@gmail.com";
    static String HostName= "smtp.gmail.com";
    static boolean SSL=false;
    
    public static void sendEmptyMail(){
        Email email = new SimpleEmail();
        try {
            email.setFrom (AdresseMailFrom);

            email.addTo("ierdremail@gmail.com");

            email.setHostName(HostName);
            email.setSmtpPort(smtpPort);
            email.setSSLOnConnect( SSL);
           


           email.setSubject("subject TEST");
           email.setMsg("Message");
           email.send();
        } catch (EmailException e) {
            e.printStackTrace();
        }


    }



    public static void sendMailFromTemplate (HashMap<String,Object> listeClef,String template,String title,String adresseMail){
        String res ="";
        STGroup group =null;
        try {
            group= new STGroupDir("template");
        }
        catch (IllegalArgumentException E){

            group = new STGroupDir("../../../template");
        }
        ST st = group.getInstanceOf(template);
        st.add("numero", listeClef.get("numero"));
        //st.add("name", "x");
        //st.add("value", 0);
        String result = st.render(); // yields "int x = 0;"

       //  System.out.println(result);
        sendMailText(adresseMail, title,result);

    }


    private static String getBody (HashMap<String,String> listeClef,String template){
        String res ="";
        STGroup group =null;
        try {
            group= new STGroupDir("template");
        }
        catch (IllegalArgumentException E){

            group = new STGroupDir("../../../template");
        }


        ST st = group.getInstanceOf(template);
        for (String key : listeClef.keySet()) {
            st.add(key, listeClef.get(key));

        }


        String result = st.render();
        return result;

    }
    public static void sendMailText(String adressseMail,String sujet,  String msg) {
        Email email = new SimpleEmail();
        try {
            email.setFrom (AdresseMailFrom);

            email.addTo(adressseMail);

            email.setHostName(HostName);
            email.setSmtpPort(smtpPort);
            email.setSSLOnConnect( SSL);
            
            email.setHostName("smtp.gmail.com");
            
            
            email.setAuthenticator(new DefaultAuthenticator("ierdremail@gmail.com","12se34cre56t"));

            email.getMailSession().getProperties().put("mail.smtps.auth", "true");
            email.getMailSession().getProperties().put("mail.debug", "true");
            email.getMailSession().getProperties().put("mail.smtps.port", "587");
            email.getMailSession().getProperties().put("mail.smtps.socketFactory.port", "587");
            email.getMailSession().getProperties().put("mail.smtps.socketFactory.class",   "javax.net.ssl.SSLSocketFactory");
            email.getMailSession().getProperties().put("mail.smtps.socketFactory.fallback", "false");
            email.getMailSession().getProperties().put("mail.smtp.starttls.enable", "true");

            email.setSubject(sujet);
            email.setMsg(msg);
            email.send();
        } catch (Exception e) {
            System.out.println(" [ENVOIE MAIL IMPOSSIBLE]");
            e.printStackTrace();
        }

    }
    public static void sendMailTemplateWithFile(String adressseMail,
                                                String sujet,
                                                HashMap<String,String> listeClef,
                                                String template,
                                                HashMap<String,String> files ) {


        EmailAttachment attachement = new EmailAttachment();
        MultiPartEmail email = new MultiPartEmail();
        try {



            email.setFrom (AdresseMailFrom);

            email.addTo(adressseMail);

            email.setHostName(HostName);
            email.setSmtpPort(smtpPort);
            email.setSSLOnConnect( SSL);

            email.setHostName("smtp.gmail.com");


            email.setAuthenticator(new DefaultAuthenticator("ierdremail@gmail.com","12se34cre56t"));

            email.getMailSession().getProperties().put("mail.smtps.auth", "true");
            email.getMailSession().getProperties().put("mail.debug", "true");
            email.getMailSession().getProperties().put("mail.smtps.port", "587");
            email.getMailSession().getProperties().put("mail.smtps.socketFactory.port", "587");
            email.getMailSession().getProperties().put("mail.smtps.socketFactory.class",   "javax.net.ssl.SSLSocketFactory");
            email.getMailSession().getProperties().put("mail.smtps.socketFactory.fallback", "false");
            email.getMailSession().getProperties().put("mail.smtp.starttls.enable", "true");

            email.setSubject(sujet);
            email.setMsg(MonMail.getBody( listeClef,template));

            // Fichier
            for (String fileName : files.keySet()) {
                String description = files.get(fileName);
                attachement.setPath(fileName);
                attachement.setDisposition(EmailAttachment.ATTACHMENT);
                attachement.setDescription(description);
                attachement.setName(description+".xls");

            }

            //fichier
            if (files.size() > 0) {

                email.attach(attachement);
            }
            email.send();
        } catch (EmailException e) {
            e.printStackTrace();
        }






    }
    @Deprecated
    public static void sendMailTextWithFile(String adressseMail,String sujet,  String msg,String file) {

        EmailAttachment attachement = new EmailAttachment();
        MultiPartEmail email = new MultiPartEmail();
        try {

            // Fichier
            attachement.setPath(file);
            attachement.setDisposition(EmailAttachment.ATTACHMENT);
            attachement.setDescription("planning");
            attachement.setName("planning.pdf");
            //fichier

            email.setFrom (AdresseMailFrom);

            email.addTo(adressseMail);

            email.setHostName(HostName);
            email.setSmtpPort(smtpPort);
            email.setSSLOnConnect( SSL);

            email.setHostName("smtp.gmail.com");


            email.setAuthenticator(new DefaultAuthenticator("ierdremail@gmail.com","12se34cre56t"));

            email.getMailSession().getProperties().put("mail.smtps.auth", "true");
            email.getMailSession().getProperties().put("mail.debug", "true");
            email.getMailSession().getProperties().put("mail.smtps.port", "587");
            email.getMailSession().getProperties().put("mail.smtps.socketFactory.port", "587");
            email.getMailSession().getProperties().put("mail.smtps.socketFactory.class",   "javax.net.ssl.SSLSocketFactory");
            email.getMailSession().getProperties().put("mail.smtps.socketFactory.fallback", "false");
            email.getMailSession().getProperties().put("mail.smtp.starttls.enable", "true");

            email.setSubject(sujet);
            email.setMsg(msg);

            email.attach(attachement);
            email.send();
        } catch (EmailException e) {
            e.printStackTrace();
        }
    }
}
