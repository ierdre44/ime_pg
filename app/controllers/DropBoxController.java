package controllers;

import Util.MonMail;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import models.CompteDropbox;
import models.Dropbox.DropInterface;
import models.Employe;
import models.Entreprise;
import models.Requete;
import models.osTicket.Ticket;
import play.Play;
import play.libs.Akka;
import play.libs.F;
import play.libs.Json;
import play.mvc.Result;
import scala.concurrent.ExecutionContext;

import java.util.ArrayList;
import java.util.List;


import static play.mvc.Results.ok;

/**
 * Created by richard on 09/06/15.
 */
public class DropBoxController {

    public static Result ListTicketDropbox() {
        ObjectNode resultat;
        resultat = Json.newObject();
        ArrayNode listeEntreprise=resultat.arrayNode();

        //parcours de la liste des entreprises
        //parcours de la liste des administrateurs de l'entreprise
        // parcours du répertoire maj
        List<Entreprise> entreprises = Entreprise.find.all();
        for (Entreprise ent:entreprises){
            ObjectNode entNode = Json.newObject();
            entNode.put("id",ent.getId());
            entNode.put("nom",ent.getNom());
            listeEntreprise.add(entNode);

        }


        resultat.put("Entreprise",listeEntreprise);



        return ok(resultat);
    }

    public static Result doCreationDropboxEntreprise(long id){
    //public static F.Promise<Result> doCreationDropboxEntreprise(long id){
        /*ExecutionContext myExecutionContext = Akka.system().dispatchers().lookup("play.akka.actor.my-context");
        F.Promise<Integer> integerPromise = F.Promise.promise(() ->
                LongRunningProcess.run(10000L)
                , myExecutionContext);

        F.Promise<Integer> integerPromise2 = F.Promise.promise(() ->
                LongRunningProcess.run(10000L)
                , myExecutionContext);*/


        ObjectNode resultat;
        resultat = Json.newObject();
        Entreprise ent= Entreprise.find.byId(id);
        resultat.put("id",ent.getId());
        resultat.put("nom",ent.getNom());
        ArrayNode listeEmp=resultat.arrayNode();

        for (Employe emp:ent.employes){
            if (emp.getIsManager()){
                ObjectNode empNode = Json.newObject();
                empNode.put("nom",emp.getNom());
                empNode.put("prenom",emp.getPrenom());
                empNode.put("nomPrenom",emp.getNomPrenom());
                empNode.put("function",emp.getFonction());
                empNode.put("mail",emp.getAdresseMail());

                // get liste fichier dans repertoire bannette de creation emp
                CompteDropbox compte = ent.getCompteDropbox();
                DropInterface drop = new DropInterface();
                drop.initDrop(compte);
                String HomePathEmploye= Play.application().configuration().getString("hubo.dropbox.HomePathTicket");

                String PathAdmin = HomePathEmploye.replace("<entreprise_nom>",ent.getNom()).
                        replace("<employe_mail>", emp.getAdresseMail());
                String adresseAdmin=Play.application().configuration().getString("hubo.mailAdmin");
                String PathCreate =PathAdmin+"/HUBO-transfert";
                ArrayList<String> listfichier = drop.getlisteFichier(PathCreate);
                ArrayNode listeFile=empNode .arrayNode();
                for (String name:listfichier){

                    // récupére fichier pour création ticket
                    String fileName= drop.getTempFichier(PathCreate+"/"+name);

                    Ticket t = new Ticket();
                    t.sujet="HUBO_TRANSFERT";
                    t.idUser= emp.getId();
                    t.nombreFichier=1;
                    String []files = {fileName};
                    t.Fichiers= new long[1];
                    t.Fichiers[0]=-1;
                    t.setFile(files);
                    t.virtual= true;
                    t.isDropBox=true;
                    t.email= emp.getAdresseMail();
                    t.filePathNameOnDropBox=(PathCreate+"/"+name);
                    t.PathNameOnDropBox=(PathCreate);
                    t.fileNameOnDropBox=name;
                    Requete req = new Requete(t);
                    JsonNode res = req.execute(t);


                    MonMail.sendMailText(adresseAdmin,"HUBO TRANSFERT" ,res.toString());



                }
                empNode.put("creations",listeFile);










                listeEmp.add(empNode);

            }

        }
        resultat.put("managers",listeEmp);




        return ok(resultat);
       // return integerPromise.flatMap(i -> integerPromise2.map(x -> ok()));

    }


    /**
     * Ticket pour une entreprise sur ID
     */
    public static Result ListTicketDropboxEntreprise(long id){
        ObjectNode resultat;
        resultat = Json.newObject();
        Entreprise ent= Entreprise.find.byId(id);
        resultat.put("id",ent.getId());
        resultat.put("nom",ent.getNom());
        // get manager
        ArrayNode listeEmp=resultat.arrayNode();
        for (Employe emp:ent.employes){
            if (emp.getIsManager()){
                ObjectNode empNode = Json.newObject();
                empNode.put("nom",emp.getNom());
                empNode.put("prenom",emp.getPrenom());
                empNode.put("nomPrenom",emp.getNomPrenom());
                empNode.put("function",emp.getFonction());
                empNode.put("mail",emp.getAdresseMail());

                // get liste fichier dans repertoire bannette de creation emp
                CompteDropbox compte = ent.getCompteDropbox();
                DropInterface drop = new DropInterface();
                drop.initDrop(compte);
                String HomePathEmploye= Play.application().configuration().getString("hubo.dropbox.HomePathTicket");

                String PathAdmin = HomePathEmploye.replace("<entreprise_nom>",ent.getNom()).
                        replace("<employe_mail>", emp.getAdresseMail());
                String PathCreate =PathAdmin+"/HUBO-transfert";
                ArrayList<String> listfichier = drop.getlisteFichier(PathCreate);
                ArrayNode listeFile=empNode .arrayNode();
                for (String name:listfichier){
                    ObjectNode fileNode = Json.newObject();
                    fileNode.put("repertoire",PathCreate);
                    fileNode.put("fichier",name);
                    listeFile.add(fileNode);

                }
                empNode.put("creations",listeFile);










                listeEmp.add(empNode);

            }

        }
        resultat.put("managers",listeEmp);




        return ok(resultat);

    }
}
