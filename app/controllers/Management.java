package controllers;

import models.osTicket.Ticket;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Results;

/**
 * Created by richard on 01/05/15.
 */
public class Management extends Controller {

    public static Result majTicket() {

        return ok(Ticket.majAllTicket());
    }
    public static Result listeTickets(){

        return ok(Ticket.AllTicket());
    }

}
