package controllers;

import play.mvc.Http.Context;
import play.mvc.Result;
import play.mvc.Security;

/*
Info sur authentification
https://www.playframework.com/documentation/2.1.0/JavaGuide4
 */

public class SecuredAdmin extends Security.Authenticator {

    @Override
    public String getUsername(Context ctx) {

        return ctx.session().get("email");
    }

    @Override
    public Result onUnauthorized(Context ctx) {

        return redirect(routes.Application.login());
    }
}