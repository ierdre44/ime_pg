/**
 * Created by richard on 28/02/15.
 */
import play.api._
import play.Logger
import play.mvc.Http.RequestHeader
import scala.concurrent.Future
import play.api.mvc.Results._
object Global extends GlobalSettings  {


  def onError(request: RequestHeader, ex: Throwable) = {
    Future.successful(InternalServerError(
      views.html.errors.onBadPage()
    ))
  }

  def onBadRequest(request: RequestHeader, error: String) = {
    Future.successful(BadRequest("Bad Request: " + error))
  }
  override  def onHandlerNotFound(request: play.api.mvc.RequestHeader) = {
    Future.successful(InternalServerError(
      views.html.errors.onBadPage()
    ))


  }
}

