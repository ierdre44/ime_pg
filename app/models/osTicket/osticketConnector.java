package models.osTicket;


import models.AbsTicket;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.sql.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

/**
 * Created by richard on 30/04/15.
 */
public class osticketConnector {


    public static ArrayList<AbsTicket> getTicketOpen() {

        ArrayList<AbsTicket> resultat = new ArrayList<AbsTicket>();
        // String sqlgetId = " select ticket_id,user_id,user_email_id,email_id,number,status_id " +
        //     "from ost_ticket where ticket_id=?;";
        String sqlgetId = " select ticket_id,user_id,user_email_id,email_id,number,status_id " +
                "from ost_ticket where status_id=1;";
        String sqlgetThread = "select * from ost_ticket_thread where ticket_id=? and user_id=? order by created ASC";
        String sqlgetNbrThread = "select count(*) from ost_ticket_thread where ticket_id=? and user_id=? order by created ASC";

        String sqlgetAllThread = "select * from ost_ticket_thread where ticket_id=?";
        String sqlgetFileAttachement = "select * from ost_ticket_attachment where ticket_id=? and ref_id=?";
        String sqlgetFileInfo = "select * from ost_file where id=?;";
        String sqlgetBlob = "select * from ost_file_chunk where file_id=?;";


        String addInfoThread = "insert into ost_ticket_thread " +
                "(ticket_id,staff_id,user_id,thread_type,poster,title,body,created,updated) " +
                "values (?,0,0,'N','HUBO',?,?,?,'0000-00-00 00:00:00');";

        //String sqlgetUser= "select default_email_id from ost_user where id=?";
        String sqlgetMail = "select * from ost_user_email where user_id=?;";
        //Statement stmt = null;
        //  Statement stmt2 = null;
        Connection conn = play.db.DB.getConnection("osticket");
        // get ticket Open
        try {
            conn.setAutoCommit(false);
            //stmt = conn.createStatement();
            PreparedStatement getTickets = conn.prepareStatement(sqlgetId);

            ResultSet res = getTickets.executeQuery();
            while (res.next()){
                Ticket t = new Ticket();

                t.idBase = res.getLong(1);
                t.idUser = res.getLong(2);
                t.number = res.getLong("number");


                resultat.add(t);


            }

            //get mail from user on ticket
            PreparedStatement getMail = conn.prepareStatement(sqlgetMail);
            Iterator I= resultat.iterator();
            while (I.hasNext()){
                Ticket ticket = (Ticket) I.next();
                getMail.setLong(1,ticket.idUser);
                ResultSet res2= getMail.executeQuery();
                if (res2.next()) {
                    ticket.email = res2.getString(3);
                }

            }
            //GEt liste of thread pour un ticket par utilisateur
            /***
             *
             * Note :
            // On peut avoir plusieurs thread par ticket
            // Mais en general comme on ne support pas le reply
            // les autre thread concernt le system ou d'autre user.
            // A explorer
             */
            PreparedStatement getCorps = conn.prepareStatement(sqlgetNbrThread);
            I= resultat.iterator();
            while (I.hasNext()){
                Ticket ticket = (Ticket) I.next();
                getCorps.setLong(1,ticket.idBase);
                getCorps.setLong(2,ticket.idUser);
                ResultSet res2= getCorps.executeQuery();

                if (res2.next()) {
                    ticket.nbrThread = res2.getInt(1);



                }

            }

            //On recupére pour le  thread demandeur le sujet corps
            getCorps = conn.prepareStatement(sqlgetThread);
            I= resultat.iterator();
            while (I.hasNext()){
                Ticket ticket = (Ticket) I.next();
                getCorps.setLong(1,ticket.idBase);
                getCorps.setLong(2,ticket.idUser);
                ResultSet res2= getCorps.executeQuery();

                if (res2.next()) {

                    ticket.sujet= res2.getString("title");
                    ticket.threadnum = res2.getLong("id");
                    ticket.format=res2.getString("format");

                    if (ticket.format.compareToIgnoreCase("html")==0) {
                        ticket.corps= res2.getString("body").toString().replaceAll("\\<.*?>","");
                    }
                    else {
                        ticket.corps= res2.getString("body").toString();


                    }


                }

            }
            // On récupére le nom et la liste des fichiers avec ID pour usage futur
            PreparedStatement getRefFile = conn.prepareStatement(sqlgetFileAttachement);
            I= resultat.iterator();
            while (I.hasNext()){
                Ticket ticket = (Ticket) I.next();
                getRefFile.setLong(1,ticket.idBase);
                getRefFile.setLong(2,ticket.threadnum);
                ResultSet  res2 = getRefFile.executeQuery();
                while (res2.next()){

                    long idFile = res2.getLong("file_id");

                    ticket.Fichiers[ticket.nombreFichier]=idFile;
                    ticket.nombreFichier++;

                    // TODO CODE A AFFINER
                    if  (ticket.nombreFichier>10) {
                        System.out.println(" ERRREUR ");
                        ticket.nombreFichier=10;
                    };


                }

            }





        } catch (SQLException e) {
            e.printStackTrace();
        }
        return resultat;


    }

    public static String[] generateFichier(double[] resultat,long ticketid, long threadid) {
        String[] resString= new String[(resultat.length/2)];

        String sqlgetFileInfo="select * from ost_file where id=?;";
        String sqlgetBlob="select * from ost_file_chunk where file_id=?;";
        //System.out.println(" -->" +ticketid+ " " + threadid);

        try {
            int cnt=0;
            int cpt_double=0;
            String sqlgetFileAttachement="select * from ost_ticket_attachment where ticket_id=? and ref_id=?";

            Connection conn =play.db.DB.getConnection("osticket");
            conn.setAutoCommit(false);
            PreparedStatement getRefFile = conn.prepareStatement(sqlgetFileAttachement);
            getRefFile.setLong(1,ticketid);
            getRefFile.setLong(2, threadid);
            ResultSet res2 = getRefFile.executeQuery();
            while(res2.next()){
                long idFile = res2.getLong("file_id");
                resultat [cpt_double] = idFile;
               // System.out.println(cpt_double);
                cpt_double++;
                resultat[cpt_double]=-1;


                PreparedStatement getInfoFile = conn.prepareStatement(sqlgetFileInfo);
                getInfoFile.setLong(1,idFile);
                ResultSet res = getInfoFile.executeQuery();
                if (res.next()) {
                    String fileName = res.getString("name");

                    PreparedStatement getBlobFile = conn.prepareStatement(sqlgetBlob);
                    getBlobFile.setLong(1, idFile);
                    ResultSet res3 = getBlobFile.executeQuery();
                    if (res3.next()) {
                        Blob blob = res3.getBlob("filedata");
                        long blobLength = blob.length();
                        int pos = 1;   // position is 1-based
                        int len = 10;
                        byte[] bytes = blob.getBytes(pos, len);
                        InputStream is = blob.getBinaryStream();
                        String pathTofile= "/tmp/"+ticketid+"/"+idFile+"/";
                        new File(pathTofile).mkdirs();
                        FileOutputStream os = new FileOutputStream(pathTofile+fileName);
                        int b = -1;
                        while (( b = is.read()) != -1)
                        {
                            os.write(b); // fos is the instance of FileOutputStream
                        }
                        os.close();
                        resString[cnt]=pathTofile+fileName;

                        //os.write(b);
                    }


                }
                //BUG ???
                // PB placement ????
                cnt = cnt+1;
                cpt_double++;
                
            }



        } catch (Exception e) {
            e.printStackTrace();
        }

        return resString;
    }
}
