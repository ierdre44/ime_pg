package models.osTicket;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import models.AbsTicket;
import models.Employe;

import models.Requete;
import play.libs.Json;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.Iterator;

/**
 * Created by richard on 30/04/15.
 */
/*class Tfile{
    long id;
    String name;

}*/
public class Ticket extends AbsTicket {

    public String filePathNameOnDropBox="";





    public Ticket(){
       // Fichiers = new ArrayList<String>();
        Fichiers = new long[10];

    }

    public static ObjectNode majAllTicket() {
        ObjectNode resultat;
        resultat = Json.newObject();
        resultat.put("type",Ticket.class.getName());

        ArrayNode listeTicket=resultat.arrayNode();
        ArrayList<AbsTicket> tickets= osticketConnector.getTicketOpen();
        Iterator I= tickets.iterator();


        while (I.hasNext()){
            Ticket ticket = (Ticket) I.next();
            listeTicket.add(ticket.getJson());

            Employe emp= Employe.findByMail(ticket.email);
            if (emp==null) {
                ticket.addSysInfo("Email inconnu","[HUBO]");
                ticket.close();


            }
            else {
                Requete req = new Requete(ticket);
                //TO DO
                /*
                VErifier si un ticket exsite déja ?

                 */
               // if (req.execute(ticket)){
                JsonNode res = req.execute(ticket);
                listeTicket.add(res);
                    //TO DO
                    //req.save();
                //}


                // TO DO
                // ticket.close();
            }

           //
        }

        resultat.put("Tickets",listeTicket);




        return resultat;


    }

    private void close() {
        if (virtual) return;
        String closeTicket= "update ost_ticket  set status_id=3  where ticket_id=?;";
        Connection conn =play.db.DB.getConnection("osticket");
        try {
            conn.setAutoCommit(false);
            PreparedStatement insertInfo = conn.prepareStatement(closeTicket);
            insertInfo.setLong(1,this.idBase);
            insertInfo.execute();
            conn.commit();
            //stmt.close();
            conn.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void addSysInfo(String message,String title){
        if (virtual) return;
        String addInfoThread= "insert into ost_ticket_thread " +
                "(ticket_id,staff_id,user_id,thread_type,poster,title,body,created,updated) " +
                "values (?,0,0,'N','HUBO',?,?,?,'0000-00-00 00:00:00');";
        Connection conn =play.db.DB.getConnection("osticket");





        try {
            conn.setAutoCommit(false);
            PreparedStatement insertInfo = conn.prepareStatement(addInfoThread);
            insertInfo.setLong(1,this.idBase);
            insertInfo.setString(2,title);
            insertInfo.setString(3, message);


            insertInfo.setTimestamp(4, new java.sql.Timestamp(System.currentTimeMillis()));
            insertInfo.execute();
            conn.commit();
            //stmt.close();
            conn.close();

        } catch (Exception e) {
            e.printStackTrace();
        }



    }

    public static  ObjectNode AllTicket() {
        ObjectNode resultat;
        resultat = Json.newObject();
        resultat.put("type",Ticket.class.getName());

        ArrayNode listeTicket=resultat.arrayNode();
        ArrayList<AbsTicket> tickets= osticketConnector.getTicketOpen();
        Iterator I= tickets.iterator();


        while (I.hasNext()){
            Ticket ticket = (Ticket) I.next();
            listeTicket.add(ticket.getJson());
        }
        resultat.put("Tickets",listeTicket);


        return resultat;
    }
    public  String[] generateFile(){
        return null;
    };

    public void setFile(String[] files) {
        this.files = files;
    }
}
