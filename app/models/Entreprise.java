package models;

import play.db.ebean.Model;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;

/**
 * Created by richard on 29/05/15.
 */
@Entity
@Table(name="Entreprise", catalog="", schema="")
public class Entreprise extends Model {
    @Version
    public Timestamp lastUpdate;
    public Timestamp getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(Timestamp lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    /**
     * Id
     */
    @Id
    public Long id;
    public long getId(){

        return this.id;
    }
    /**
     *Nom
     *
     */
    public String nom="";
    public String getNom() {
        return nom;
    }
    public void setNom(String nom) {
        this.nom = nom;
    }
    @OneToMany(mappedBy="entreprise", cascade=CascadeType.ALL)
    public List<Employe> employes;

    @ManyToOne
    @JoinColumn(name="compteDropboxId", nullable=false)
    public CompteDropbox compteDropbox;

    public static Finder<Long,Entreprise> find = new Finder<Long,Entreprise>(
            Long.class, Entreprise.class
    );


    public void setCompteDropbox(CompteDropbox compteDropbox) {
        this.compteDropbox = compteDropbox;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public CompteDropbox getCompteDropbox() {
        return compteDropbox;
    }
}
