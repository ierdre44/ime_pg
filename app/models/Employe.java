package models;

import javax.persistence.*;
import javax.validation.Constraint;

import com.avaje.ebean.Ebean;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import play.data.validation.Constraints;
import play.db.ebean.Model;
import play.libs.Json;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by richard on 08/04/15.
 */
@Entity
public class Employe extends Model{
    /* *

    Time stamp
     */
    @Version
    public Timestamp lastUpdate;
    public Timestamp getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(Timestamp lastUpdate) {
        this.lastUpdate = lastUpdate;
    }
/**
 * Entreprise
 */


@ManyToOne
@JoinColumn(name="entrepriseId", nullable=false)
public Entreprise entreprise;
    /**
     * Id
     */
    @Id
    public Long id;
    public long getId(){

        return this.id;
    }
    /**
     * level
     *
     */
    @Constraints.Max(10)
    public int level=1;
    public int getLevel(){
        return this.level;
    }
    public void setLevel(int l){
        this.level = l;
    }
    /**
     * isPortalUser
     * definis si l'utilisateur peut utiliser le portail
     */
    public boolean isPortalUser = false;
    public void setIssPortalUser (boolean v){
        this.isPortalUser =v;
    }
    public boolean getIsPortalUser(){
        return this.isPortalUser;
    }

    public boolean isPortalUser() {
        return isPortalUser;
    }

    public void setPortalUser(boolean isPortalUser) {
        this.isPortalUser = isPortalUser;
    }

    public boolean isManager() {
        return isManager;
    }

    public void setManager(boolean isManager) {
        this.isManager = isManager;
    }

    /**
     * isManager
     * definis si l'utilisateur manage d'autre Employe
     */
    public boolean isManager = false;
    public void setIsManager (boolean v){
        this.isManager =v;
    }
    public boolean getIsManager(){
        return this.isManager;
    }

    /**
     * nom
     *
     */
    public String nom="";
    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }
    // TO DO a exploiter
    /**
     *Fonction
     *
     */
    public String fonction="";
    public String getFonction() {
        return fonction;
    }

    public void setFonction(String fonction) {
        this.fonction = fonction;
    }
    /**
     *Prenom
     *
     */
    public String Prenom="";
    public String getPrenom() {
        return Prenom;
    }

    public void setPrenom(String Prenom) {
        this.Prenom = Prenom;
    }

    /**
     * nom Prenom
     *
     */
    // Des fois on a juste NOMPrenom et pas separer ....
    public String nomPrenom="";
    public String getNomPrenom() {
        return nomPrenom;
    }

    public void setNomPrenom(String nomPrenom) {
        this.nomPrenom = nomPrenom;
    }

    /**
     * Mot de passe
     *
     */
    public String password="";
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    /**
     * Adresse mail
     * Utilisé aussi comme identifiant;
     *
     */
    @Column(unique=true)
    public String adresseMail="";
    public String getAdresseMail() {
        return adresseMail;
    }

    public void setAdresseMail(String email) {
        this.adresseMail = email;
    }


    /* Indentifiant de gestionnaite */

    String  traitementRequete="Requete";
    public static Finder<Long,Employe> find = new Finder<Long,Employe>(
            Long.class, Employe.class
    );



    public  static Employe findByMail(String adresseMail) {
        Employe emp= (Employe) Ebean.find(Employe.class).where().eq("adresseMail",adresseMail).findUnique();
        return emp;

    }
    public JsonNode getJson() {
        ObjectNode info = Json.newObject();
        info.put("mail",this.adresseMail);
        info.put("nomPrenom",this.nomPrenom);
        info.put("nom",this.nom);

        info.put("prenom",this.getPrenom());
        info.put("fonction",this.getFonction());


        return info;
    }

    public void setTraitementRequete(String traitementRequete) {
        this.traitementRequete = traitementRequete;
    }

    public String getTraitementRequete() {
        return traitementRequete;
    }

    public void setEntreprise(Entreprise entreprise) {
        this.entreprise = entreprise;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Entreprise getEntreprise() {
        return entreprise;
    }
}
