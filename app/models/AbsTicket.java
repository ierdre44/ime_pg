package models;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import play.libs.Json;

/**
 * Created by richard on 04/06/15.
 */
public abstract class AbsTicket {
    public long idBase;
    public long idUser;
    public long number;
    public String email;

    // public ArrayList<String> Fichiers;
    public long[]Fichiers;
    public String sujet;
    public String corps="";
    public int nbrThread=0;
    public long threadnum;
    public int nombreFichier=0;
    public String format="";
    public String[] files;
    public double[] listesheet=null;
    public boolean virtual=false;
    public boolean isDropBox=false;
    public String filePathNameOnDropBox;
    public String PathNameOnDropBox;
    public String fileNameOnDropBox;


    @Override
    public String toString(){
        String res="";

        res = res + " " +this.sujet + " "  +idBase+" "+idUser + " " + number + " "+email + " " + nbrThread + " "
                + sujet  +" " +  " " +threadnum + " Fichiers :";
        for (int i =0; i < nombreFichier ; i ++ ) {
            res = res + " " + Fichiers[i];
        }
        return res;
    }
    public JsonNode getJson() {
        ObjectNode info = Json.newObject();
        info.put("sujet",sujet);
        info.put("format",format);
        info.put("corps",this.corps);

        info.put("idBase",idBase);
        info.put("idUser",idUser);

        info.put("number)",number);
        info.put("email", email);
        info.put("nbrThread", nbrThread);
        info.put("nombreFichier", nombreFichier);
        return info;
    }

    public void setFile(String[] files) {
        this.files = files;
    }


    public abstract String[] generateFile();

}
