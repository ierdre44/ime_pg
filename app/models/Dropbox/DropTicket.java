package models.Dropbox;

import models.AbsTicket;

/**
 * Created by richard on 04/06/15.
 */
public class DropTicket extends AbsTicket {

    public DropInterface dropInterface;
    public boolean isDropBox=false;
    public String filePathNameOnDropBox="";

    public  String[] generateFile(){

        String res[]=new String[this.nombreFichier];
        int i =0;
        for (String filename:this.files){
            res[i]=this.dropInterface.getTempFichier(filename);
            i++;
        }
        this.files=res;
        return res;
    }

}
