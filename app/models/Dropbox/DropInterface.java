package models.Dropbox;

import com.dropbox.core.*;
import com.dropbox.core.http.HttpRequestor;
import com.dropbox.core.http.StandardHttpRequestor;
import com.dropbox.core.json.JsonReader;
import com.dropbox.core.util.IOUtil;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import models.CompteDropbox;
import play.Play;
import play.libs.Json;

import java.io.*;
import java.net.InetSocketAddress;
import java.net.MalformedURLException;
import java.net.Proxy;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by richard on 31/05/15.
 */
public class DropInterface {
    private DbxClient dropClient;
    public DbxClient getDropClient() {
        return dropClient;
    }

    public void setDropClient(DbxClient dropClient) {
        this.dropClient = dropClient;
    }

    /**
     * Initialisation compte avec objet compte (lié à l'entreprise)
     * @param compte
     */
    public ObjectNode initDrop(CompteDropbox compte) {
        ObjectNode resultat=null;
        resultat = Json.newObject();
        DbxAuthInfo authInfo=null;
        try {
            //System.out.println(compte.getAccessToken());
            authInfo = DbxAuthInfo.Reader.readFromFile(compte.getAccessToken());
            String userLocale = Locale.getDefault().toString();


            //config proxy irccyn
            //test if behind proxy
            //URL url = new URL("http://www.hubo-soft.com/");
            //System.out.println(url.getContent());
           boolean useProxy=  Play.application().configuration().getBoolean("hubo.proxy");
            DbxRequestConfig requestConfig=null;
            if (useProxy) {
                String Pname = Play.application().configuration().getString("hubo.proxyHost");
                int port = Play.application().configuration().getInt("hubo.proxyPort");
                Proxy proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress("cache.irccyn.ec-nantes.fr", 3128));


                HttpRequestor req = new StandardHttpRequestor(proxy);

                requestConfig = new DbxRequestConfig("examples-account-info", userLocale, req);
            }
            else{
                requestConfig = new DbxRequestConfig("examples-account-info", userLocale);

            }

            DbxClient dbxClient = new DbxClient(requestConfig, authInfo.accessToken, authInfo.host);
            DbxAccountInfo dbxAccountInfo=null;
            dbxAccountInfo = dbxClient.getAccountInfo();
            this.setDropClient(dbxClient);

            //System.out.println(dbxAccountInfo.toStringMultiline());

                    ObjectMapper mapper = new ObjectMapper();
            JsonNode res= mapper.valueToTree(dbxAccountInfo.toString());
            resultat.put("dropbox",res);

        } catch (JsonReader.FileLoadException e) {
            e.printStackTrace();
        } catch (DbxException e) {
            e.printStackTrace();

        }


        return resultat;


    }
    /**
     * Move file pour archive par exemple
     */
    public void moveFichier(String source,String destination) {
        try {
            this.getDropClient().move(source,destination);
        } catch (DbxException e) {
            e.printStackTrace();
        }
    }
        /**
         * Delete file
         */
    public void deleteFichier(String Path){
        try {
            this.getDropClient().delete(Path);
        } catch (DbxException e) {
            e.printStackTrace();
        }
    }
    /**
     * uplaod plusieurs  fichier vers le compte dropbox
     * @param source
     * @param destination
     */
    public void putFichier(String[] source,String[] names,String destination){
        InputStream in=null;
        for (int i=0; i< source.length;i++){
            if (source[i] == null) continue;

            try {
                in = new FileInputStream(source[i]);
                DbxEntry.File metadata =this.getDropClient().uploadFile(destination+"/"+names[i], DbxWriteMode.force(), -1, in);


            } catch (DbxException e) {
                e.printStackTrace();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            finally {
                IOUtil.closeInput(in);
            }

        }


    }


        /**
         * uplaod un fichier vers le compte dropbox
         * @param source
         * @param destination
         */
    public void putFichier(String source,String destination){
        InputStream in=null;
        while (this.fileExist(destination)) {
            this.deleteFichier(destination);
        }
        while (!this.fileExist(destination)) {
            try {
                //
                File inputFile = new File(source);
                in = new FileInputStream(inputFile);

                // in = new FileInputStream(source);
                //System.out.println(this.getDropClient().getRequestConfig().httpRequestor.);
                DbxEntry.File metadata = this.getDropClient().uploadFile(destination, DbxWriteMode.force(), inputFile.length(), in);
                //DbxEntry.File metadata =this.getDropClient().uploadFile(destination, DbxWriteMode.force(), -1, in);


            } catch (DbxException e) {
                System.err.println("[erreur --> copy --> ] "+ destination);

                //e.printStackTrace();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                IOUtil.closeInput(in);
            }
        }
    }
    public String getTempFichier(String source){
        FileOutputStream outputStream = null;
        File f = null;
        try {
            String extension=".xlsx";
            if (source.contains("xlsm")) extension=".xlsm";
            f = File.createTempFile("tmp", extension, new File("/tmp"));

            outputStream = new FileOutputStream(f);
            DbxEntry.File downloadedFile = this.getDropClient().getFile(source,
                    null, outputStream);
            //System.out.println("Metadata: " + downloadedFile.toString());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (DbxException e) {
            e.printStackTrace();
        }
        return f.getAbsolutePath();


    }
    public void getFichier(String source,String destination){
        FileOutputStream outputStream = null;
        try {
            outputStream = new FileOutputStream(destination);
            DbxEntry.File downloadedFile = this.getDropClient().getFile(source,
                    null, outputStream);
            //System.out.println("Metadata: " + downloadedFile.toString());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (DbxException e) {
            e.printStackTrace();
        }


    }

    /**
     * Get liste fichier dans une liste
     * @param Path
     * @return
     */
    public ArrayList<String> getlisteFichier(String Path){
        ArrayList <String>resultat = new ArrayList<String>();
        DbxEntry.WithChildren listing = null;
        try {
            listing = this.getDropClient()
                    .getMetadataWithChildren(Path);
            if (listing == null ) return resultat;
            for (DbxEntry child : listing.children) {
                if (child.isFile()) resultat.add(child.name);
            }
        } catch (DbxException e) {
            e.printStackTrace();
        }
        return resultat;
    }

    public boolean fileExist(String Path) {
        boolean res = false;
        ArrayList <String>resultat = new ArrayList<String>();
        DbxEntry listing = null;

        try {
            listing = this.getDropClient()
                    .getMetadata(Path);
           // System.out.println (Path+" --_> "+listing);
            if (listing != null  ) res = true;
        } catch (DbxException e) {
            e.printStackTrace();
        }
        return res;
    }

    public boolean createDirectory(String Path) {
        boolean res=false;
        DbxEntry.Folder listing = null;

        try {
            listing = this.getDropClient()
                   .createFolder(Path);
            // System.out.println (Path+" --_> "+listing);
            if (listing != null  ) res = true;
        } catch (DbxException e) {
            e.printStackTrace();
        }
        return res;

    }
    public boolean removeDirectory(String Path) {
        boolean res=false;

        try {
            this.getDropClient()
                    .delete(Path);
            // System.out.println (Path+" --_> "+listing);

        } catch (DbxException e) {
            e.printStackTrace();
        }
        return res;

    }

    public ArrayList<String> getlisteDirectory(String Path) {
        ArrayList <String>resultat = new ArrayList<String>();
        DbxEntry.WithChildren listing = null;
        try {
            listing = this.getDropClient()
                    .getMetadataWithChildren(Path);

            for (DbxEntry child : listing.children) {
                if (child.isFolder()) resultat.add(child.name);
            }
        } catch (DbxException e) {
            e.printStackTrace();
        }
        return resultat;
    }

    public void syncDiretory(String PathHomeUser, String PathAdmin) {
        File folder = new File(PathHomeUser);
        File[] listOfFiles = folder.listFiles();

        //TEST COMPARAISON
        for (File file : listOfFiles) {
            if (file.isFile()) {

                this.putFichier(PathHomeUser+"/"+file.getName(),PathAdmin+"/"+file.getName());



            }


        }
    }
     /*   System.err.println("Debut Test ");

        DbxAuthInfo authInfo=null;
        try {
            authInfo = DbxAuthInfo.Reader.readFromFile(compte.accessToken);
        }
        catch (JsonReader.FileLoadException ex) {
            System.err.println("Error loading <auth-file>: " + ex.getMessage());
            //System.exit(1); return;
        }

        // Create a DbxClient, which is what you use to make API calls.
        String userLocale = Locale.getDefault().toString();
        DbxRequestConfig requestConfig = new DbxRequestConfig("examples-account-info", userLocale);
        DbxClient dbxClient = new DbxClient(requestConfig, authInfo.accessToken, authInfo.host);

        // Make the /account/info API call.
        DbxAccountInfo dbxAccountInfo=null;
        try {
            dbxAccountInfo = dbxClient.getAccountInfo();
        }
        catch (DbxException ex) {
            ex.printStackTrace();
            System.err.println("Error in getAccountInfo(): " + ex.getMessage());

        }
              System.out.println("User's account info: " + dbxAccountInfo.toStringMultiline());


        //write file
        InputStream in=null;
        try {
            in = new FileInputStream("/tmp/test.xlsx");
            DbxEntry.File metadata = dbxClient.uploadFile("/Apps/test.xlsx", DbxWriteMode.add(), -1, in);


        } catch (DbxException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
            }
      finally {
        IOUtil.closeInput(in);
        }
       // get liste file

            DbxEntry.WithChildren listing = null;
            try {
                listing = dbxClient
                        .getMetadataWithChildren("/Apps");
                System.out.println("Files List:");
                for (DbxEntry child : listing.children) {
                    System.out.println("	" + child.name + ": " + child.toString());
                }
            } catch (DbxException e) {
                e.printStackTrace();
            }

            //GEt file from dropbox
            FileOutputStream outputStream = null;
            try {
                outputStream = new FileOutputStream("/tmp/test-get.xlsx");
                DbxEntry.File downloadedFile = dbxClient.getFile("/Apps/test.xlsx" ,
                        null, outputStream);
                System.out.println("Metadata: " + downloadedFile.toString());
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (DbxException e) {
                e.printStackTrace();
            }


             finally {
                try {
                    outputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }*/




}
