package models.TraitementExcel;
import Util.MyUtilLib;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.poifs.filesystem.NPOIFSFileSystem;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.xmlbeans.XmlObject;
import play.libs.Json;

import java.io.*;
import java.util.*;

/**
 * Created by richard on 28/04/15.
 */


// Identifiant des types sur couleur
// Pour samedi/Dimanche
//


class VDay{
   // double valHaut=-1;
    double valBas=0.0;
    String lblHaut="";
    //String lblBas="??";
    boolean isOpen=false;
    @Override
    public String toString(){
        String res="";

        res = res + lblHaut + " "+valBas;

        return res;

    }

    public  JsonNode getJson() {
        ObjectNode info = Json.newObject();
       /* if (valHaut>=0)
            info.put("duree_h",valHaut);
        else {*/
            info.put("inf_h",lblHaut);
        //}
       // if (valBas>=0)
            info.put("duree_b",valBas);
       // else {
        //    info.put("inf_b",lblBas);

        //}
        info.put("isOpen",isOpen);

        return info;
    }

}
public class TrameBase extends ExcelModele{
    private static final String ISFERIE = "F";
    String templateName="trameBase-model.xlsx";
    public static double ID   = 76000.1;
    private cellCase CDepartAnnee =new cellCase(0,14);
    private cellCase  CdesCP=new cellCase(30,5);

    private cellCase  TypeSamediDimanche=new cellCase(29,0);
    private cellCase  TypeNotExist=new cellCase(30,5);
    private cellCase  TypeFerie=new cellCase(29,5);
    //Identification
    private cellCase  CadresseMail=new cellCase(0,6);
    private cellCase  Cnom=new cellCase(1,6);
    private cellCase  Cprenom=new cellCase(1,14);
    //Id

    private String adresseMail="NO mail";
    private String nom="NO fname";
    private String prenom="NO name";
    private int nbMois= 12;
    private int nbJourMo= 31;
    private long annee;
    XSSFWorkbook wb;
    private int PREMIER_MOIS=5;
    private int PREMIER_JOUR=1;
    private HashMap<String,VDay> listeJour;
    //private Boolean SamediChome=true;
    //private Boolean DimanceChome=true;

    public TrameBase(){
         listeJour=new HashMap<String,VDay>();
        for (int i=1; i <=12 ; i++) {
            for (int j=1; j <=31 ; j++) {
                listeJour.put(j+"-"+i,new VDay());
            }
        }
    }
    @Override
    public ObjectNode getResultat() {
        ObjectNode info = Json.newObject();
        info.put("trameBase",ID);
        ArrayNode arr = resultat.arrayNode();
        Set cles = listeJour.keySet();
        Iterator it = cles.iterator();
        while (it.hasNext()) {
            String cle = (String) it.next(); // tu peux typer plus finement ici
            VDay valeur = listeJour.get(cle);
            ObjectNode inf = Json.newObject();
            inf.put("key",cle);
            inf.put("value",valeur.getJson());
            arr.add(inf);
        }

        resultat.put("jours",arr);
        resultat.put("info",info);


        return resultat;
    }
    @Override
    public void initFromFile(String file) {
        OPCPackage pkg = null;
        try {
            pkg = OPCPackage.open(new File(file));
            wb = new XSSFWorkbook(pkg);

            firstGenerate();

            wb=null;

            pkg.close();

        } catch (InvalidFormatException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    @Override
    public void initFromTemplate(String s){
        this.fileTemplate=s;
        OPCPackage pkg = null;
        try {
            pkg = OPCPackage.open(new File(s));
            wb = new XSSFWorkbook(pkg);

            firstGenerate();

            wb=null;

            pkg.close();

        } catch (InvalidFormatException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void initFromTemplate(){
        OPCPackage pkg = null;
        try {
            pkg = OPCPackage.open(new File(this.getPathModele()+this.templateName));
             wb = new XSSFWorkbook(pkg);

            firstGenerate();

            wb=null;

            pkg.close();

        } catch (InvalidFormatException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Ici on genere une première version du WB
     * En prenant en compte un certains nomrbre de point
     * les jours absents (mois à 31 jours)
     * les samedis et dimanche chomé
     * les vacances
     * les jours féries
     *
     *
     */
    private void firstGenerate() {
        Sheet feuille = wb.getSheetAt(0);
        this.annee = MyUtilLib.getLongFrom(feuille.getRow(CDepartAnnee.row),CDepartAnnee.col);
        //System.out.println("--> "+ annee);
        Cell cell = feuille.getRow(CdesCP.row).getCell(CdesCP.col);
        //System.out.println("--> " +cell.getStringCellValue());

        Cell c = feuille.getRow(CadresseMail.row).getCell(CadresseMail.col);
        if (c!=null) this.adresseMail= c.getStringCellValue();
        c = feuille.getRow(Cprenom.row).getCell(Cprenom.col);
        if (c!=null) this.prenom= c.getStringCellValue();
        c = feuille.getRow(Cnom.row).getCell(Cnom.col);
        if (c!=null) this.nom= c.getStringCellValue();


        for (int m=0;m<24; m =m+2) {
            int mois=0;
            int p=0;
           // if ((m%2)==0){
            //    mois=(m/2)+1;
            //}
            //else{
                mois =((m+1)/2)+1;
            //}

            for (int j = 0; j < 31; j++) {

                Row row = feuille.getRow(PREMIER_MOIS + m);
                Row row2 = feuille.getRow(PREMIER_MOIS + m+1);

                //if  (cell!= null)System.out.println(cell.getCellType());
                Cell Hcell = row.getCell(j+PREMIER_JOUR);
                Cell Bcell = row2.getCell(j+PREMIER_JOUR);
              //  double valHaut=-1;
              //  double valBas=-1;
              //  String lblHaut="";
              //  String lblBas="";
                VDay dayCrt= this.listeJour.get((j+1)+"-"+mois);




                /*if( (Hcell!= null) && (Hcell.getCellType()==Cell.CELL_TYPE_STRING)){
                   // System.out.println(cell.getStringCellValue());
                    dayCrt.lblHaut= Hcell.getStringCellValue();

                }
                if( (Hcell!= null) && (Hcell.getCellType()==Cell.CELL_TYPE_NUMERIC)){
                    //System.out.println(Hcell.getNumericCellValue());
                    dayCrt.valHaut=Hcell.getNumericCellValue();

                }
                if( (Bcell!= null) && (Bcell.getCellType()==Cell.CELL_TYPE_STRING)){
                    // System.out.println(cell.getStringCellValue());
                    dayCrt.lblBas= Bcell.getStringCellValue();


                }
                if( (Bcell!= null) && (Bcell.getCellType()==Cell.CELL_TYPE_NUMERIC)){
                    //System.out.println(Bcell.getNumericCellValue());
                    dayCrt.valBas=Bcell.getNumericCellValue();

                }*/

               // Mise à jour des labels
               if( (Hcell!= null) && (Hcell.getCellType()==Cell.CELL_TYPE_STRING)){
                    // System.out.println(cell.getStringCellValue());
                    dayCrt.lblHaut= Hcell.getStringCellValue();

                }
                /*else {
                    dayCrt.lblHaut="";
                }*/
                if( (Bcell!= null) && (Bcell.getCellType()==Cell.CELL_TYPE_NUMERIC)){
                    //System.out.println(Bcell.getNumericCellValue());
                    dayCrt.valBas=Bcell.getNumericCellValue();

                }
                ///else{*/
                    //if (dayCrt.lblBas.length()>0)
                       // dayCrt.valBas=0.0;

               // }
                // Check spec value()
                // Jour MARQUE F
                if (dayCrt.lblHaut.compareToIgnoreCase(TrameBase.ISFERIE)==0){
                   // dayCrt.valBas=-5.0;
                }
                //JOUR VIDE
                if (dayCrt.lblHaut.length()==0){
                    //dayCrt.valBas=-6.0;
                }

               // System.out.println((j+1)+"--"+mois+ "  " + dayCrt);
            }
        }


        /*for (Row row : feuille) {
            Cell cell = row.getCell(12, Row.RETURN_BLANK_AS_NULL);
            if (cell!= null) {
                if (cell.getCellType()==Cell.CELL_TYPE_STRING) {
                    System.out.println(row.getRowNum() + "--> " + cell.getStringCellValue());
                }
            }

        }*/

    }

    /**
     * Ici je crée un fichier xlsx à partir des données du modèle en utilsant un patron vide
     *
     * @param s
     *
     */
    @Override
    public void saveToFile(String s) {
        saveToFile(s,this.getPathModele()+"trameBase-model.xlsx");
    }
    @Override
    public void saveToFile(String s,String template) {
        OPCPackage pkg = null;
        try {

            InputStream inp=new FileInputStream(template);

            Workbook workbook = WorkbookFactory.create(inp);
            inp.close();
            populate(workbook);


            // FileOutputStream fileOut = new FileOutputStream(s);
            // wb.write(fileOut);

            //fileOut.close();

            FileOutputStream fileOut = new FileOutputStream(s);
            workbook.write(fileOut);
            fileOut.close();




        } catch (InvalidFormatException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    @Override
    public void saveToFileFromTemplate(String s) {
        OPCPackage pkg = null;
        try {
            InputStream inp=null;
            if (this.fileTemplate.length()>1){
                inp = new FileInputStream(this.fileTemplate);
            }else {
              inp = new FileInputStream(this.getPathModele() + this.templateName);
            }

            Workbook workbook = WorkbookFactory.create(inp);
            inp.close();
            populate(workbook);


           // FileOutputStream fileOut = new FileOutputStream(s);
           // wb.write(fileOut);

            //fileOut.close();

            FileOutputStream fileOut = new FileOutputStream(s);
            workbook.write(fileOut);
            fileOut.close();




        } catch (InvalidFormatException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    private void populate(Workbook workbook) {
        Sheet feuille = workbook.getSheetAt(0);

        //Id
        Cell c = feuille.getRow(CadresseMail.row).getCell(CadresseMail.col);
        c.setCellValue(this.adresseMail);
        c = feuille.getRow(Cprenom.row).getCell(Cprenom.col);
        c.setCellValue(this.prenom);
        c = feuille.getRow(Cnom.row).getCell(Cnom.col);
        c.setCellValue(this.nom);


        //
        for (int m=0;m<24; m =m+2) {
            int mois = 0;
            int p = 0;
            if ((m % 2) == 0) {
                mois = (m / 2) + 1;
            } else {
                mois = ((m + 1) / 2) + 1;
            }

            for (int j = 0; j < 31; j++) {


                Row row = feuille.getRow(PREMIER_MOIS + m);
                Row row2 = feuille.getRow(PREMIER_MOIS + m + 1);

                //if  (cell!= null)System.out.println(cell.getCellType());
                Cell Hcell = row.getCell(j + PREMIER_JOUR);
                Cell Bcell = row2.getCell(j + PREMIER_JOUR);
                VDay dayCrt= this.listeJour.get((j + 1) + "-" + mois);
               /* if (dayCrt.valBas>0) {
                    Bcell.setCellValue(dayCrt.valBas );
                }
                else {
                    Bcell.setCellValue(dayCrt.lblBas);
                }


                if (dayCrt.valHaut>0) {
                    Hcell.setCellValue(dayCrt.valHaut );
                }
                else {
                    Hcell.setCellValue(dayCrt.lblHaut);
                }*/


                // TO do à compléter pour les cases

                Hcell.setCellValue(dayCrt.lblHaut);

               if (dayCrt.lblHaut.length()>0) {

                   Bcell.setCellValue(dayCrt.valBas );
               }

            }
        }


    }

    public void mergePlanning(PlanningRoulement plan) {

        Set cles = listeJour.keySet();
        Iterator it = cles.iterator();
        this.adresseMail=plan.getAdresseMail();
         this.nom=plan.getNom();
        this.prenom = plan.getPrenom();


        while (it.hasNext()) {
            String cle = (String) it.next(); // tu peux typer plus finement ici
            VDay valeur = listeJour.get(cle);
            //System.out.println(cle + " " + valeur);
            // Calcul de la date du jour
            String[] dateString = cle.split("-");
            Calendar dateCourante = Calendar.getInstance();

            dateCourante.set(Calendar.YEAR,(int)this.annee);
            dateCourante.set(Calendar.DAY_OF_MONTH,Integer.parseInt(dateString[0]));
            dateCourante.set(Calendar.MONTH,Integer.parseInt(dateString[1]));
            Date dateCrt = MyUtilLib.formatDate(dateString[0],dateString[1],this.annee+"");
            // On regarde si la date est dans la date géré par le planning
            if (plan.hasDate(dateCrt)){
                // on demande a plan le montant des TE si TE;
                if (valeur.lblHaut.compareTo(ExcelModele.LBL_TE)==0) {

                    double dureTe=plan.getDurePresence(dateCrt);
                    valeur.valBas=dureTe;
                }


            }

        }

        //Mise à jour nom prenom fonction quotité




    }

    public void createPlanSemaineAnne(PlanningRoulement plan,String Path) {



        int numero =0;
        Date dateDebut = null;
        Date dateCrt=null;

        String []masque = {"NP",
                "NP",
                "NP",
                "NP",
                "NP",
                "NP",
                "NP",

        };
        for (int mois=1; mois< 13 ; mois ++) {
            for (int day = 1; day < 32; day++) {

                String cle = day+"-"+mois;
                if (!listeJour.containsKey(cle))continue;






                String[] dateString = cle.split("-");

                dateCrt= MyUtilLib.formatDate(dateString[0], dateString[1], this.annee + "");

                if (dateDebut==null) {
                    dateDebut= MyUtilLib.formatDate(dateString[0], dateString[1], this.annee + "");
                }
                Calendar cal = Calendar.getInstance();
                cal.setTime(dateCrt);


                // Jour et filtre
                VDay valeur = listeJour.get(cle);
                String filtre ="";
                if (valeur.lblHaut.compareTo(ExcelModele.LBL_TE)!=0) {
                    filtre=valeur.lblHaut;
                }

                switch (cal.get(Calendar.DAY_OF_WEEK)){
                    //dimanche
                    case (Calendar.SUNDAY):
                        masque[6]=filtre;
                        break;
                    //lundi
                    case (Calendar.MONDAY):
                        masque[0]=filtre;
                        break;
                    //mardi
                    case (Calendar.TUESDAY):
                        masque[1]=filtre;
                        break;
                    //mercredi
                    case (Calendar.WEDNESDAY):
                        masque[2]=filtre;
                        break;
                    //jeudi
                    case (Calendar.THURSDAY):
                        masque[3]=filtre;
                        break;
                    //vendredi
                    case (Calendar.FRIDAY):
                        masque[4]=filtre;
                        break;
                    // samedi
                    case (Calendar.SATURDAY):
                        masque[5]=filtre;
                        break;

                    default:
                        break;
                }
                // On a un lundi ?


                if (cal.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
                 /*   System.out.println(numero + " " + dateDebut + " " +dateCrt + "  \n"
                            +masque[0]+"\n"
                            +masque[1]+"\n"
                            +masque[2]+"\n"
                            +masque[3]+"\n"
                            +masque[4]+"\n"
                            +masque[5]+"\n"
                            +masque[6]+"\n"+
                            "*********************************************************");
*/
                    plan.generateSemaine(numero, dateDebut, dateCrt, masque, Path);
                    numero++;
                    dateDebut = null;



                }
                //dernier jour de l'année
                else if (mois==12 && day==31){
                    plan.generateSemaine(numero, dateDebut, dateCrt, new String[7], Path);
                }




            }
        }




    }

    public String getAdresseMail() {
        return adresseMail;
    }
}
