package models.TraitementExcel;

import Util.MyUtilLib;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.IOException;

/**
 * Created by richard on 27/05/15.
 */
public class PlanningSemaine extends PlanningRoulement {
    public static double ID = 71000.1; // type // version
    public double semaine;


    public boolean validation(String Filename) {
        boolean res= super.validation(Filename);

        OPCPackage pkg = null;
        try {
            pkg = OPCPackage.open(new File(Filename));
            XSSFWorkbook wb = new XSSFWorkbook(pkg);
            Sheet feuille = wb.getSheetAt(0);
            // get Semaine
            System.out.println(feuille.getRow(CdateSemaine.row).getCell(CdateSemaine.col));
            System.out.println(feuille.getRow(CdateSemaine.row+1).getCell(CdateSemaine.col));
            this.semaine = MyUtilLib.getDoubleFrom(feuille.getRow(CdateSemaine.row),CdateSemaine.col);
            this.DateDebut=MyUtilLib.formatDateFromString(feuille.getRow(CdateDebut.row).getCell(CdateDebut.col).getStringCellValue());
            this.DateFin=MyUtilLib.formatDateFromString(feuille.getRow(CdateFin.row).getCell(CdateFin.col).getStringCellValue());


            pkg.close();

        } catch (InvalidFormatException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }



        return res;
    }


    public double getSemaine() {
        return semaine;
    }
}
