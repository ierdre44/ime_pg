package models.TraitementExcel;


import Util.MyUtilLib;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.eventusermodel.XSSFReader;
import org.apache.poi.xssf.model.SharedStringsTable;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFRichTextString;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.xml.sax.*;
import org.xml.sax.helpers.DefaultHandler;
import org.xml.sax.helpers.XMLReaderFactory;
import play.libs.Json;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by richard on 27/04/15.
 */
public class PlanningCesiFormateur extends ExcelModele {

    public boolean validation(String Filename) {
        boolean res=false;
        OPCPackage pkg = null;
        try {
            pkg = OPCPackage.open(new File(Filename));
            XSSFWorkbook wb = new XSSFWorkbook(pkg);
            parseStructure(wb);

            pkg.close();

        } catch (InvalidFormatException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return res;

    }

    private void parseStructure(XSSFWorkbook wb) {
        Sheet sheet = wb.getSheetAt(0);
        Workbook wbres = new XSSFWorkbook();

        Sheet sheetres = wbres.createSheet("new sheet");
        int count =0;
        String intitule="";
        String Datedebut="";
        for (Row row : sheet) {


           // for (Cell cell : row) {
           //     if (cell.getCellType() == Cell.CELL_TYPE_STRING) {
           //         System.out.print(cell.getStringCellValue().replace("\n", " ") + "-");

           //     }
           //     System.out.println(" ");

            //}
            String formateur="";

            Cell crt = row.getCell(5, Row.RETURN_BLANK_AS_NULL);
            if (crt!=null) formateur= crt.getStringCellValue();



            crt = row.getCell(4, Row.RETURN_BLANK_AS_NULL);

            crt = row.getCell(0, Row.RETURN_BLANK_AS_NULL);
            String DateFin=crt.getStringCellValue();

            if (crt!=null) {
               // if (Datedebut.compareToIgnoreCase(crt.getStringCellValue())!=0){
                if (formateur.toLowerCase().contains("urunuela".toLowerCase())  ){

                    if (DateFin.compareToIgnoreCase(Datedebut)!=0) {
                        Row res = sheetres.createRow(count);
                        count++;
                        Cell rescell = res.createCell(0);
                        crt = row.getCell(4, Row.RETURN_BLANK_AS_NULL);
                        intitule = crt.getStringCellValue();
                        rescell.setCellValue("Cesi Aso nantes");
                        rescell = res.createCell(1);
                        rescell.setCellValue(intitule);
                        //rescell = res.createCell(2);
                        //rescell.setCellValue(Datedebut);
                        rescell = res.createCell(2);
                        rescell.setCellValue(DateFin);
                         crt = row.getCell(0, Row.RETURN_BLANK_AS_NULL);

                        rescell = res.createCell(4);
                        rescell.setCellValue("FFMSI");
                        Datedebut = crt.getStringCellValue();
                    }

                }
                //Intitulé

            }

        }

        try {
            FileOutputStream fileOut = new FileOutputStream("/tmp/res-1.xlsx");

            wbres.write(fileOut);
            fileOut.close();

        } catch (IOException e) {
            e.printStackTrace();
        }


    }
}
