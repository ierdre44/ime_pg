package models.TraitementExcel;

import Util.MyUtilLib;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.common.io.Files;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.openxml4j.exceptions.OpenXML4JException;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellReference;
import org.apache.poi.xssf.eventusermodel.XSSFReader;
import org.apache.poi.xssf.model.SharedStringsTable;
import org.apache.poi.xssf.usermodel.*;
import org.apache.xmlbeans.*;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.openxmlformats.schemas.drawingml.x2006.main.CTTextBody;
import org.openxmlformats.schemas.drawingml.x2006.main.CTTextBodyProperties;
import org.openxmlformats.schemas.drawingml.x2006.main.CTTextListStyle;
import org.openxmlformats.schemas.drawingml.x2006.main.CTTextParagraph;
import org.w3c.dom.Node;
import org.xml.sax.*;
import org.xml.sax.ext.LexicalHandler;
import org.xml.sax.helpers.DefaultHandler;
import org.xml.sax.helpers.XMLReaderFactory;
import play.libs.Json;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLStreamReader;
import java.io.*;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by richard on 20/04/15.
 */
/***
    Source
 http://poi.apache.org/spreadsheet/how-to.html#xssf_sax_api
 voir :
 http://stackoverflow.com/questions/9372630/reading-an-excel-sheet-using-pois-xssf-and-sax-event-api
 https://poi.apache.org/spreadsheet/quick-guide.html
 */
/*/*class LowPlanningRoulementSaxe{
    private final PlanningRoulement planningRoulement;

    public LowPlanningRoulementSaxe(PlanningRoulement planningRoulement) {
        this.planningRoulement  = planningRoulement;
    }

    public void processOneSheet(String filename) throws Exception {
        OPCPackage pkg = OPCPackage.open(filename);
        XSSFReader r = new XSSFReader( pkg );
        SharedStringsTable sst = r.getSharedStringsTable();

        XMLReader parser = fetchSheetParser(sst);

        // rId2 found by processing the Workbook
        // Seems to either be rId# or rSheet#

        InputStream sheet2 = r.getSheet("rId1");

        InputSource sheetSource = new InputSource(sheet2);
        parser.parse(sheetSource);
        sheet2.close();
    }

    public void processAllSheets(String filename) throws Exception {
        OPCPackage pkg = OPCPackage.open(filename);
        XSSFReader r = new XSSFReader( pkg );
        SharedStringsTable sst = r.getSharedStringsTable();

        XMLReader parser = fetchSheetParser(sst);

        Iterator<InputStream> sheets = r.getSheetsData();
        while(sheets.hasNext()) {

            InputStream sheet = sheets.next();
           // System.out.println("Processing new sheet:\n" + ((XSSFReader.SheetIterator)sheets).getSheetName());


            InputSource sheetSource = new InputSource(sheet);
            parser.parse(sheetSource);
            sheet.close();
            System.out.println("");
        }
    }
    public XMLReader fetchSheetParser(SharedStringsTable sst) throws SAXException {
        XMLReader parser =
                XMLReaderFactory.createXMLReader(
                        "org.apache.xerces.parsers.SAXParser"
                );
        ContentHandler handler = new SheetHandler(sst);
        parser.setContentHandler(handler);
        return parser;
    }
    private static class SheetHandler extends DefaultHandler {
        private SharedStringsTable sst;
        private String lastContents;
        private boolean nextIsString;

        private SheetHandler(SharedStringsTable sst) {
            this.sst = sst;
        }

        public void startElement(String uri, String localName, String name,
                                 Attributes attributes) throws SAXException {
            // c => cell
            if(name.equals("c")) {
                // Print the cell reference
               // System.out.print(attributes.getValue("r") + " - ");
                // Figure out if the value is an index in the SST
                String cellType = attributes.getValue("t");
               *//* for (int i = 0; i < attributes.getLength(); i++)
                    System.out.println(attributes.getValue(i));*//*

                if(cellType != null && cellType.equals("s")) {
                    nextIsString = true;
                } else {
                    nextIsString = false;
                }
            }
            // Clear contents cache
            lastContents = "";
        }

        public void endElement(String uri, String localName, String name)
                throws SAXException {
            // Process the last contents as required.
            // Do now, as characters() may be called more than once
            if(nextIsString) {
                int idx = Integer.parseInt(lastContents);
                lastContents = new XSSFRichTextString(sst.getEntryAt(idx)).toString();
                nextIsString = false;
            }

            // v => contents of a cell
            // Output after we've seen the string contents
            if(name.equals("v")) {
                System.out.println(lastContents);
            }
        }

        public void characters(char[] ch, int start, int length)
                throws SAXException {
            lastContents += new String(ch, start, length);
        }
    }


}*/

/**
 * Classe pour wrapper avec les vevent Du modèle
 * utilisation en interne pour l'instant
 */

class IVEvent {
    Date hDebut;
    Date hFin;
    int jour;
    String libelle;
    private double duree;
    //  CodeCell style;

    public IVEvent (double heureDebut,
                    double heureFin,
                    int jour,
                    String libelle
                    ){
            this.jour= jour;

        this.libelle= libelle;
        try {
        DateFormat dfm = new SimpleDateFormat("HH:mm");
        double hour=(int) heureDebut;
        double minute =  (heureDebut - hour);
        minute = (int) (minute*60);

            hDebut = dfm.parse((int)hour+":"+(int)minute);

            hour=(int) heureFin;

            minute =  (heureFin - hour);
            minute = (int) (minute*60);
         //   System.out.println(hour+ "  " + minute);

            hFin = dfm.parse((int)hour+":"+(int)minute);
        } catch (ParseException e) {
            e.printStackTrace();
        }


    }
    public JsonNode getJson() {
        ObjectNode info = Json.newObject();
       info.put("jour",jour);
        info.put("debut",MyUtilLib.formatTime(hDebut));
        info.put("fin",MyUtilLib.formatTime(hFin));
        info.put("libelle",libelle);
      //  info.put("style",style.getJson());

        return info;
    }


    public double getdureeInMinute() {

        long diff = this.hFin.getTime() - this.hDebut.getTime();
        //long diffSeconds = diff / 1000 % 60;
        long diffMinutes = diff / (60 * 1000) % 60;


        //System.out.println( diff + "  " + (diffMinutes));

        return ((double)diffMinutes);
    }
}

/**
 * Class utilisé pour avoir des infos sur une jour de la semaine et la charge horaire associé
 *
 */
class IVDay{
    double durePresence;
    int jour;

    public IVDay(double duree, int day) {
        this.durePresence= duree;
        this.jour= day;
    }

    public  JsonNode getJson() {
        ObjectNode info = Json.newObject();
        info.put("duree",durePresence);
        info.put("day",jour);

        return info;
    }
}
class CodeCell {
    String backgroundColor="";
    String foregroudColor="";
    Short fillPattern=0;

    CodeCell(String backgroundColor, String foregroudColor, Short fillPattern) {
        this.backgroundColor = backgroundColor;
        this.foregroudColor = foregroudColor;
        this.fillPattern = fillPattern;
    }

    public JsonNode getJson() {
        ObjectNode info = Json.newObject();
        info.put("backgroundColor",backgroundColor);
        info.put("foregroudColor",foregroudColor);
        info.put("fillPattern",fillPattern);
        return info;
    }
}
class Intervalle{
    CodeCell code;

}

public class PlanningRoulement extends ExcelModele {
    public static double getID() {
        return ID;
    }

    public double getIdLu() {
        return idLu;
    }

    public Date getDateDebut() {
        return DateDebut;
    }

    public Date getDateFin() {
        return DateFin;
    }

    public String getAdresseMail() {
        return AdresseMail;
    }

    public String getNomPrenom() {
        return NomPrenom;
    }

    public String getFonction() {
        return fonction;
    }

    public String getService() {
        return service;
    }

    public double getQuotite() {
        return quotite;
    }

    public int getPosColonneHeure() {
        return posColonneHeure;
    }

    public int getValHeureDepart() {
        return valHeureDepart;
    }

    public int getColFinHeure() {
        return colFinHeure;
    }

    private static final int FIRST_ROW_CODE_COULEUR = 6;
    private static final int LAST_ROW_CODE_COULEUR = 10;
    private static final int COL_CODE_COULEUR = 3;
    private HashMap<String, CodeCell> listeLibelle;
    private ArrayList<IVEvent> listVEvent;

    private ArrayList<IVDay> listVDAY;
    public static double ID = 75000.1; // type // version
    private double idLu;
    protected Date DateDebut;
    protected Date DateFin;
    private String AdresseMail = "";
    private String NomPrenom;
    //CEllule a modifier pour creation planning semaine
    private cellCase  CID=new cellCase(0,1);
    protected cellCase  CdateSemaine=new cellCase(3,1);
    protected cellCase  CdateDebut=new cellCase(1,1);
    protected cellCase  CdateFin=new cellCase(2,1);

    //Poisiton de cellulle important
    //Position de la date de début et fin
    protected cellCase CdateDebutApplication=new cellCase(1,1);
    protected cellCase CdateFinApplication=new cellCase(2,1);

    public static CellReference refDebutZonePlanning = new CellReference("E7");
    public static CellReference refFinZonePlanning = new CellReference("BP11");

    ///**************
    // A ajouter
    // TO DO
    // N'existe pas dans le fichier excel .....
    private String Prenom;
    private String Nom;
    ///**************
    private String fonction;
    private String service;
    private double quotite;
    private double baseHoraire=39;

    public String getPrenom() {
        return Prenom;
    }

    public void setPrenom(String prenom) {
        Prenom = prenom;
    }

    public String getNom() {
        return Nom;
    }

    public void setNom(String nom) {
        Nom = nom;
    }

    public void setFonction(String fonction) {
        this.fonction = fonction;
    }

    public void setService(String service) {
        this.service = service;
    }

    public void setQuotite(double quotite) {
        this.quotite = quotite;
    }

    public double getBaseHoraire() {
        return baseHoraire;
    }

    public void setBaseHoraire(double baseHoraire) {
        this.baseHoraire = baseHoraire;
    }

    private int posColonneHeure = -1;
    private int valHeureDepart = -1;
    private int colFinHeure = -1;


    // surcharge pour getResultat ajout de specifcation
    public ObjectNode getResultat() {

        // ADD liste des code Couleurs
        Set cles = listeLibelle.keySet();
        Iterator it = cles.iterator();
        while (it.hasNext()) {
            String cle = (String) it.next(); // tu peux typer plus finement ici
            CodeCell valeur = listeLibelle.get(cle); // tu peux typer plus finement ici
            ObjectNode info2 = Json.newObject();
            info2.put("libelle", cle);
            info2.put("code", valeur.getJson());
            listeMessage.add(info2);

        }
        ObjectNode info = Json.newObject();
        info.put("idLu", idLu);
        info.put("ID", ID);
        info.put("DateDebut", MyUtilLib.formatDate(this.DateDebut));
        info.put("DateFin", MyUtilLib.formatDate(this.DateFin));
        info.put("AdresseMail", this.AdresseMail);
        info.put("IdNom", this.NomPrenom);
        info.put("Nom",this.Nom);
        info.put("Prenom",this.Prenom);
        info.put("fonction", this.fonction);
        info.put("service", this.service);
        info.put("quotite", this.quotite);
        info.put("heureDepart", this.valHeureDepart);
        info.put("basehoraire",this.baseHoraire);
        listeMessage.add(info);
        Iterator I = this.listVEvent.iterator();
        while (I.hasNext()) {

            IVEvent crt = (IVEvent) I.next();
            ObjectNode info2 = Json.newObject();
            info2.put("intervalle", crt.getJson());
            listeMessage.add(info2);
        }
        I = this.listVDAY.iterator();
        while (I.hasNext()) {

            IVDay crt = (IVDay) I.next();
            ObjectNode info2 = Json.newObject();
            info2.put("charge", crt.getJson());
            listeMessage.add(info2);
        }


        return resultat;
    }

    /**
     * Validation du fichier Excel en vue du traitement
     * Pas de moficifcation
     */
   /* public boolean validation(String Filename) {

        boolean res=false;

       // LowPlanningRoulementSaxe fichierEntree=new LowPlanningRoulementSaxe(this);
        try {
            fichierEntree.processAllSheets(Filename);
           //fichierEntree.processOneSheet(Filename);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return res;

    }*/
    //https://poi.apache.org/spreadsheet/quick-guide.html
    // palette
    // http://stackoverflow.com/questions/1499739/how-do-i-get-the-java-apache-poi-hssf-background-color-for-a-given-cell
    private static String[] getDate(String desc) {
        int count = 0;
        String[] allMatches = new String[2];
        Matcher m = Pattern.compile("(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\\d\\d").matcher(desc);
        while (m.find()) {
            allMatches[count] = m.group();
            count++;
        }
        return allMatches;
    }

    private boolean initStructure(XSSFWorkbook wb) {
        boolean res = true;
        listeLibelle = new HashMap<String, CodeCell>();
        listVEvent = new ArrayList<IVEvent>();
        listVDAY = new ArrayList<IVDay>();

        Sheet sheet = wb.getSheet("roulement");
        //Sheet sheet = wb.getSheetAt(0);

        //Maintenant on a fixe position date debut date Fin//

        Cell cellDatedebut=sheet.getRow(this.CdateDebutApplication.row).getCell(CdateDebutApplication.col);
        Cell cellDateFin=sheet.getRow(this.CdateFinApplication.row).getCell(CdateFinApplication.col);
        //System.out.println(" \n\n ----> \n "+cellDatedebut.getDateCellValue());

        //System.out.println(" \n\n ----> \n "+cellDateFin.getStringCellValue());
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        String dateInString = "01/01/2050";
        try {


            Date date = formatter.parse(dateInString);
            this.DateFin = date;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        try {


            if (cellDatedebut.getDateCellValue() == null) {
                this.addErrorInfo("Pas de date de debut");
                return false;
            } else {
                this.DateDebut = cellDatedebut.getDateCellValue();
            }
            if (cellDateFin.getDateCellValue() == null) {
                /*this.addMessageInfo(" Pas de date de fin ajout 10 ans pour calcul");
                SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
                String dateInString = "01/01/2050";
                try {
                    Date date = formatter.parse(dateInString);
                    this.DateFin = date;
                } catch (ParseException e) {
                    e.printStackTrace();
                }*/
            } else {
                this.DateFin = cellDateFin.getDateCellValue();

            }
        }
        // ajout du a erreur sur mes format de dates dans excel
        catch(IllegalStateException E){
            this.addErrorInfo("warning date mauvais format");
            try {
                this.DateDebut =formatter.parse(cellDatedebut.getStringCellValue().toString());


            if (cellDateFin.getStringCellValue()!=null){
                this.DateFin =formatter.parse(cellDateFin.getStringCellValue().toString());

            }
            } catch (ParseException e) {
                e.printStackTrace();
            }


        }

        /// END CALCUL des dates
        for (Row row : sheet) {
            for (Cell cell : row) {
                if (cell.getCellType() == Cell.CELL_TYPE_STRING) {
                    //System.out.println(cell.getStringCellValue());

                    String valCourante = cell.getStringCellValue();
                    //GEt cell font  color pour les codes
                    if ((row.getRowNum() >= FIRST_ROW_CODE_COULEUR) &&
                            (row.getRowNum() <= LAST_ROW_CODE_COULEUR)) {
                        if (cell.getColumnIndex() == COL_CODE_COULEUR) {

                            XSSFCellStyle style = (XSSFCellStyle) cell.getCellStyle();

                           /* System.out.println(cell.getStringCellValue()+" " +

                                            style.getFillBackgroundColorColor().getARGBHex() +"  "+
                                            style.getFillForegroundColorColor().getARGBHex()+ " " +
                                            style.getFillPattern()+
*/

                            //style.getFillForegroundColorColor().getARGBHex()+ " " +
                            //style.getFillBackgroundColorColor().getARGBHex()+ " "
                            // style.get
                            //                                 ""
                            //                         );

                            CodeCell codecrt = new CodeCell(this.getFillBackgroundColorColor(style),
                                    this.getFillForegroundColorColor(style),
                                    style.getFillPattern());
                            this.listeLibelle.put(cell.getStringCellValue(), codecrt);
                            //IndexedColors.values();


                        }

                    }
                    if (cell.getStringCellValue().contains("Mail")) {
                        Cell celltmp=row.getCell(cell.getColumnIndex() + 1);
                        this.AdresseMail=celltmp.getStringCellValue();
                    }
                    if (cell.getStringCellValue().contains("Nom")) {
                        Cell celltmp=row.getCell(cell.getColumnIndex() + 1);
                        this.Nom=celltmp.getStringCellValue();
                    }
                    if (cell.getStringCellValue().contains("Prénom")) {
                        Cell celltmp=row.getCell(cell.getColumnIndex() + 1);
                        this.Prenom=celltmp.getStringCellValue();
                    }
                    if (cell.getStringCellValue().contains("Emploi")) {
                        Cell celltmp=row.getCell(cell.getColumnIndex() + 1);
                        this.fonction=celltmp.getStringCellValue();
                    }
                    if (cell.getStringCellValue().contains("ETP")) {
                        this.quotite = MyUtilLib.getDoubleFrom(row, cell.getColumnIndex()+1);
                    }
                    //identification
                    if (cell.getStringCellValue().endsWith("#ID#")) {
                        Cell idcell = row.getCell(cell.getColumnIndex() + 1);
                        this.idLu = MyUtilLib.getDoubleFrom(row, cell.getColumnIndex() + 1);
                        // JE considere cette case avec le style vide utilisé comme référence
                        XSSFCellStyle style = (XSSFCellStyle) cell.getCellStyle();
                        CodeCell codecrt = new CodeCell(this.getFillBackgroundColorColor(style),
                                this.getFillForegroundColorColor(style),
                                style.getFillPattern());
                        this.listeLibelle.put("NULL", codecrt);

                    }
                    if (cell.getStringCellValue().contains("Base  planning")) {
                        this.baseHoraire=MyUtilLib.getDoubleFrom(row, cell.getColumnIndex()+1);
                    }
                    //Date
                    // regex date
                    //
                    //http://howtodoinjava.com/2014/11/12/java-regex-date-format-validation/
                    //http://stackoverflow.com/questions/18591242/java-extract-date-from-string-using-regex-failing
                    //
                    //if (cell.getStringCellValue().contains("Horaire applicable à compter")) {
                        /*String[] dates = getDate(cell.getStringCellValue());
                        //System.out.println(dates[0]);
                        //System.out.println(dates[1]);
                        this.DateDebut = MyUtilLib.formatDateFromString(dates[0]);
                        this.DateFin = MyUtilLib.formatDateFromString(dates[1]);*/




                    //}


                    // Nom adrese et colonne administrative
                   /* if (cell.getStringCellValue().contains("NOM  et  Qualité")) {
                        Row crt = sheet.getRow(row.getRowNum() + 1);
                        this.AdresseMail = (crt.getCell(cell.getColumnIndex())).getStringCellValue();
                        crt = sheet.getRow(row.getRowNum() + 2);
                        this.NomPrenom = (crt.getCell(cell.getColumnIndex())).getStringCellValue();
                        crt = sheet.getRow(row.getRowNum() + 3);
                        this.fonction = (crt.getCell(cell.getColumnIndex())).getStringCellValue();
                        crt = sheet.getRow(row.getRowNum() + 4);
                        this.service = (crt.getCell(cell.getColumnIndex())).getStringCellValue();
                        crt = sheet.getRow(row.getRowNum() + 5);
                        this.quotite = MyUtilLib.getDoubleFrom(crt, cell.getColumnIndex());

                    }*/
                    // heure et depart et position
                    /*if ((valCourante.contains("h")) &&
                            (valCourante.length() <= 3) &&
                            (this.posColonneHeure < 0)) {
                        this.posColonneHeure = cell.getColumnIndex();
                        this.valHeureDepart = Integer.parseInt((String) (valCourante.split("h")[0]));


                    }*/
                    if (cell.getStringCellValue().contains("#DEBUT#")) {
                        Cell celltmp=row.getCell(cell.getColumnIndex() + 1);

                        this.posColonneHeure = cell.getColumnIndex()+1
                        ;
                        this.valHeureDepart = Integer.parseInt((String) (celltmp.getStringCellValue().split("h")[0]));
                     //   System.out.println( "POS " + celltmp.getStringCellValue());

                    }
                    // Colonne de fin heure
                    if (cell.getStringCellValue().contains("#FIN#")) {
                        this.colFinHeure = cell.getColumnIndex()
                        ;
                    }


                }


            }
        }

        // IDENTIFICATION CELLULE HORAIRE
        double duree = 0;
        for (Row row : sheet) {

            int day = 0;
            String libelleRef = "NULL";
            int offset = -1;
            for (Cell cell : row) {
                //Style
                if (cell != null) {

                    // Cadre des cases
                    if ((row.getRowNum() >= FIRST_ROW_CODE_COULEUR) &&
                            (row.getRowNum() <= FIRST_ROW_CODE_COULEUR + 4) &&
                            (cell.getColumnIndex() <= this.colFinHeure) &&
                            (cell.getColumnIndex() > COL_CODE_COULEUR)) {
                        XSSFCellStyle style = (XSSFCellStyle) cell.getCellStyle();
                       // System.out.println( "POS " + cell.getColumnIndex()+ " " + row.getRowNum());
                         if (cell.getCellType() == Cell.CELL_TYPE_STRING) {
                             System.out.println("\n\n-->"+cell.getStringCellValue());
                        }


                        String libelleCodeCrt = this.getStyleName(style);
                        // Libelle n'est pas un libelle vide
                        Cell cNAme = row.getCell(2);
                        //double heure = (this.valHeureDepart + (double) (cell.getColumnIndex() - (FIRST_ROW_CODE_COULEUR)) / 4) - 0.25;
                        double heure = (this.valHeureDepart + (double) (cell.getColumnIndex() - (FIRST_ROW_CODE_COULEUR)) / 4)+0.5 ;
                        //System.out.println(heure+ "  "+this.valHeureDepart+ "  "+cell.getColumnIndex() + " " + FIRST_ROW_CODE_COULEUR);
                        //if (libelleCodeCrt.compareTo(libelleRef)!=0){
                        // Fin d'un intervalle
                        if (libelleCodeCrt.compareTo("NULL") == 0) {

                            if (libelleRef.compareTo("NULL") != 0) {
                                double heureDebut = (this.valHeureDepart + (double) (offset - (FIRST_ROW_CODE_COULEUR)) / 4) +0.5;
                                    /* System.out.println(cNAme.getStringCellValue() + " "
                                             + libelleRef + " "
                                             + heureDebut + " " +
                                             +heure);*/
                                IVEvent vevent = new IVEvent(heureDebut, heure, MyUtilLib.getNumDayFromString(cNAme.getStringCellValue()), libelleRef);
                                //duree = duree + vevent.getdureeInMinute();

                                day = vevent.jour;
                                listVEvent.add(vevent);

                            }
                            libelleRef = "NULL";
                            offset = -1;

                        } else {
                            duree += 15;
                            // On continue avec le même intervalle
                            if (libelleRef.compareTo(libelleCodeCrt) == 0) {
                            }
                            // debut
                            if (libelleRef.compareTo("NULL") == 0) {
                                offset = cell.getColumnIndex();
                                libelleRef = libelleCodeCrt;
                            }
                            // debut et fin
                            if (libelleCodeCrt.compareTo(libelleRef) != 0) {
                                double heureDebut = (this.valHeureDepart + (double) (offset - (FIRST_ROW_CODE_COULEUR)) / 4) +0.5;
                                    /* System.out.println(" -- >" +cNAme.getStringCellValue() + " "
                                             + libelleRef + " "
                                             + heureDebut + " " +
                                             +heure);*/
                                IVEvent vevent = new IVEvent(heureDebut, heure, MyUtilLib.getNumDayFromString(cNAme.getStringCellValue()), libelleRef);
                                //duree = duree + vevent.getdureeInMinute();
                                day = vevent.jour;
                                listVEvent.add(vevent);

                                offset = cell.getColumnIndex();
                                libelleRef = libelleCodeCrt;


                            }

                        }
                    }
                }

            }

            if (duree > 0) {
                IVDay vd = new IVDay(duree / 60, day);
                this.listVDAY.add(vd);
                day = 0;
                duree = 0;
            }
        }
        return res;
    }

    private String getStyleName(XSSFCellStyle style) {
        Set cles = listeLibelle.keySet();
        String res = "NULL";
        Iterator it = cles.iterator();
        if (style == null) return ("NULL");

       // System.out.println(this.getFillForegroundColorColor(style));

        while (it.hasNext()) {
            String cle = (String) it.next();
            CodeCell valeur = listeLibelle.get(cle);
            // System.out.println(this.getFillBackgroundColorColor(style) + " B "+  valeur.backgroundColor);
            // System.out.println(this.getFillForegroundColorColor(style) + " F "+  valeur.foregroudColor);

            //if ((this.getFillBackgroundColorColor(style) == valeur.backgroundColor) &&
            if (valeur.foregroudColor != null) {
                if (this.getFillForegroundColorColor(style) != null) {
                    if (this.getFillForegroundColorColor(style).compareTo(valeur.foregroudColor) == 0) //&&
                    //
                    // (style == valeur.fillPattern)) {
                    {
                        //System.out.println(" RE " + cle);
                        return cle;
                    }

                }


            }

        }
        // PAs de clef mais couleur de fond ....
        //Creation clef NR si n'existe pas et retourne NR
        boolean NoKeyButColor = (res.compareToIgnoreCase("NULL")==0)&&(this.getFillForegroundColorColor(style)!=null)
                &&(this.getFillForegroundColorColor(style).toString().compareToIgnoreCase("FFFFFFFF")!=0);
        if(NoKeyButColor){


            return "NR";
        }

        return res;


    }

    private String getFillForegroundColorColor(XSSFCellStyle style) {
        if (style.getFillForegroundColorColor() == null)
            return null;
        else return style.getFillForegroundColorColor().getARGBHex();
    }

    private String getFillBackgroundColorColor(XSSFCellStyle style) {
        if (style.getFillBackgroundColorColor() == null)
            return null;
        else return style.getFillBackgroundColorColor().getARGBHex();
    }

    public boolean validation(String Filename) {
        boolean res = false;
        OPCPackage pkg = null;
        try {
            pkg = OPCPackage.open(new File(Filename));
            XSSFWorkbook wb = new XSSFWorkbook(pkg);
            initStructure(wb);

            pkg.close();

        } catch (InvalidFormatException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return res;

    }

    public boolean hasDate(Date dateCrt) {
        if (dateCrt.compareTo(this.DateDebut) == 0) return true;
        if (dateCrt.compareTo(this.DateFin) == 0) return true;
        if (dateCrt.after(this.DateDebut) && dateCrt.before(this.DateFin)) {
            return true;
        }
        return false;
    }

    public double getDurePresence(Date dateCourante) {
        double res = 0.0;
        //calcule du jour de la date
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(dateCourante);


        int day_of_week = calendar.get(Calendar.DAY_OF_WEEK);
        Iterator I = this.listVDAY.iterator();
        while (I.hasNext()) {

            IVDay crt = (IVDay) I.next();
            if (crt.jour == day_of_week) {
                res = crt.durePresence;
            }

        }
//         System.out.println("PLAN " +day_of_week +" "+  MyUtilLib.formatDate(dateCourante) + " " +res );

        return res;
    }

    public void generateSemaine(int i, Date dateDebut, Date dateFin, String[] masque, String path) {
        OPCPackage pkg = null;
        try {

            File tempFile = File.createTempFile("plan",".xlsm");
            File from = new File(path + "planning-Roulement.xlsm");
           // File from = new File("/var/hubo/modele/hubo_hebdo.xlsm");
            Files.copy(from,tempFile);
            pkg = OPCPackage.open(tempFile);
            XSSFWorkbook wb = new XSSFWorkbook(pkg);
            Sheet sheet = wb.getSheet("roulement");




            //change ID
            Cell cell = sheet.getRow(CID.row).getCell(CID.col);
            cell.setCellValue(PlanningSemaine.ID);
            //sheet.createRow(this.CdateSemaine.row);
            //sheet.createRow(this.CdateDebut.row);
            //sheet.createRow(this.CdateFin.row);
            sheet.getRow(this.CdateSemaine.row).createCell(CdateSemaine.col);
            sheet.getRow(this.CdateDebut.row).createCell(CdateDebut.col - 1);
            sheet.getRow(this.CdateDebut.row).createCell(CdateDebut.col);
            sheet.getRow(this.CdateFin.row).createCell(CdateFin.col - 1);
            sheet.getRow(this.CdateFin.row).createCell(CdateFin.col);
            cell = sheet.getRow(this.CdateSemaine.row).createCell(CdateSemaine.col-1);

            //cell = sheet.getRow(this.CdateSemaine.row).getCell(CdateSemaine.col-1);
            cell.setCellValue("Semaine");
            cell = sheet.getRow(this.CdateSemaine.row).getCell(CdateSemaine.col);

            cell.setCellValue(""+i);//+ " du " +MyUtilLib.formatDate(dateDebut) + " au " +MyUtilLib.formatDate(dateFin)  );
            cell = sheet.getRow(this.CdateDebut.row).getCell(CdateDebut.col-1);
            cell.setCellValue("du");
            cell = sheet.getRow(this.CdateDebut.row).getCell(CdateDebut.col);

            cell.setCellValue(MyUtilLib.formatDate(dateDebut)+"");
            cell = sheet.getRow(this.CdateFin.row).getCell(CdateFin.col-1);
            cell.setCellValue("au");
            cell = sheet.getRow(this.CdateFin.row).getCell(CdateFin.col);
            cell.setCellValue(MyUtilLib.formatDate(dateFin));


            //Mise a jour champs avec filtre
            CellStyle style= MyUtilLib.createStyleNoTEPourPlanning(wb);
            for (int j  =0; j < 5 ; j++) {

                if (masque[j]!="") {
                    Row row = sheet.getRow(6+j);
                    for (int k=4; k <= this.colFinHeure-1; k++) {
                        Cell c =row.getCell(k);
                        if (c!=null) {
                            c.setCellValue(" ");
                            c.setCellStyle(style);
                        }

                    }


                }
            }



            //Retire signature

            //List<XSSFShape> shapes  =  sheet.getDrawingPatriarch().getChildren();
          /*  for (int j  =0; j < 5 ; j++) {


                    Row row = sheet.getRow(13 + j);
                    sheet.removeRow(row);


            }*/

            XSSFDrawing drawing = ((XSSFSheet)sheet).createDrawingPatriarch();

            for (XSSFShape shape : drawing.getShapes()) {
                //System.out.println(shape.);
               // shape.setFillColor(255,255,255);
               // shape.setNoFill(true);
                //((org.apache.poi.xssf.usermodel.XSSFSimpleShape)shape).getCTShape().setFPublished(false);
                //System.out.println(((org.apache.poi.xssf.usermodel.XSSFSimpleShape)shape).getCTShape().getTxBody());
                //((org.apache.poi.xssf.usermodel.XSSFSimpleShape)shape).getCTShape()
                //drawing.getCTDrawing().removeAbsoluteAnchor(shape.getAnchor().);
                /*shape.getAnchor().setDx1(0);
                shape.getAnchor().setDx2(0);
                shape.getAnchor().setDy1(0);
                shape.getAnchor().setDy2(0);*/
                //shape.setNoFill(true);
                //((ClientAnchor)shape.getAnchor()).setRow1(0);


            }



            // FileOutputStream fileOut = new FileOutputStream(s);
                // wb.write(fileOut);

                //fileOut.close();

                FileOutputStream fileOut = new FileOutputStream(path+"planning-semaine-"+i+".xlsm");
                wb.write(fileOut);
                fileOut.close();
                pkg.close();




            } catch (IOException e) {
                e.printStackTrace();


        } catch (InvalidFormatException e) {
            e.printStackTrace();

        }
    }
}
