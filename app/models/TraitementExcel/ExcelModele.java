package models.TraitementExcel;

import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import play.libs.Json;
import Util.MyUtilLib;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.eventusermodel.XSSFReader;
import org.apache.poi.xssf.model.SharedStringsTable;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFRichTextString;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.xml.sax.*;
import org.xml.sax.helpers.DefaultHandler;
import org.xml.sax.helpers.XMLReaderFactory;
import play.libs.Json;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by richard on 21/04/15.
 */
class cellCase {
    int row;
    int col;

    public cellCase(int row, int col) {
        this.row   =row ;
        this.col=col;
    }
}
public abstract class ExcelModele {

    public static final String LBL_TE = "TE";
    ObjectNode resultat;
    ArrayNode listeMessage;
    public static double ID  =0;
    String templateName="";
    private String pathModele="/var/hubo/modele/";
    public String fileTemplate="";

    public void addMessageInfo(String mess){
        ObjectNode info = Json.newObject();
        info.put("msg",mess);
        listeMessage.add(info);


    }

    public ObjectNode getResultat() {

        return resultat;
    }

    public void setResultat(ObjectNode resultat) {
        this.resultat = resultat;
    }

    public ArrayNode getListeMessage() {
        return listeMessage;
    }

    public void setListeMessage(ArrayNode listeMessage) {
        this.listeMessage = listeMessage;
    }

    public void addErrorInfo(String mess){
        ObjectNode info = Json.newObject();
        info.put("error",mess);
        listeMessage.add(info);


    }
    public ExcelModele() {
                resultat = Json.newObject();
        ArrayNode arr = resultat.arrayNode();



        resultat.put("TraitementExcel", arr);
        listeMessage =  arr;
    }

    public boolean validation(String s) {
        return false;
    }


    @Deprecated
    public void initFromTemplate() {

    }
    public void initFromTemplate(String s){

    }

    public void setPathModele(String pathModele) {
        this.pathModele = pathModele;
    }

    public String getPathModele() {
        return pathModele;
    }

    public void saveToFile(String s, String template){

    }

    public void saveToFileFromTemplate(String s) {


    }

    public void initFromFile(String file) {

    }

    public void saveToFile(String s) {

    }
}
