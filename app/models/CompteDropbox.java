package models;

//import com.dropbox.core.*;

import com.avaje.ebean.Ebean;
import models.Dropbox.DropInterface;
import models.Dropbox.DropTicket;
import models.osTicket.Ticket;
import play.Play;
import play.db.ebean.Model;

import javax.persistence.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by richard on 31/05/15.
 */
@Entity
@Table(name="CompteDropbox", catalog="", schema="")
public class CompteDropbox extends Model {


        @Version
        public Timestamp lastUpdate;
   //private DbxClient dropClient;

    public Timestamp getLastUpdate() {
            return lastUpdate;
        }

    /*public DbxClient getDropClient() {
        return dropClient;
    }

    public void setDropClient(DbxClient dropClient) {
        this.dropClient = dropClient;
    }*/

    public void setId(Long id) {
        this.id = id;
    }

    public void setLastUpdate(Timestamp lastUpdate) {
            this.lastUpdate = lastUpdate;
        }

        /**
         * Id
         */
        @Id
        @Column(name = "compteDropbox_index")
        public Long id;
        public long getId(){

            return this.id;
        }
        /**
         *Nom
         *
         */
        public String nom="";
        public String getNom() {
            return nom;
        }
        public void setNom(String nom) {
            this.nom = nom;
        }


        @OneToMany(mappedBy="compteDropbox", cascade=CascadeType.ALL)
         public List<Entreprise> entreprises;


        public String key;
        public String ssecret;
        public String dropUsername;
        public String dropPass;
        public String accessToken;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getSsecret() {
        return ssecret;
    }

    public void setSsecret(String ssecret) {
        this.ssecret = ssecret;
    }

    public String getDropUsername() {
        return dropUsername;
    }

    public void setDropUsername(String dropUsername) {
        this.dropUsername = dropUsername;
    }

    public String getDropPass() {
        return dropPass;
    }

    public void setDropPass(String dropPass) {
        this.dropPass = dropPass;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }


    public static Finder<Long,CompteDropbox> find = new Finder<Long,CompteDropbox>(
            Long.class, CompteDropbox.class
    );


    public ArrayList<AbsTicket> getTicketOpen(){
        ArrayList<AbsTicket> resultat = new ArrayList<AbsTicket>();
        // Home
        String HomePathTicket= Play.application().configuration().getString("hubo.dropbox.HomePathTicket");
        // A get liste entreprise

       //  //List<Entreprise> entreprises = Entreprise.find.all();
        System.out.println("-->"+entreprises.size());
       for (Entreprise ent:entreprises){

           DropInterface drop = new DropInterface();
           drop.initDrop(this);

           //Parcours admnistrateurs
           for (Employe emp:ent.employes){
               String PathAdmin = HomePathTicket.replace("<entreprise_nom>",ent.getNom()).
                                  replace("<employe_mail>", emp.getAdresseMail());
              // System.out.println(PathAdmin);

               //System.out.println(drop.getlisteFichier(PathAdmin).size());

               // Ticket en création  ???
               String Pathcreate = PathAdmin+"/HUBO-transfert";
               ArrayList <String>ListeFiche= drop.getlisteFichier(Pathcreate);
               System.out.println(Pathcreate + "-->" + ListeFiche + drop.getTempFichier("/Apps/Entreprises/IMEPG/administrateurs/richard.urunuela@gmail.com/Hubo-Planning/HUBO-tranfert/hubo_roulement.xlsm"));
               for (String filename:ListeFiche){
                   System.out.println(filename);
                   DropTicket ticket = new DropTicket();
                   ticket.email=emp.adresseMail;
                   ticket.sujet="hubo_transfert";
                   ticket.nombreFichier=1;
                   ticket.Fichiers=new long[1];
                   ticket.Fichiers[0]=-1;
                   ticket.files = new String[1];
                   ticket.files[0] =Pathcreate+"/"+filename;
                   ticket.virtual= true;
                   ticket.dropInterface = drop;
                   ticket.isDropBox=true;


                   resultat.add(ticket);
               }



           }







       }



        return resultat;

    }




    /**
     * Init communication avec dropbox
     *
      */
  /*  public void initDrop() {
        DbxAppInfo dbxAppInfo = new DbxAppInfo(this.getKey(), this.getSsecret());
        DbxRequestConfig dbxRequestConfig = new DbxRequestConfig(
                "JavaDropboxTutorial/1.0", Locale.getDefault().toString());
        DbxWebAuthNoRedirect dbxWebAuthNoRedirect = new DbxWebAuthNoRedirect(
                dbxRequestConfig, dbxAppInfo);
        String authorizeUrl = dbxWebAuthNoRedirect.start();
        System.out.println("1. Authorize: Go to URL and click Allow : "
                + authorizeUrl);
        try {
        System.out
                .println("2. Auth Code: Copy authorization code and input here ");
        String dropboxAuthCode = new BufferedReader(new InputStreamReader(
                System.in)).readLine().trim();
        DbxAuthFinish authFinish = null;

            authFinish = dbxWebAuthNoRedirect.finish(dropboxAuthCode);
            String authAccessToken = authFinish.accessToken;
            DbxClient dbxClient = new DbxClient(dbxRequestConfig, authAccessToken);
            System.out.println("Dropbox Account Name: "
                    + dbxClient.getAccountInfo().displayName);

            this.dropClient=dbxClient;

        } catch (DbxException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }*/
    //
    // Pas necessaire ici pas compatible neccesite un wrapper

}
