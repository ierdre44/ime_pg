package models;

import com.fasterxml.jackson.databind.node.ObjectNode;
import models.TraitementRequete.TraitementRequete;
import models.osTicket.Ticket;
import play.db.ebean.Model;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by richard on 04/05/15.
 */

public class Requete extends Model {
    public static final String CREATION = "creation";
    public static final String ABSENCE ="absence" ;
    public static final String PLANNING = "planning";
    public static final CharSequence HUBO_TRANSFERT = "hubo_transfert";

    private boolean aFichier=false;
    /*
       Time stamp

        */

    private boolean resultat;




    public boolean succes= true;


    public String commande; // Titre de la commande
    public String pattern; // Commande dans le corps

    public Long numOsTicket;
    public Employe fromEmp;

    // On crée une requete à partir d'un ticket (OSticket )
    public Requete(AbsTicket ticket) {
            fromEmp= Employe.findByMail(ticket.email);
            this.commande=ticket.sujet;
            this.pattern = ticket.corps;
            this.numOsTicket = ticket.number;
        if (ticket.nombreFichier> 0 ) {
            this.aFichier = true;
        }



    }
/*
Traitement de la requete

 */
    public ObjectNode execute(AbsTicket ticket){


 //       // On regarde valeur de la commande

//        //Commande de creation
//        // IMEPG
//        if (this.fromEmp.getTraitementRequete().compareToIgnoreCase(Requete.IMEPG) ==0) {
//            if (this.commande.toLowerCase().contains(Requete.CREATION)) {
//                //System.out.println(" Ticket touvé ");
//                // Identification des fichiers associé si fichiers associé
//
//
//            }
//        }
        TraitementRequete traitant = TraitementRequete.factory(this.fromEmp.traitementRequete);
        ObjectNode res =traitant.execute(ticket,this);

        return res;

    }



}
