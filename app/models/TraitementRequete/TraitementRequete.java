package models.TraitementRequete;

import Util.MyUtilLib;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import models.AbsTicket;
import models.Requete;
import models.osTicket.Ticket;
import play.libs.Json;

/**
 * Created by richard on 04/05/15.
 */
public class TraitementRequete {
    Requete requete;
    AbsTicket ticket;
    ObjectNode resultat;
    ArrayNode listeMessage;
    public  void addMessageInfo(JsonNode json) {
        listeMessage.add(json);
    }

    public void addMessageInfo(String mess){
        ObjectNode info = Json.newObject();
        info.put("msg",mess);
        listeMessage.add(info);


    }
    public void addErrorInfo(String mess){
        ObjectNode info = Json.newObject();
        info.put("error",mess);
        listeMessage.add(info);


    }

    public static TraitementRequete factory(String interpreteur) {

        TraitementRequete resultat = null;
        Class c = null;
        try {
            c = Class.forName("models.TraitementRequete." + interpreteur);
            resultat = (TraitementRequete ) c.newInstance();
            resultat.resultat = Json.newObject();
            ArrayNode arr = resultat.resultat.arrayNode();



            resultat.resultat.put("TraitementRequete", arr);
            resultat.listeMessage =  arr;



          //  ArrayNode arr = resultat.resultat.arrayNode();
           //    resultat.resultat.put("listeMessage", arr);
            //resultat.listeMessage =  arr;

            //resultat.owner = owner;

        } catch (Exception e) {
            e.printStackTrace();

        }
        if (resultat == null ) {
            //Exception F= new Exception("Class non connu "+ className);
            //throw  F;
            System.err.println(" Classe de ticket gestionnaire non connu");
        }
        return resultat;


    }

    public  ObjectNode execute(AbsTicket ticket, Requete requete) {
        this.ticket = ticket;
        this.requete = requete;
           if (ticket.nombreFichier>0) {

                 MyUtilLib.getSheetConnus(ticket);


            }
        return this.resultat;


    }
}
