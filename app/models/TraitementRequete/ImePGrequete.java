package models.TraitementRequete;

import Util.MonMail;
import Util.MyUtilLib;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.common.io.Files;
import models.*;
import models.Dropbox.DropInterface;
import models.TraitementExcel.*;
import models.osTicket.Ticket;
import play.Play;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.Date;

/**
 * Created by richard on 04/05/15.
 */
public class ImePGrequete extends TraitementRequete {

    @Override
    public ObjectNode execute(AbsTicket ticket, Requete requete) {

        super.execute(ticket, requete);
        // Alimenttion boolean de test
        boolean isCreateDropBoxEmp_75000=this.requete.commande.toLowerCase().contains(Requete.HUBO_TRANSFERT) &&
                                        ticket.listesheet[1] == PlanningRoulement.ID&& ticket.isDropBox;
        boolean isPoseCongeDropBoxEmp=this.requete.commande.toLowerCase().contains(Requete.HUBO_TRANSFERT) &&
                ticket.listesheet[1] == TrameBase.ID && ticket.isDropBox;
        boolean isPoseModificationPlanningBoxEmp=this.requete.commande.toLowerCase().contains(Requete.HUBO_TRANSFERT) &&
                ticket.listesheet[1] == PlanningSemaine.ID && ticket.isDropBox;
        boolean isSetUserProfil=this.requete.commande.toLowerCase().contains(Requete.HUBO_TRANSFERT) &&
                ticket.listesheet[1] == AdminSetUser.ID && ticket.isDropBox;

        // Le demandeur est un manager ?
        if (!requete.fromEmp.getIsManager())  return this.resultat;
       /* if (this.requete.commande.toLowerCase().contains(Requete.PLANNING)) {
            for (int i=0 ; i <ticket.files.length ; i++) {
                if (ticket.listesheet[(2 * i) + 1] == PlanningSemaine.ID) {
                    PlanningSemaine plan= new PlanningSemaine();
                    //System.out.println(ticket.files[i]);
                    plan.validation(ticket.files[i]);
                    addMessageInfo(PlanningSemaine.ID+"");
                    Employe emp= Employe.findByMail(plan.getAdresseMail());
                    if (emp!=null) {
                        String varHome = Play.application().configuration().getString("hubo.HomeUsers");
                        String PathHomeUser = varHome+emp.getId()+"/";


                        File from = new File(ticket.files[i]);
                        String PathTo=PathHomeUser+"mod-"+(int)plan.semaine+"-planning-Semaine.xlsx";
                        File fto= new File(PathTo);
                        try {
                            Files.copy(from,fto);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        TrameBase trame = new TrameBase();

                        ((ExcelModele) trame).initFromFile(PathHomeUser+"trame-Annuel.xlsx");
                        trame.mergePlanning(plan);
                        trame.saveToFile(PathHomeUser+"trame-Annuel.xlsx");
                        //



                        System.out.println(" PLANNNN"+emp + " " +plan.getSemaine() + "  "+ plan.getDateDebut() + "  "+plan.getDateFin());
                      //  MonMail.sendMailText(ticket.email," Modification planning","demande de modification du planning  l'employe "
                         //       + plan.getAdresseMail() + " effectuée pour la semaine "+plan.semaine);

                        //plan.saveToFile("/tmp/req.xlsx");
                    }

                }
            }

        }
        else if (this.requete.commande.toLowerCase().contains(Requete.ABSENCE)) {

            for (int i=0 ; i <ticket.files.length ; i++){
                if (ticket.listesheet[(2*i)+1]== TrameBase.ID){
                    //System.out.println(" Trame base OK");
                    TrameBase trame = new TrameBase();

                    ((ExcelModele) trame).initFromFile(ticket.files[i]);
                    Employe emp= Employe.findByMail(trame.getAdresseMail());
                    if (emp !=null) {
                        String varHome = Play.application().configuration().getString("hubo.HomeUsers");
                        String PathHomeUser = varHome+emp.getId()+"/";

                        ((ExcelModele) trame).saveToFile(PathHomeUser+"trame-Annuel.xlsx");

                        PlanningRoulement plann= new PlanningRoulement();
                        String pathToFile= PathHomeUser+"planning-Roulement.xlsx";

                        plann.validation(pathToFile);
                        trame.createPlanSemaineAnne(plann,PathHomeUser);


                       // MonMail.sendMailText(ticket.email," DEMANDE ABSENCE","demande de modification des absences pour  l'employe " + plann.getAdresseMail() + " effectuée");



                    }







                }
            }

        }
        else*/ if (isPoseModificationPlanningBoxEmp){
            addMessageInfo("Demande Modification planning ");
            for (int i = 0; i < ticket.files.length; i++) {
                if (ticket.listesheet[(2 * i) + 1] == PlanningSemaine.ID) {
                    PlanningSemaine plan= new PlanningSemaine();
                    System.out.println(ticket.files[i]);
                    plan.validation(ticket.files[i]);
                    addMessageInfo(PlanningSemaine.ID+"");
                    Employe emp= Employe.findByMail(plan.getAdresseMail());
                    if (emp!=null) {
                        //init dropbox
                        Entreprise ent = emp.getEntreprise();
                        CompteDropbox compte = ent.getCompteDropbox();
                        DropInterface drop = new DropInterface();
                        drop.initDrop(compte);
                        String HomeDropPathEmploye= Play.application().configuration().getString("hubo.dropbox.HomePathEmploye").replace("<entreprise_nom>",ent.getNom()).
                                replace("<nom-prenom-id>", emp.getNom().trim()+"-"+emp.getPrenom().trim()+"-"+emp.getId());
                        //end init drop

                        String varHome = Play.application().configuration().getString("hubo.HomeUsers");
                        String PathHomeUser = varHome+emp.getId()+"/";


                        File from = new File(ticket.files[i]);
                        String PathTo=PathHomeUser+"mod-"+(int)plan.semaine+"-planning-Semaine.xlsm";
                        File fto= new File(PathTo);
                        try {
                            Files.copy(from,fto);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        TrameBase trame = new TrameBase();

                        ((ExcelModele) trame).initFromFile(PathHomeUser+"trame-Annuel.xlsx");
                        trame.mergePlanning(plan);
                        trame.saveToFile(PathHomeUser+"trame-Annuel.xlsx");
                        //



                      //
                        //Archive ancienne semaine en local et dropbox
                        if (drop.fileExist(HomeDropPathEmploye + "/planning-semaine-" + ((int) plan.getSemaine()) + ".xlsm")) {
                            drop.moveFichier(HomeDropPathEmploye + "/planning-semaine-" + ((int) plan.getSemaine()) + ".xlsm",
                                    HomeDropPathEmploye + "/ARCHIVE/planning-semaine-" + MyUtilLib.formatDate(new Date()).replace("/", "_").trim() + "_" + MyUtilLib.getRandom(5).trim() + "_" + plan.getSemaine() + ".xlsm");
                        }
                        //pousee fichier nouvelle semaine
                        drop.putFichier(PathTo,HomeDropPathEmploye +"/"+"mod-"+(int)plan.semaine+"-planning-Semaine.xlsm");
                       //sauvegarde ancienne trame annuel
                        drop.moveFichier(HomeDropPathEmploye + "/trame-Annuel.xlsx",HomeDropPathEmploye + "/ARCHIVE/"+MyUtilLib.formatDate(new Date()).replace("/", "_").trim() + "_" + MyUtilLib.getRandom(5).trim()
                                + "trame-Annuel.xlsx");
                        //pousse nouvelle trame base
                        drop.putFichier(PathHomeUser+"trame-Annuel.xlsx",HomeDropPathEmploye + "/trame-Annuel.xlsx");

                        //supprime fichier du repertoires source
                        drop.moveFichier(ticket.filePathNameOnDropBox,ticket.PathNameOnDropBox+"/ARCHIVES/"+MyUtilLib.formatDate(new Date()).replace("/","_").trim()+"_"+MyUtilLib.getRandom(5).trim()+"_"+ticket.fileNameOnDropBox);

                    }



                }
            }
        }
       // Ici hub_transfert et pose conge
        //  a partir du fichier de trame de base
        else if (isPoseCongeDropBoxEmp) {
            System.err.println(" Demande d'absence");
            addMessageInfo("Demande d'ABSENCE Fichier base :");
            for (int i = 0; i < ticket.files.length; i++) {

                if (ticket.listesheet[(2 * i) + 1] == TrameBase.ID) {
                    TrameBase trame = new TrameBase();

                    ((ExcelModele) trame).initFromFile(ticket.files[i]);
                    System.err.println(ticket.files[i]);
                    System.out.println(trame.getAdresseMail());
                    Employe emp= Employe.findByMail(trame.getAdresseMail());
                    addMessageInfo(emp.getAdresseMail());
                    if (emp !=null) {
                        //init dropbox
                        Entreprise ent = emp.getEntreprise();
                        CompteDropbox compte = ent.getCompteDropbox();
                        //System.out.println(compte + "  " +ent.getNom());
                        DropInterface drop = new DropInterface();
                        drop.initDrop(compte);
                        String entreprise_modele= Play.application().configuration().getString("hubo.dropbox.HomePathEntreprise");
                        String PathEntreprise = entreprise_modele.replace("<entreprise_nom>", ent.getNom());
                        String template = drop.getTempFichier(PathEntreprise + "/modeles" + "/trameBase-model.xlsx");
                        // on recupere le fichier



                        //


                        String varHome = Play.application().configuration().getString("hubo.HomeUsers");
                        String PathHomeUser = varHome+emp.getId()+"/";



                        PlanningRoulement plan= new PlanningRoulement();
                        String pathToFile= PathHomeUser+"planning-Roulement.xlsm";

                        plan.validation(pathToFile);
                        trame.mergePlanning(plan);
                        trame.createPlanSemaineAnne(plan, PathHomeUser);


                        ((ExcelModele) trame).saveToFile(PathHomeUser + "trame-Annuel.xlsx", ticket.files[i]);
                        //((ExcelModele) trame).saveToFileFromTemplate(PathHomeUser + "trame-Annuel.xlsx");
                        System.err.println("save " + PathHomeUser + "trame-Annuel.xlsx ");






                        //Mise à jour dropbox
                        //
                        // Sauvegarde ancienne trame

                        String HomePathEmploye= Play.application().configuration().getString("hubo.dropbox.HomePathEmploye");

                        String PathAdmin = HomePathEmploye.replace("<entreprise_nom>",ent.getNom()).
                                replace("<nom-prenom-id>", emp.getNom().trim()+"-"+emp.getPrenom().trim()+"-"+emp.getId());
                        String date = (MyUtilLib.formatDate(new Date())).replace("/","_").trim();
                        drop.moveFichier(PathAdmin+"/trame-Annuel.xlsx",PathAdmin+"/ARCHIVE/"+date+"_"+MyUtilLib.getRandom(4)+"-trame-Annuel.xlsx");
                        //copie nouveaux_fichiers
                        File folder = new File(PathHomeUser);
                        File[] listOfFiles = folder.listFiles();
                        for (File file : listOfFiles) {
                            if (file.isFile()) {
                                //System.out.println(file.getName());

                                drop.putFichier(PathHomeUser+"/"+file.getName(),
                                        PathAdmin+"/"+file.getName());
                                //System.out.println("[copie ] "+file.getName());


                            }
                        }
                       /* for (File file : listOfFiles) {
                            if (file.isFile()) {
                                //System.out.println(file.getName());
                                while (drop.fileExist(PathAdmin+"/"+file.getName())) {
                                    drop.deleteFichier(PathAdmin + "/" + file.getName());
                                }
                                while (!drop.fileExist(PathAdmin+"/"+file.getName())) {
                                    drop.putFichier(PathHomeUser + "/" + file.getName(),
                                            PathAdmin + "/" + file.getName());
                                }



                            }
                        }*/
                        //supprime le fichier en référence
                        //System.out.println(ticket.files[0]);
                        Date d=new Date();
                        //drop.moveFichier
                        drop.moveFichier(ticket.filePathNameOnDropBox,ticket.PathNameOnDropBox+"/ARCHIVES/"+MyUtilLib.formatDate(d).replace("/","_").trim()+"_"+MyUtilLib.getRandom(5).trim()+"_"+ticket.fileNameOnDropBox);









                    }

                }
            }
        }
        // Changement test sur HUBO_TRNASFERT et type (id) du premier onglet du fichier Excel
        //else if (this.requete.commande.toLowerCase().contains(Requete.CREATION))
        else if (isCreateDropBoxEmp_75000) {
            System.out.println("-->" + ticket.files.length);
                // Identification des fichiers associé si fichiers associé
                for (int i=0 ; i <ticket.files.length ; i++){
                   if (ticket.listesheet[(2*i)+1]== PlanningRoulement.ID){
                       PlanningRoulement plan= new PlanningRoulement();
                       //System.out.println(ticket.files[i]);
                       plan.validation(ticket.files[i]);
                       addMessageInfo(PlanningRoulement.ID+"");
                       Employe emp= Employe.findByMail(plan.getAdresseMail());
                       if (emp==null) {

                           addMessageInfo(" Demande de création d'un employe");


                           addMessageInfo("demande de création d'un employe");
                           emp = new Employe();
                           emp.setAdresseMail(plan.getAdresseMail());
                           emp.setFonction(plan.getFonction());
                           emp.setIsManager(false);
                           emp.setIssPortalUser(false);
                           emp.setLevel(1);
                           emp.setNomPrenom(plan.getNomPrenom());
                           emp.setNom(plan.getNom());
                           emp.setPrenom(plan.getPrenom());
                           emp.entreprise=requete.fromEmp.entreprise;

                           emp.save();
                           addMessageInfo(emp.getJson());
                           //entrteprise

                       }
                       else {
                           addErrorInfo(" Employe existe déja "+plan.getAdresseMail());
                           //supprime le fichier en référence
                           //System.out.println(ticket.files[0]);
                           Date d=new Date();
                           //drop.moveFichier
                           Entreprise ent = requete.fromEmp.getEntreprise();
                           DropInterface drop = new DropInterface();
                           drop.initDrop(ent.getCompteDropbox());
                           drop.moveFichier(ticket.filePathNameOnDropBox, ticket.PathNameOnDropBox + "/ARCHIVES/" + MyUtilLib.formatDate(d).replace("/", "_").trim() + "_" + MyUtilLib.getRandom(5).trim() + "_ERREUR_EXISTE_EE_" + ticket.fileNameOnDropBox);

                           //TO DO
                           return this.resultat;

                       }

                           



                       // Sauvegarde du document de emp
                       // Avec creation de son environnement de travail si il
                       // n'existe pas cad le nom repertoire  <ID>/document/
                       // Et enregistrement dans la base de donnée
                       // Retire le return précédent pour test envue de raffiner l'execution
                       String varHome = Play.application().configuration().getString("hubo.HomeUsers");
                       String PathHomeUser = varHome +emp.getId() + "/";
                       File fb = new File(PathHomeUser);
                       if (!fb.exists()) {
                           fb.mkdir();
                       }
                       String pathToFile = PathHomeUser + "planning-Roulement.xlsm";
                       fb = new File(pathToFile);
                       if (fb.exists()) {
                           // erreur car le fichier ne peut pas exister on vient de creer l'user
                           addErrorInfo("Employe n'existe pas mais son repertoires si !!!");

                       }
                       File fileFrom = new File(ticket.files[i]);
                       try {
                           Files.copy(fileFrom, fb);
                       } catch (IOException e) {
                           e.printStackTrace();
                       }
                       PlanningRoulement plann = new PlanningRoulement();
                       plann.validation(pathToFile);

                       String pathTrameTemplate="/var/hubo/modele/PlanningAnnuel.xlsx";
                       // Ticket dropbox ??
                       // oui mise à jour modéle par rapport à hubo ...
                       if (ticket.isDropBox){
                           Entreprise ent = requete.fromEmp.getEntreprise();
                           DropInterface drop = new DropInterface();
                           drop.initDrop(ent.getCompteDropbox());
                           String entrePriseTest = Play.application().configuration().getString("hubo.dropboxApp.HomePathEntreprise").replace("<entreprise_nom>", ent.getNom());

                           pathTrameTemplate=drop.getTempFichier(entrePriseTest+"/modeles/trameBase-model.xlsx");
                          // System.err.println(entrePriseTest+"/modeles/trameBase-model.xlsx"+"\n"+pathTrameTemplate);
                           addMessageInfo(" ajout trame defaut dropbox " + pathTrameTemplate);


                       }





                           //Construction trame de base employe
                           TrameBase trame = new TrameBase();
                           //((ExcelModele) trame).setPathModele("./test/test-A/");
                           //((ExcelModele) trame).initFromTemplate();
                            ((ExcelModele) trame).initFromTemplate(pathTrameTemplate);

                           trame.mergePlanning(plan);
                           pathToFile = PathHomeUser + "trame-Annuel.xlsx";
                           ((ExcelModele) trame).saveToFileFromTemplate(pathToFile);


                           // Generation des 52 fiches de roulement de l'année pour l'employé
                          trame.createPlanSemaineAnne(plan, PathHomeUser);

                       //synchro repertoires locale et distant
                       if (ticket.isDropBox){
                           Entreprise ent = emp.getEntreprise();
                           CompteDropbox compte = ent.getCompteDropbox();
                           //System.out.println(compte + "  " +ent.getNom());
                           DropInterface drop = new DropInterface();
                           drop.initDrop(compte);
                           String HomePathEmploye= Play.application().configuration().getString("hubo.dropbox.HomePathEmploye");

                           String PathAdmin = HomePathEmploye.replace("<entreprise_nom>",ent.getNom()).
                                   replace("<nom-prenom-id>", emp.getNom().trim()+"-"+emp.getPrenom().trim()+"-"+emp.getId());

                           //PathHomeUser chemin vers fichier user
                           File folder = new File(PathHomeUser);
                           File[] listOfFiles = folder.listFiles();

                           //TEST COMPARAISON
                         /*  for (File file : listOfFiles) {
                               if (file.isFile()) {
                                   //System.out.println(file.getName());
                                   while (drop.fileExist(PathAdmin+"/"+file.getName())) {
                                       drop.deleteFichier(PathAdmin + "/" + file.getName());
                                   }
                                   while (!drop.fileExist(PathAdmin+"/"+file.getName())) {
                                       drop.putFichier(PathHomeUser + "/" + file.getName(),
                                               PathAdmin + "/" + file.getName());
                                   }



                               }
                           }*/


                           drop.syncDiretory(PathHomeUser,PathAdmin);


                          //TEST
                           /*String[] listeF = new String[listOfFiles.length];
                           String[] lname = new String[listOfFiles.length];

                           int lf=0;
                           for (File file : listOfFiles) {
                               if (file.isFile()) {
                                   listeF[i] = PathHomeUser + "/" + file.getName();
                                   lname[i] = file.getName();


                               }
                               listeF[i] =null;lname[i]=null;
                               lf++;

                           }
                           drop.putFichier(listeF,lname,PathAdmin);
                           */
                           // TEST //////
                           //supprime le fichier en référence
                           //System.out.println(ticket.files[0]);
                           Date d=new Date();
                           //drop.moveFichier
                           drop.moveFichier(ticket.filePathNameOnDropBox,ticket.PathNameOnDropBox+"/ARCHIVES/"+MyUtilLib.formatDate(d).replace("/","_").trim()+"_"+MyUtilLib.getRandom(5).trim()+"_"+ticket.fileNameOnDropBox);

                       }



                       //MonMail.sendMailText(ticket.email," CREATION EMP","creation de l'employe " + plan.getAdresseMail() + " effectuée" + "\n" );



                   }
                   // End planning roulement


                }

            }
        /*
        Gestion des compte utilisateurs
         */
        else if(isSetUserProfil){
            AdminSetUser adminSet = new AdminSetUser();
            for (int i=0 ; i <ticket.files.length ; i++) {
                if (ticket.listesheet[(2*i)+1]== AdminSetUser.ID) {
                    adminSet.updateProfil(ticket.files[i]);
                }
            }

        }
        else{

                MonMail.sendMailText(ticket.email,"COMMANDE INCONNUS",ticket.sujet+" \n" +ticket.corps);

        }

        return this.resultat;
    }


}