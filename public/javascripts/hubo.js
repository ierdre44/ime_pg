 
// Traitement formulaire inscription landing page
$(document).ready(OnReady);
function OnReady(){
  console.log(" OK3 ");
    $("form").submit(OnSubmit); // Abonne un callback à l'évènement "submit" du formulaire

}
function OnSubmit(){
 
var markers  = {"adresseMail":$("#address").val(),
        
        "password":""

      };
      console.log(" DEM ");
      if (!isEmail($("#address").val())) {
      $("#result").html("Entrez une  adresse mail " + $("#address").val());
      return false;
      }
$.ajax({
    type: "POST",
    url: "/enregistrement",
    // The key needs to match your method's input parameter (case-sensitive).
    data: JSON.stringify({ Markers: markers }),
    contentType: "application/json; charset=utf-8",
    dataType: "json",
    success:  OnSuccess,
    //function(data){alert(data);},
    failure: function(errMsg) {
        alert(" ERREUR " + errMsg);
        
    }
});
    return false; // Annule l'envoi classique du formulaire
}
function OnSuccess(result){
  console.log("---> " +(JSON.stringify(result)));
  if (JSON.stringify(result["create"])=="true"){
     $("#result").html(" <div class=\"alert alert-success\"><button type=\"button\" class=\"close\">×</button>Merci de votre intérêt ! Nous avons bien pris note de votre adresse.</div> ");
     
//ajout DV
window.setTimeout(function () {
     $(".alert-success").fadeTo(500, 0).slideUp(500, function () {
         $(this).remove();
     });
 }, 12000);

 //Adding a click event to the 'x' button to close immediately
 $('.alert .close').on("click", function (e) {
     $(this).parent().fadeTo(500, 0).slideUp(500);
 });

  }


  else  if (JSON.stringify(result["create"])=="false"){
        $("#result").html(" <div class=\"alert alert-info\"><button type=\"button\" class=\"close\">×</button>A bien y regarder, nous  avons déjà pris note de votre adresse. Nous vous contacterons prochainement.</div> ");


        //ajout DV
window.setTimeout(function () {
     $(".alert-info").fadeTo(500, 0).slideUp(500, function () {
         $(this).remove();
     });
 }, 12000);

 //Adding a click event to the 'x' button to close immediately
 $('.alert .close').on("click", function (e) {
     $(this).parent().fadeTo(500, 0).slideUp(500);
 });
  }
   else {
  $("#result").html(" <div class=\"alert alert-danger\"><button type=\"button\" class=\"close\">×</button>Oups, nous n'avons pas réussi à enregistrer votre adresse. Le plus simple : nous envoyer un mail : <a href=\"mailto:contact@@hubo-soft.fr\"</a></div> ");

 //ajout DV
window.setTimeout(function () {
     $(".alert-danger").fadeTo(500, 0).slideUp(500, function () {
         $(this).remove();
     });
 }, 24000);

 //Adding a click event to the 'x' button to close immediately
 $('.alert .close').on("click", function (e) {
     $(this).parent().fadeTo(500, 0).slideUp(500);
 });

  }

      //$("#result").html(JSON.stringify(result));
  
}


      function isEmail(myVar){
      // La 1ère étape consiste à définir l'expression régulière d'une adresse email
      var pattern = new RegExp(/^[+a-zA-Z0-9._-]+@@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i);
      return pattern.test(myVar);
      }
  