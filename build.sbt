name := """imepg"""

version := "1.0"

lazy val root = (project in file(".")).enablePlugins(PlayJava)
//resolvers += "bintray" at "http://dl.bintray.com/shinsuke-abe/maven"



scalaVersion := "2.11.1"

libraryDependencies ++= Seq(
  javaJdbc,
  javaEbean,
  cache,
  javaWs,
 "postgresql" % "postgresql" % "9.1-901-1.jdbc4",
  "mysql" % "mysql-connector-java" % "5.1.18"
    ,"org.apache.poi" % "poi" % "3.9",
  "org.apache.poi" % "poi-ooxml" % "3.9",
  "org.apache.commons" % "commons-email" % "1.3.3",
// "com.dropbox.core" % "dropbox-core-sdk" % "1.7"
//"com.github.Shinsuke-Abe" %% "dropbox4s" % "0.2.1"
"org.json4s" %% "json4s-native" % "3.2.10",
  "com.dropbox.core" % "dropbox-core-sdk" % "[1.7,1.8)",
  "commons-codec" % "commons-codec" % "1.9",
"org.seleniumhq.selenium" % "selenium-java" % "2.41.0" % "test",
"org.fluentlenium" % "fluentlenium-core" % "0.9.2" % "test",
"org.fluentlenium" % "fluentlenium-festassert" % "0.9.2" % "test"
)
