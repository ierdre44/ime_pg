# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table CompteDropbox (
  compteDropbox_index       bigint not null,
  nom                       varchar(255),
  key                       varchar(255),
  ssecret                   varchar(255),
  drop_username             varchar(255),
  drop_pass                 varchar(255),
  access_token              varchar(255),
  last_update               timestamp not null,
  constraint pk_CompteDropbox primary key (compteDropbox_index))
;

create table employe (
  id                        bigint not null,
  entrepriseId              bigint,
  level                     integer,
  is_portal_user            boolean,
  is_manager                boolean,
  nom                       varchar(255),
  fonction                  varchar(255),
  prenom                    varchar(255),
  nom_prenom                varchar(255),
  password                  varchar(255),
  adresse_mail              varchar(255),
  traitement_requete        varchar(255),
  last_update               timestamp not null,
  constraint uq_employe_adresse_mail unique (adresse_mail),
  constraint pk_employe primary key (id))
;

create table Entreprise (
  id                        bigint not null,
  nom                       varchar(255),
  compteDropboxId           bigint,
  last_update               timestamp not null,
  constraint pk_Entreprise primary key (id))
;

create sequence CompteDropbox_seq;

create sequence employe_seq;

create sequence Entreprise_seq;

alter table employe add constraint fk_employe_entreprise_1 foreign key (entrepriseId) references Entreprise (id) on delete restrict on update restrict;
create index ix_employe_entreprise_1 on employe (entrepriseId);
alter table Entreprise add constraint fk_Entreprise_compteDropbox_2 foreign key (compteDropboxId) references CompteDropbox (compteDropbox_index) on delete restrict on update restrict;
create index ix_Entreprise_compteDropbox_2 on Entreprise (compteDropboxId);



# --- !Downs

SET REFERENTIAL_INTEGRITY FALSE;

drop table if exists CompteDropbox;

drop table if exists employe;

drop table if exists Entreprise;

SET REFERENTIAL_INTEGRITY TRUE;

drop sequence if exists CompteDropbox_seq;

drop sequence if exists employe_seq;

drop sequence if exists Entreprise_seq;

